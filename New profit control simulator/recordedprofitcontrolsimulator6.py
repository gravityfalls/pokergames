import csv
#import matplotlib.pyplot as plt
#import random as rd
import numpy as np

numbers=pow(10,5)
source=np.arange(0, numbers)

runclients=77

grandaccubets=0
grandaccubonus=0
accumgrandprofitrate=[]
accumgrandbets=[]

collectivebets=[[] for j in range(runclients)]
collectivebonus=[[] for j in range(runclients)]
eachprofitrate=[[] for j in range(runclients)]
eachcycleprofitrate=[]

lowerbounds=np.full(runclients,0.03)
upperbounds=np.full(runclients,0.07)

UL=-0.05
HUL=-3
sampleNumber=5000
cycles=1441
spikeslocation=[[] for j in range(runclients)]
deviationRounds=[0 for j in range(runclients)]
cycleswitch=1000

accubets=[0 for j in range(runclients)]
accubonus=[0 for j in range(runclients)]
profitrate=[[] for j in range(runclients)]
collectivedriftaway=[]
driftbounds=[0.052,0.013]
driftpullingbackactivated=0

def getKeysByValue(dictOfElements, valueToFind):
    listOfKeys = list()
    listOfItems = dictOfElements.items()
    for item  in listOfItems:
        if item[1] == valueToFind:
            listOfKeys.append(item[0])
    return  listOfKeys

def totalplatform(allbets, allbonus):
    BETS=0
    BONUS=0
    for j in range(runclients):
        BETS+=sum(allbets[j])
        BONUS+=sum(allbonus[j])
    return 1-BONUS/BETS

def rankingsales():
    each={}
    for j in range(runclients):
        each[j]=accubets[j]
    topranks={k: v for k, v in sorted(each.items(), key=lambda item: item[1],reverse=True)}
    return list(topranks.keys())[:5]

def avesquaredsum(a,c):
    s=0
    for i in a:
        s+=(i-c)*(i-c)
    return s/len(a)

def targetfunctionBAN(a):
    b=sorted(a)
    M=max(b)
    m=min(b)
    c=b.copy()
    c.remove(M)
    c.remove(m)
    targetfunction={}
    for i in range(len(c)-1):
        targetfunction[i]=avesquaredsum(c[:i+1],m)+avesquaredsum(c[i+1:],M)
    index=getKeysByValue(targetfunction, min(targetfunction.values()))[0]
    lowerhalf=c[:index+1]
    avelowerhalf=sum(lowerhalf)/len(c[:index+1])
    return lowerhalf, avelowerhalf

def targetfunction(a):
    originallen=len(a)
    if len(a)>70:
        a=a[int(len(a)/2):]
    b=sorted(a)
    M=max(b)
    m=min(b)
    c=b.copy()
    c.remove(M)
    c.remove(m)
    targetfunction={}
    for i in range(len(c)-1):
        targetfunction[i]=avesquaredsum(c[:i+1],m)+avesquaredsum(c[i+1:],M)    
    index=getKeysByValue(targetfunction, min(targetfunction.values()))[0]
    lowerhalf=c[:index+1]
    avelowerhalf=sum(lowerhalf)/len(c[:index+1])
    upperhalf=c[index+1:]
    aveupperhalf=sum(upperhalf)/len(c[index+1:])
    if len(c[index+1:])>originallen/4:
        return upperhalf, aveupperhalf
    else:        
        return lowerhalf, avelowerhalf

def targetfunctionbeta(a):
    b=sorted(a)
    M=max(b)
    m=min(b)
    c=b.copy()
    c.remove(M)
    c.remove(m)
    targetfunction={}
    for i in range(len(c)-1):
        #[squaredsum(c[:i+1],m)+squaredsum(c[i+1:],M),
        targetfunction[i]=avesquaredsum(c[:i+1],m)+avesquaredsum(c[i+1:],M)
    index=getKeysByValue(targetfunction, min(targetfunction.values()))[0]
    #lowerhalf=c[:index+1]
    #avelowerhalf=sum(lowerhalf)/len(c[:index+1])
    higherhalf=c[index+1:]
    avehigherhalf=sum(higherhalf)/len(c[index+1:])
    return avehigherhalf

def intermediate(x):
    b=sorted(x)
    M=max(b)
    m=min(b)
    c=b.copy()
    c.remove(M)
    c.remove(m)
    targetfunction={}
    for i in range(len(c)-1):
        targetfunction[i]=avesquaredsum(c[:i+1],m)+avesquaredsum(c[i+1:],M)
    index=getKeysByValue(targetfunction, min(targetfunction.values()))[0]
    higherhalf=c[index+1:]
    #avehigherhalf=sum(higherhalf)/len(c[index+1:])
    return higherhalf


for c in range(1,cycles):
    #以下為針對全平台的貪婪算法
    if c<cycleswitch:
        q="//Users/asmusk/betamt0527/betdata0527betamt"+str(c).zfill(4)+".csv"
        t="//Users/asmusk/betdata0527/betdata0527"+str(c).zfill(4)+".csv"
        eachclientbets=[0 for j in range(runclients)]
        eachclientbonustable=[np.zeros(numbers) for j in range(runclients)]
        #print("Current cycle: ",c)
        with open(q,'r') as csvfile:
            csvreader = csv.reader(csvfile)
            for row in csvreader:
                if int(row[0])==c:
                    eachclientbets[int(row[1])-1]=float(row[2])
    
        with open(t,'r') as csvfile:
            csvreader = csv.reader(csvfile)
            count=0
            for row in csvreader:
                if int(row[0])==c:
                    eachclientbonustable[int(row[1])-1][int(row[2])]=float(row[3])    
    
        grandbets=sum(eachclientbets)
        grandbonus=sum(eachclientbonustable)
        grandprofitrate=1-grandbonus/grandbets
        thisbonus=min(grandbonus)    
        location=list(grandbonus).index(thisbonus)
        for j in range(runclients):
            collectivebets[j].append(eachclientbets[j])
            collectivebonus[j].append(eachclientbonustable[j][location])
            accubets[j]+=collectivebets[j][-1]
            accubonus[j]+=collectivebonus[j][-1]
            if accubets[j]!=0:
                profitrate[j].append(1-accubonus[j]/accubets[j])
            else:
                profitrate[j].append(0)
            
        grandaccubets+=grandbets
        grandaccubonus+=thisbonus
        #accumulated profit rate to this cycle
        TothisPoint=1-grandaccubonus/grandaccubets
        accumgrandprofitrate.append(TothisPoint)
        #each cycle in game total bets
        accumgrandbets.append(grandaccubets)
        thistimeMax=max(grandprofitrate)
        eachcycleprofitrate.append(thistimeMax)
        driftaway=np.zeros(2)
        for i in range(runclients):
            if profitrate[i][-1]<0:
               driftaway[0]+=1
            elif profitrate[i][-1]>0.2:
                driftaway[1]+=1
        collectivedriftaway.append(driftaway/runclients)
        print(c, driftaway/runclients, thistimeMax, TothisPoint)
    #以下為本殺率算法部分
    else:
        q="//Users/asmusk/betamt0527/betdata0527betamt"+str(c).zfill(4)+".csv"
        t="//Users/asmusk/betdata0527/betdata0527"+str(c).zfill(4)+".csv"
        eachclientbets=[0 for j in range(runclients)]
        eachclientbonustable=[np.zeros(numbers) for j in range(runclients)]
        #print("Current cycle: ",c)
        with open(q,'r') as csvfile:
            csvreader = csv.reader(csvfile)
            for row in csvreader:
                if int(row[0])==c:
                    eachclientbets[int(row[1])-1]=float(row[2])
    
        with open(t,'r') as csvfile:
            csvreader = csv.reader(csvfile)
            count=0
            for row in csvreader:
                if int(row[0])==c:
                    eachclientbonustable[int(row[1])-1][int(row[2])]=float(row[3])
        
        grandbets=sum(eachclientbets)
        grandbonus=sum(eachclientbonustable)
        grandprofitrate=1-grandbonus/grandbets     
        thistimeMax=max(grandprofitrate)
        thispick=np.random.choice(source, sampleNumber, replace=False)
        wanted={}
        unwanted={}
        for i in thispick:
            wanted[i]=0
            unwanted[i]=0
            
        voting={}
        topranks=rankingsales()
        for j in range(runclients):        
            if accubets[j]!=0:
                if j in topranks and profitrate[j][-1]<lowerbounds[j]/2:
                    voting[j]=int(np.log(accubets[j]))+1
                else:
                    voting[j]=1
            else:
                voting[j]=0

        for j in range(runclients):
            if  profitrate[j]!=[] and profitrate[j][-1]<0 and profitrate[j][-1]!=0 and profitrate[j][-2]!=0:
                deviationRounds[j]+=1
            else:
                deviationRounds[j]=0
            '''
            for element in profitrate[j]:
                if element<lowerbounds[j]:
                    deviationRounds[j]+=1
            '''
            #only when too many clients under water
            if collectivedriftaway!=[] and collectivedriftaway[-1][0]>driftbounds[0]:
                driftpullingbackactivated=1
                #to provide extra votingweight for losing clients
            elif collectivedriftaway!=[] and collectivedriftaway[-1][0]<driftbounds[1]:
                driftpullingbackactivated=0

            if driftpullingbackactivated:
                if profitrate[j]!=[] and collectivebets[j][-1]!=0 and profitrate[j][-1]<UL:
                    if deviationRounds[j]!=0:
                        voting[j]*=int(np.log(deviationRounds[j]))+1
                #if profitrate[j][-1]<0:
                #    if deviationRounds[j]!=0:
                #        print(j, profitrate[j][-1], int(np.log(deviationRounds[j]))+1)
                
            if eachclientbets[j]!=0:
                if len(collectivebets[j])>3:
                    lowerhalf, avelowerhalf = targetfunction(collectivebets[j])
                    if eachclientbets[j]>4*avelowerhalf:
                        spikeslocation[j].append(c)
                        votingweight=int(np.log(c/len(spikeslocation[j])))+1 #adding log here
                        voting[j]=votingweight
            if eachclientbets[j]!=0:
                eachNumberProfitrate=1-eachclientbonustable[j]/eachclientbets[j]
            else:
                eachNumberProfitrate=np.zeros(numbers)
            
            if c>1:
                accubets[j]+=collectivebets[j][-1]
                accubonus[j]+=collectivebonus[j][-1]
            if accubets[j]!=0:
                profitrate[j].append(1-accubonus[j]/accubets[j])
            else:
                profitrate[j].append(0)
            if accubets[j]!=0 and eachclientbets[j]!=0:
                accuprofitrateEachNumber=1-(accubonus[j]+eachclientbonustable[j])/(accubets[j]+eachclientbets[j])
                #diagram(accuprofitrateEachNumber)
                for index in thispick:
                    if profitrate[j][-1]<lowerbounds[j]:
                        if profitrate[j][-1]>UL:
                            if eachNumberProfitrate[index]>lowerbounds[j]:
                                wanted[index]+=voting[j]
                            elif eachNumberProfitrate[index]<=HUL or accuprofitrateEachNumber[index]<=profitrate[j][-1]:
                                unwanted[index]+=voting[j]*(abs(int(eachNumberProfitrate[index]))+1)
                        else:
                            if eachNumberProfitrate[index]>lowerbounds[j] or accuprofitrateEachNumber[index]>profitrate[j][-1]:
                                wanted[index]+=voting[j]
                            elif eachNumberProfitrate[index]<=HUL or accuprofitrateEachNumber[index]<=profitrate[j][-1]:
                                unwanted[index]+=voting[j]*(abs(int(eachNumberProfitrate[index]))+1)
                            
                    elif profitrate[j][-1]>upperbounds[j]:
                        if accuprofitrateEachNumber[index]>=lowerbounds[j] and eachNumberProfitrate[index]<profitrate[j][-1]:
                            wanted[index]+=voting[j]*2
                        elif eachNumberProfitrate[index]<=HUL or accuprofitrateEachNumber[index]<=lowerbounds[j]:
                            unwanted[index]+=voting[j]*(abs(int(eachNumberProfitrate[index]))+1)
                    else:
                        if eachNumberProfitrate[index]<=UL:
                            unwanted[index]+=1#voting[j]                            
                        if eachNumberProfitrate[index]<=HUL or accuprofitrateEachNumber[index]<UL:
                            unwanted[index]+=voting[j]*(abs(int(eachNumberProfitrate[index]))+1)
    
        #分層找wanted 每層分漸層去除unwanted 最大的開始找 找去除最多層的
        penLayers={}
        sliced=1500
        ini=int(targetfunctionbeta(sorted(wanted.values(), reverse=True)[:sliced]))
        for w in range(ini,int(max(wanted.values()))+1):
            if getKeysByValue(wanted, w)!=[]:
                wantedresult=getKeysByValue(wanted, w)
                unwantedresult=[]#getKeysByValue(unwanted, max(unwanted.values()))    
                finalwantedresult=[]
                penLayers[w]=0
                #i here is depth from the highest unwanted value
                for i in range(max(unwanted.values())):
                    copywantedresult=wantedresult.copy()
                    unwantedresult=getKeysByValue(unwanted, max(unwanted.values())-i)    
                    for e in wantedresult:
                        if e in unwantedresult:
                            copywantedresult.remove(e)
                            if copywantedresult==[]:
                                finalwantedresult=wantedresult.copy()
                                break
                    if finalwantedresult!=[]:
                        break
                    penLayers[w]=i
                    wantedresult=copywantedresult.copy()
    
        MW=max(wanted.values())
        MU=max(unwanted.values())
    
        if penLayers!={}:
            squaredsum={}
            for k in penLayers:
                s=(MW-k)*(MW-k)+(MU-penLayers[k])*(MU-penLayers[k])
                squaredsum[k]=s
            index=getKeysByValue(squaredsum,min(squaredsum.values()))[0]
            preprocessingwantedresult=getKeysByValue(wanted, index)
            deepwanted={}
            for i in preprocessingwantedresult:
                deepwanted[i]=MU-unwanted[i]
            finalwantedresult=getKeysByValue(deepwanted, max(deepwanted.values()))        
        else:
            finalwantedresult=getKeysByValue(wanted, max(wanted.values()))
            
        get=np.random.choice(finalwantedresult)
    
        for assign in range(runclients):
            collectivebonus[assign].append(eachclientbonustable[assign][get])        
            collectivebets[assign].append(eachclientbets[assign])
            eachcycleprofitrate.append(grandprofitrate[assign])

        if c>0:
            driftaway=np.zeros(2)
            for i in range(runclients):
                if profitrate[i][-1]<0:
                   driftaway[0]+=1
                elif profitrate[i][-1]>0.2:
                    driftaway[1]+=1
            print(c,driftaway/runclients, thistimeMax,totalplatform(collectivebets, collectivebonus))
            collectivedriftaway.append(driftaway/runclients)
        else:
            print(c, "---")
    
'''
np.savetxt("collectivedriftaway06526.csv", collectivedriftaway, delimiter=",")

import csv
with open('eachprofitrate0617.csv', 'w', newline='') as f:
    writer = csv.writer(f)
    writer.writerows(eachprofitrate)

''' 

'''   
import matplotlib.pyplot as plt        
fig, ax = plt.subplots(num=None, figsize=(16,12),dpi=300, facecolor='w', edgecolor='k')
ax.set_color_cycle=(["royalblue","red","green","yellow"])
for i in range(runclients):
    n=len(profitrate[i])
    x1=[r for r in range(n)]
    y1=[profitrate[i][s] for s in range(n)]
    #plt.bar(x1, y1)
    plt.scatter(x1, y1, s=1)
    plt.plot(x1, y1, '-')        
'''