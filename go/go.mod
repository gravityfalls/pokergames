module practice-go

go 1.13

require (
	github.com/gin-gonic/gin v1.5.0
	github.com/go-sql-driver/mysql v1.4.1
	github.com/gorilla/websocket v1.4.1
	github.com/jinzhu/gorm v1.9.11
	gitlab.silkrode.com.tw/golang/log v1.1.1
	go.mongodb.org/mongo-driver v1.3.3
)
