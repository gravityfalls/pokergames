package save2mongo

import (
	"context"
	"fmt"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/mongo/readpref"
	"time"
)

//MongoDB 結構
type MongoDB struct {
	Client *mongo.Client
	ctx    context.Context
}

//Object mongoDB實體
var Object *MongoDB

func init() {
	Object = &MongoDB{}
	Object.Client, Object.ctx = newMongoClient()
	err := Object.Ping()
	if err != nil {
		panic(err)
	}
}

//NewMongoClient 取得新連線
func newMongoClient() (*mongo.Client, context.Context) {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()
	mongoURI := fmt.Sprintf("mongodb://%s:%s@%s", "root", "root_midas", "localhost:27017")
	option := options.Client().ApplyURI(mongoURI)
	client, err := mongo.Connect(ctx, option)
	if err != nil {
		panic("set mongo config error")
	}
	return client, ctx
}

//Ping 測試連線
func (m *MongoDB) Ping() error {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()
	return m.Client.Ping(ctx, readpref.Primary())
}

//InsertDocument 建立新資料
func (m *MongoDB) InsertDocument(database, collection string, doc interface{}) error {
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	newColl := m.Client.Database(database).Collection(collection)
	_, err := newColl.InsertOne(ctx, doc)
	if err != nil {
		fmt.Errorf("insert mongo fail: %d", err)
		return err
	}
	return nil
}
