package awardcentertest

import (
	"fmt"
	"sync"
)

func (i *InvNodeKeyBeta) BrutalExpandSubTree(KeyThisIsoNode InvNodeKeyBeta, thisIsoNode Isopoints, invent map[InvNodeKeyBeta]Isopoints, group *sync.WaitGroup, mu *sync.Mutex) {
	//var mainLayer = thisIsoNode.Index % 1000
	//length := len(thisIsoNode.RestCards[0]) + len(thisIsoNode.RestCards[1]) + len(thisIsoNode.RestCards[2])
	//fmt.Println("This isonode has total cards:", length)
	var role = (thisIsoNode.Index % 1000) % 3
	var subtree = make([]Layer, role+1)
	//建造初始節點
	subtree[role].Nodes = append(subtree[role].Nodes, Nodes{role, role,
		KeyThisIsoNode.PassCount, -999, []int{},
		CardType{thisIsoNode.CardSet, KeyThisIsoNode.CardTypeSerial},
		map[int][]int{}, []int{}})
	for i := 0; i < 3; i++ {
		subtree[role].Nodes[0].RestCards[i] = thisIsoNode.RestCards[i]
	}
	var l = 1 + role
	for {
		var newLayer Layer
		subtree = append(subtree, newLayer)
		if len(subtree[l-1].Nodes) != 0 {
			subtree[l-1].iGPBrutal(subtree, subtree[l-1].Nodes, l, (l-1)%3) //開始分佈節點
			//fmt.Println(l-1, len(subtree[l-1-role].Nodes))
		} else {
			break
		}
		l++
	}
	bfWOi(subtree)
	//把結果傳給倉庫指定的iso node
	if len(subtree[role].Nodes[0].Outcome) == 0 {
		fmt.Println(subtree[role].Nodes[0])
	}
	mu.Lock()
	var x = invent[KeyThisIsoNode]
	x.Outcome = subtree[role].Nodes[0].Outcome
	invent[KeyThisIsoNode] = x
	mu.Unlock()
	//if len(invent[KeyThisIsoNode].Outcome) == 0 {
	//	fmt.Println(thisIsoNode.Index, role, len(subtree), invent[KeyThisIsoNode])
	//}
	//tree[thisIsoNode.Index%1000].Nodes[thisIsoNode.Index/1000].Outcome = invent[KeyThisIsoNode].Outcome
	group.Done()
}

func (i *InvNodeKeyBeta) ShrinkedExpandSubTree(KeyThisIsoNode InvNodeKeyBeta, thisIsoNode Isopoints, invent map[InvNodeKeyBeta]Isopoints, group *sync.WaitGroup, mu *sync.Mutex) {
	var role = (thisIsoNode.Index % 1000) % 3
	var subtree = make([]Layer, role+1)
	//建造初始節點
	subtree[role].Nodes = append(subtree[role].Nodes, Nodes{role, role,
		KeyThisIsoNode.PassCount, -999, []int{},
		CardType{thisIsoNode.CardSet, KeyThisIsoNode.CardTypeSerial},
		map[int][]int{}, []int{}})
	for i := 0; i < 3; i++ {
		subtree[role].Nodes[0].RestCards[i] = thisIsoNode.RestCards[i]
	}

	var l = 1 + role
	for {
		var newLayer Layer
		subtree = append(subtree, newLayer)
		if len(subtree[l-1].Nodes) != 0 {
			subtree[l-1].iGPShrinked(subtree, subtree[l-1].Nodes, l, (l-1)%3) //開始分佈節點
			//fmt.Println(l-1, len(subtree[l-1-role].Nodes))
		} else {
			break
		}
		l++
	}
	bfWOi(subtree)
	if len(subtree[role].Nodes[0].Outcome) == 0 {
		if getTotalRestCards(subtree[role].Nodes[0].RestCards) < 14 {
			subtree[role].Nodes[0].Outcome = BrutalExpandSubTree(subtree[role].Nodes[0])
			//i.BrutalExpandSubTree(KeyThisIsoNode, thisIsoNode, invent, group, mu)
		} else {
			subtree[role].Nodes[0].Outcome = ShrinkedExpandSubTree(subtree[role].Nodes[0])
		}
	}
	//把結果傳給倉庫指定的iso node
	mu.Lock()
	var x = invent[KeyThisIsoNode]
	x.Outcome = subtree[role].Nodes[0].Outcome
	invent[KeyThisIsoNode] = x
	mu.Unlock()
	//if len(invent[KeyThisIsoNode].Outcome) == 0 {
	//	fmt.Println(thisIsoNode.Index, role, len(subtree), invent[KeyThisIsoNode])
	//}
	//tree[thisIsoNode.Index%1000].Nodes[thisIsoNode.Index/1000].Outcome = invent[KeyThisIsoNode].Outcome
	group.Done()
}

//func SubTree() {
//	thisNode := Nodes{6, 3006, 2, 3005, []int{},
//		CardType{[]int{10013}, 0},
//		map[int][]int{0: {20009}, 1: {10012, 10011, 10006, 20006}, 2: {-1}},
//		[]int{}, 6}
//	outcome := ExpandSubTree(thisNode)
//	fmt.Println(outcome)
//}

func getNodeRestCardsTotal(restcards map[int][]int) int {
	var total = 0
	for i := range restcards {
		total += len(restcards[i])
	}
	return total
}

func BrutalExpandSubTree(thisNode Nodes) []int {
	//var mainLayer = thisIsoNode.Index % 1000
	//defer timeTrack(time.Now(), "獲取Brutal Expansion subtree結果建造時間")
	var Nextrole = (thisNode.Index % 1000) % 3
	var subtree = make([]Layer, Nextrole+1)
	//建造初始節點
	subtree[Nextrole].Nodes = append(subtree[Nextrole].Nodes, Nodes{Nextrole, Nextrole,
		thisNode.PassCount, -999, []int{},
		CardType{thisNode.CardSetInfo.CardSet, thisNode.CardSetInfo.CardTypeSerial},
		map[int][]int{}, []int{}})
	for i := 0; i < 3; i++ {
		subtree[Nextrole].Nodes[0].RestCards[i] = thisNode.RestCards[i]
	}
	//fmt.Println(thisIsoNode.Index, mainLayer, role, len(subtree))
	//fmt.Println("Role:", role)
	var l = 1 + Nextrole
	for {
		var newLayer Layer
		subtree = append(subtree, newLayer)
		if len(subtree[l-1].Nodes) != 0 {
			subtree[l-1].iGPBrutal(subtree, subtree[l-1].Nodes, l, (l-1)%3) //開始分佈節點
			//fmt.Println(l-1, len(subtree[l-1-role].Nodes))
		} else {
			break
		}
		l++
	}
	bfWOi(subtree)
	//totalLayers(subtree, 1, 3)
	return subtree[Nextrole].Nodes[0].Outcome
}

func ShrinkedExpandSubTree(thisNode Nodes) []int {
	//var mainLayer = thisIsoNode.Index % 1000
	var Nextrole = (thisNode.Index % 1000) % 3
	var subtree = make([]Layer, Nextrole+1)
	//建造初始節點
	subtree[Nextrole].Nodes = append(subtree[Nextrole].Nodes, Nodes{Nextrole, Nextrole,
		thisNode.PassCount, -999, []int{},
		CardType{thisNode.CardSetInfo.CardSet, thisNode.CardSetInfo.CardTypeSerial},
		map[int][]int{}, []int{}})
	for i := 0; i < 3; i++ {
		subtree[Nextrole].Nodes[0].RestCards[i] = thisNode.RestCards[i]
	}
	//fmt.Println(thisIsoNode.Index, mainLayer, role, len(subtree))
	//fmt.Println("Role:", role)
	var l = 1 + Nextrole
	for {
		var newLayer Layer
		subtree = append(subtree, newLayer)
		if len(subtree[l-1].Nodes) != 0 {
			subtree[l-1].iGPShrinked(subtree, subtree[l-1].Nodes, l, (l-1)%3) //開始分佈節點
			//fmt.Println(l-1, len(subtree[l-1-role].Nodes))
		} else {
			break
		}
		l++
	}
	bfWOi(subtree)
	//totalLayers(subtree, 1, l)
	return subtree[Nextrole].Nodes[0].Outcome
}
