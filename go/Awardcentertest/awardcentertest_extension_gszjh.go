package awardcentertest

import (
	"fmt"
	"math/rand"
	"sort"
	"time"
	//"time"
)

//EnumGszjhType NameMap
var EnumGszjhType = map[int32]string{
	0: "EnumGszjhTypeUnknown",
	1: "EnumGszjhTypeHighCard",
	2: "EnumGszjhTypePair",
	3: "EnumGszjhTypeStraight",
	4: "EnumGszjhTypeFlush",
	5: "EnumGszjhTypeStraightFlush",
	6: "EnumGszjhTypeThreeKing",
}

func randKey(m map[int]float64) int {
	var key int
	for k := range m {
		key = k
	}
	return key
}

func gszjhMaxFinder(theseOdds []uint32) uint32 {
	var reference uint32
	for i := 0; i < len(theseOdds); i++ {
		if theseOdds[i] > reference {
			reference = theseOdds[i]
		}
	}
	return reference
}

func gszjhMaxIndexFinder(theseOdds []uint32) int {
	var reference uint32
	var index int
	for i := 0; i < len(theseOdds); i++ {
		if theseOdds[i] > reference {
			reference = theseOdds[i]
			index = i
		}
	}
	return index
}

func pairFinder(p []uint32) (uint32, uint32) {
	var repeat = make(map[uint32]uint32)
	var pair uint32
	var single uint32
	for i := 0; i < len(p); i++ {
		repeat[p[i]]++
	}
	for i, v := range repeat {
		if v == 2 {
			pair = i
		} else {
			single = i
		}
	}
	return pair, single
}

//pairColorFinder presume input CardSet is pair+single
func pairColorFinder(w []uint32) ([]uint32, uint32) {
	p := []uint32{w[0] % SuitShift, w[1] % SuitShift, w[2] % SuitShift}
	var _, single = pairFinder(p)
	var singleColor uint32
	var pairColor []uint32
	for i := 0; i < 3; i++ {
		if w[i]%10000 == single {
			singleColor = w[i] / 10000
		} else {
			pairColor = append(pairColor, w[i]/10000)
		}
	}
	return pairColor, singleColor
}

func colorFinder(e uint32, w []uint32) uint32 {
	var color uint32
	for i := 0; i < len(w); i++ {
		if e == w[i]%10000 {
			color = w[i] / 10000
		}
	}
	return color
}

//type2odds 牌型轉換到賠率
func type2odds(w []uint32) (uint32, uint32) {
	sort.Slice(w, func(i, j int) bool { return w[i]%SuitShift < w[j]%SuitShift })
	e := []uint32{w[0] % SuitShift, w[1] % SuitShift, w[2] % SuitShift}
	s := []uint32{w[0] / SuitShift, w[1] / SuitShift, w[2] / SuitShift}
	var score uint32
	if e[0] == e[1] && e[1] == e[2] { //判斷豹子回傳賠率40
		score = 6*TypeScore*TypeScore + e[0]
		return 40, score
	} else if ((e[2]-e[1]) == 1 && (e[1]-e[0]) == 1) || (e[0] == 2 && e[1] == 3 && e[2] == 14) { //判斷順子
		if s[0] == s[1] && s[1] == s[2] { //判斷同花順
			if e[0] != 2 && e[1] != 3 && e[2] != 14 {
				score = 5*TypeScore*TypeScore + (e[0]+e[1]*16+e[2]*16*16)*10 + s[0] //同花順照位置加乘分數再加上花色分數
			} else {
				score = 5*TypeScore*TypeScore + s[0] //最小的同花順 只計算花色
			}
			return 20, score
		} //判斷非同花順子
		if e[0] != 2 && e[1] != 3 && e[2] != 14 {
			score = 3*TypeScore*TypeScore + (e[0]+e[1]*16+e[2]*256)*100 + s[0] + s[1]*4 + s[2]*16
		} else {
			score = 3*TypeScore*TypeScore + s[0] + s[1]*4 + s[2]*16
		}
		return 6, score

	} else if s[0] == s[1] && s[1] == s[2] { //判斷金花
		score = 4*TypeScore*TypeScore + (e[0]+e[1]*16+e[2]*256)*10 + s[0] //點數都一樣時候花色會影響比大小
		return 10, score
	} else if (e[0] == e[1] && e[1] != e[2]) || (e[1] == e[2] && e[0] != e[2]) || (e[0] == e[2] && e[1] != e[2]) { //判斷對子
		pair, single := pairFinder(e)
		_, colorSingle := pairColorFinder(w)
		score = 2*TypeScore*TypeScore + (pair*16+single)*100 + colorSingle //比較玩對子點數之後比單張點數，之後再比單張花色
		return 4, score
	} else { //剩下為散牌
		score = TypeScore*TypeScore + (e[0]+e[1]*16+e[2]*256)*100 + (s[0] + s[1]*4 + s[2]*16) //把所有牌照大小花色比較
		return 2, score
	}
}

//CookGSZJH 做牌系統
func CookGSZJH() {
	var Poker []uint32
	var deckNumber = 1
	for k := 1; k <= deckNumber; k++ {
		for i := 1; i <= 4; i++ {
			for j := 2; j <= 14; j++ {
				Poker = append(Poker, uint32(i*SuitShift+j))
			}
		}
	}
	var Base = 100 //抓取變數
	//var HouseGain = 1000000.0
	var HouseGain = 9090.0 //抓取變數

	var PLayerCount = 3
	var BotCount = 1
	var target = 0.05
	var current = 0.16
	var deviation = 0.1
	var src cryptoSource
	rnd := rand.New(src)
	rand.Seed(rnd.Int63n(time.Now().UnixNano()))
	rand.Shuffle(len(Poker), func(i, j int) { Poker[i], Poker[j] = Poker[j], Poker[i] })
	var CardSetsPrep [][]uint32
	for j := 0; j < BotCount+PLayerCount; j++ {
		CardSetsPrep = append(CardSetsPrep, Poker[j*3:j*3+3])
	}
	fmt.Println(CardSetsPrep)

	var cardOdds []uint32
	var scores []uint32
	var h int
	var Odd float64
	for i := 0; i < len(CardSetsPrep); i++ {
		//fmt.Println(CardSetsPrep[i])
		var thisOdds, thisScore uint32
		thisOdds, thisScore = type2odds(CardSetsPrep[i])
		cardOdds = append(cardOdds, thisOdds)
		scores = append(scores, thisScore)
	}

	//最大的牌型位置
	h = gszjhMaxIndexFinder(scores)
	var NetGain = (current - target) * HouseGain
	//最大的牌型對應的賠率
	Odd = float64(gszjhMaxFinder(cardOdds))
	var thisBonus = Odd * float64(Base)

	var RearrangedCards [][]uint32
	if len(CardSetsPrep) != 0 {
		//先放一張點數最大的牌
		RearrangedCards = append(RearrangedCards, CardSetsPrep[h])
		//再依序把剩下的牌放進入
		for i := 0; i < len(CardSetsPrep); i++ {
			if i != h {
				RearrangedCards = append(RearrangedCards, CardSetsPrep[i])
			}
		}
		//fmt.Println(RearrangedCards)
	}
	if len(RearrangedCards) < PLayerCount+BotCount {
		fmt.Errorf("for some reason, we don't have card sets")
	}
	//最大的牌在最前面，其他的牌在後面
	//fmt.Println("NewOrder", RearrangedCards)
	if len(RearrangedCards) != 0 {
		if current < target {
			//把最大牌放在BOTS
			fmt.Println("殺")
			var botsCards = RearrangedCards[:BotCount]   //to fill bots
			var pLayerCards = RearrangedCards[BotCount:] //to fill pLayers
			rand.Seed(rnd.Int63n(time.Now().UnixNano()))
			rand.Shuffle(len(botsCards), func(i, j int) { botsCards[i], botsCards[j] = botsCards[j], botsCards[i] })
			rand.Shuffle(len(pLayerCards), func(i, j int) { pLayerCards[i], pLayerCards[j] = pLayerCards[j], pLayerCards[i] })
			fmt.Println(botsCards, pLayerCards)
		} else if current >= target+deviation && thisBonus < NetGain { //增加條件
			//把最大牌放在pLayers 裡面的任意位置
			fmt.Println("吐")
			var pLayerCards = RearrangedCards[:PLayerCount] //to fill pLayers
			var botsCards = RearrangedCards[PLayerCount:]   //to fill bots
			rand.Seed(rnd.Int63n(time.Now().UnixNano()))
			rand.Shuffle(len(botsCards), func(i, j int) { botsCards[i], botsCards[j] = botsCards[j], botsCards[i] })
			rand.Shuffle(len(pLayerCards), func(i, j int) { pLayerCards[i], pLayerCards[j] = pLayerCards[j], pLayerCards[i] })
			fmt.Println(botsCards, pLayerCards)
		}
	}

}
