package awardcentertest

import (
	crand "crypto/rand"
	"encoding/binary"
	"fmt"
	"log"
	"math"
	"math/rand"
	"time"
)

const (
	//TypeScore 位移3位
	TypeScore = 1000
	//CardScore 位移1位
	CardScore = 10
)

type cryptoSource struct{}

func (s cryptoSource) Seed(seed int64) {}

func (s cryptoSource) Int63() int64 {
	return int64(s.Uint64() & ^uint64(1<<63))
}

func (s cryptoSource) Uint64() (v uint64) {
	err := binary.Read(crand.Reader, binary.BigEndian, &v)
	if err != nil {
		log.Fatal(err)
	}
	return v
}

func rangeSlice(start, stop int) []int {
	if start > stop {
		panic("Slice ends before it started")
	}
	xs := make([]int, stop-start)
	for i := 0; i < len(xs); i++ {
		xs[i] = i + start
	}
	return xs
}

func permutation(xs []int) (permuts [][]int) {
	var rc func([]int, int)
	rc = func(a []int, k int) {
		if k == len(a) {
			permuts = append(permuts, append([]int{}, a...))
		} else {
			for i := k; i < len(xs); i++ {
				a[k], a[i] = a[i], a[k]
				rc(a, k+1)
				a[k], a[i] = a[i], a[k]
			}
		}
	}
	rc(xs, 0)
	return permuts
}
func bundle(slice []int) int {
	var cardSum = 0
	for l := 0; l < 3; l++ {
		if (slice[l] % SuitShift) < 10 {
			cardSum += slice[l] % SuitShift
		} else {
			cardSum += 10
		}
	}
	if cardSum%10 == 0 {
		return 1
	}
	return 0
}

func findNiu(slice []int) int {
	var cardSum = 0
	for l := 3; l < 5; l++ {
		if (slice[l] % SuitShift) < 10 {
			cardSum += slice[l] % SuitShift
		} else {
			cardSum += 10
		}
	}
	if cardSum%10 > 0 {
		return cardSum % 10
	}
	return 10

}

func findFiveSmallNiu(slice []int) int {
	var s = []int{0, 0, 0, 0, 0}
	for l := 0; l < 5; l++ {
		if (slice[l] % SuitShift) < 10 {
			s[l] = slice[l] % SuitShift
		} else {
			s[l] = 10
		}
	}
	if s[0] < 5 && s[1] < 5 && s[2] < 5 && s[3] < 5 && s[4] < 5 && s[0]+s[1]+s[2]+s[3]+s[4] <= 10 {
		return 1
	}
	return 0
}

func findFourFlowerNiu(slice []int) int {
	if (slice[0]%SuitShift) > 10 && (slice[1]%SuitShift) > 10 && (slice[2]%SuitShift) > 10 && (slice[3]%SuitShift) > 10 && (slice[4]%SuitShift) == 10 {
		return 1
	}
	return 0

}

func findFiveFlowerNiu(slice []int) int {
	if (slice[0]%SuitShift) > 10 && (slice[1]%SuitShift) > 10 && (slice[2]%SuitShift) > 10 && (slice[3]%SuitShift) > 10 && (slice[4]%SuitShift) > 10 {
		return 1
	}
	return 0

}

func findFourBomb(slice []int) int {
	var c = slice[0] % SuitShift
	var check = 1
	for i := 0; i < 4; i++ {
		if c == slice[i]%SuitShift {
			check *= 1
		} else {
			check *= 0
		}
	}
	if check == 1 {
		return 1
	}
	return 0

}

func findMax(theseCards []int) int {
	var reference = 0
	var index = 0
	for i := 0; i < len(theseCards); i++ {
		if theseCards[i] > reference {
			reference = theseCards[i]
			index = i
		}
	}
	return index
}

func cardPoints(card int) int {
	return (card%SuitShift)*CardScore + (card / SuitShift)
}

func findMaxPointCard(thisCards []int) int {
	var cardPoints = []int{0, 0, 0, 0, 0}
	for i := 0; i < 5; i++ {
		cardPoints[i] = (thisCards[i]%SuitShift)*CardScore + (thisCards[i] / SuitShift)
		//fmt.Println(cardPoints[i])
	}
	return findMax(cardPoints)
}

func cardsType(tableCardSets [][]int, total int) (int, int) {
	EachSetPerms := permutation(rangeSlice(0, 5)) //EachSet Has 5 cards, which is fixed number for TBNN.
	var packScore = make([]int, total)            //六副牌的牌組分數
	for i := range packScore {
		packScore[i] = 0
	}
	var oddsMap = make(map[int]int)
	for t := 0; t < len(tableCardSets); t++ {
		var NiuCount = 0
		var fiveSmallNiu int
		var fourFlowerNiu int
		var FiveFlowerNiu int
		var fourBomb int
		var odds = 1
		var setScore = 0
		for i := 0; i < len(EachSetPerms); i++ {
			slice := make([]int, 5)
			for s := 0; s < 5; s++ {
				slice[s] = tableCardSets[t][EachSetPerms[i][s]]
			}
			//判斷無牛／牛幾／牛牛
			var thisSetScore = 0
			var thisNiu = findNiu(slice)
			//沒牛～牛牛
			if bundle(slice) == 1 && thisNiu > NiuCount {
				NiuCount = findNiu(slice)
				if NiuCount <= 6 {
					odds = 1
				} else if NiuCount > 6 && NiuCount < 10 {
					odds = 2
				} else {
					odds = 3
				}
				thisSetScore = (0*TypeScore+NiuCount)*TypeScore + cardPoints(tableCardSets[t][findMaxPointCard(tableCardSets[t])])
			}
			//四花牛
			if findFourFlowerNiu(slice) == 1 {
				fourFlowerNiu = 1
				if odds < 4 {
					odds = 4
				}
				thisSetScore = (2*TypeScore)*TypeScore + cardPoints(tableCardSets[t][findMaxPointCard(tableCardSets[t])])
			}
			//五花牛
			if findFiveFlowerNiu(slice) == 1 {
				FiveFlowerNiu = 1
				if odds < 4 {
					odds = 4
				}
				thisSetScore = (3*TypeScore)*TypeScore + cardPoints(tableCardSets[t][findMaxPointCard(tableCardSets[t])])
			}
			//四炸
			if findFourBomb(slice) == 1 {
				fourBomb = 1
				if odds < 4 {
					odds = 4
				}
				thisSetScore = (4*TypeScore)*TypeScore + slice[0]%SuitShift
			}
			//五小牛
			if findFiveSmallNiu(slice) == 1 {
				fiveSmallNiu = 1
				if odds < 4 {
					odds = 4
				}
				thisSetScore = (5*TypeScore)*TypeScore + cardPoints(tableCardSets[t][findMaxPointCard(tableCardSets[t])])
			}
			if NiuCount == 0 && fiveSmallNiu == 0 && fourFlowerNiu == 0 && FiveFlowerNiu == 0 && fourBomb == 0 {
				thisSetScore = cardPoints(tableCardSets[t][findMaxPointCard(tableCardSets[t])])
			}

			if thisSetScore > setScore {
				setScore = thisSetScore
			}
		}

		packScore[t] = setScore
		oddsMap[t] = odds

	}
	fmt.Println("所有牌組的分數", packScore)
	//fmt.Println(oddsMap)
	//fmt.Println(findMax(packScore))
	return findMax(packScore), oddsMap[findMax(packScore)]
}

type pLayerAssign struct {
	multi      int
	bonus      float64
	crtp       float64 //complement of RTP
	rateDiffer float64
}

//CookTBNN ... 殺數控制
func CookTBNN() {
	var Poker []int
	var deckNumber = 1
	for k := 1; k <= deckNumber; k++ {
		for i := 1; i <= 4; i++ {
			for j := 1; j <= 13; j++ {
				Poker = append(Poker, i*SuitShift+j)
			}
		}
	}
	var base = 150
	var HouseGain = 1000.0
	var target = 0.05
	var current = 0.5
	var fluctuation = 0.01
	var countPLayer = 4
	var countBot = 5
	var total = countPLayer + countBot
	var commission = 0.05
	var PLayerMulti = make([]int, countPLayer)
	PLayerMulti = []int{1, 2, 5, 1, 1, 1, 1, 1, 1}
	var pLayerBetSum int
	//var botBetSum = base * countPLayer
	var pLayerInfos []pLayerAssign
	for i := 0; i < countPLayer; i++ {
		newPLayerInfos := pLayerAssign{}
		newPLayerInfos.multi = PLayerMulti[i]
		pLayerInfos = append(pLayerInfos, newPLayerInfos)
		pLayerBetSum += PLayerMulti[i]
	}

	var h int
	var hOdds int
	var pLayerChoices []int
	var src cryptoSource
	rnd := rand.New(src)
	rand.Seed(rnd.Int63n(time.Now().UnixNano()))
	rand.Shuffle(len(Poker), func(i, j int) { Poker[i], Poker[j] = Poker[j], Poker[i] })
	var tableCardSets [][]int
	for j := 0; j < total; j++ {
		tableCardSets = append(tableCardSets, Poker[j*5:j*5+5])
	}
	fmt.Println("候選牌組清單", tableCardSets)
	var abundance = (current - target) * HouseGain
	h, hOdds = cardsType(tableCardSets, total)
	var bonus = hOdds * base
	fmt.Println(abundance, bonus)
	fmt.Println("Highest location", h, "With its Odds", hOdds)
	if len(pLayerInfos) == 0 {
		fmt.Println("No pLayers to Consider...")
	} else {
		for i := 0; i < countPLayer; i++ {
			pLayerInfos[i].bonus = float64(hOdds*pLayerInfos[i].multi*countBot) * (1 - commission)
			if pLayerInfos[i].bonus < target {
				pLayerChoices = append(pLayerChoices, i)
			}
		}
		//ensure pLayer choices has something
		if len(pLayerChoices) == 0 {
			var smallestDiffer = math.Inf(+1)
			for i := 0; i < countPLayer; i++ {
				if pLayerInfos[i].rateDiffer < smallestDiffer {
					smallestDiffer = pLayerInfos[i].rateDiffer
				}
			}
			for i := 0; i < countPLayer; i++ {
				if pLayerInfos[i].rateDiffer == smallestDiffer {
					pLayerChoices = append(pLayerChoices, i)
				}
			}
		}
	}
	fmt.Println(pLayerInfos)
	//random some guy out!
	rand.Seed(rnd.Int63n(time.Now().UnixNano()))
	PLayerWinner := pLayerChoices[rand.Intn(len(pLayerChoices))]

	fmt.Println(PLayerWinner)

	var RearrangedCards [][]int
	//先放一張點數最大的牌
	RearrangedCards = append(RearrangedCards, tableCardSets[h])
	//再依序把剩下的牌放進入
	for i := 0; i < len(tableCardSets); i++ {
		if i != h {
			RearrangedCards = append(RearrangedCards, tableCardSets[i])
		}
	}
	//最大的牌在最前面，其他的牌在後面
	//fmt.Println("NewOrder", RearrangedCards)
	if current < target {
		//把最大牌放在BOTS
		fmt.Println("殺")
		var BotsCards = RearrangedCards[:countBot]   //to fill bots
		var pLayerCards = RearrangedCards[countBot:] //to fill pLayers
		rand.Seed(rnd.Int63n(time.Now().UnixNano()))
		rand.Shuffle(len(BotsCards), func(i, j int) { BotsCards[i], BotsCards[j] = BotsCards[j], BotsCards[i] })
		rand.Shuffle(len(pLayerCards), func(i, j int) { pLayerCards[i], pLayerCards[j] = pLayerCards[j], pLayerCards[i] })
		fmt.Println(BotsCards, pLayerCards)
	} else if current >= target+fluctuation && float64(bonus) < abundance {
		//把最大牌放在pLayers 裡面的特定位置PLayerWinner
		fmt.Println("吐")

		var pLayerCards = RearrangedCards[1:countPLayer] //to fill pLayers with No pLayerWinner Cards
		pLayerCards = append(pLayerCards, []int{})
		copy(pLayerCards[PLayerWinner+1:], pLayerCards[PLayerWinner:])
		pLayerCards[PLayerWinner] = RearrangedCards[0] //to insert pLayers with pLayerWinner Cards

		var BotsCards = RearrangedCards[countPLayer:] //to fill bots
		fmt.Println(BotsCards, pLayerCards)
	}

}
