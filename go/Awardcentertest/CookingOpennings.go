package awardcentertest

import (
	"fmt"
	"math"
	"math/rand"
	"sort"
	"time"
)

func Straight6Finder() ([][]int, [][]int) {
	var straight6s [][]int
	var demographic = make(map[int]int, 13)
	var inversedemographic = make(map[int]int, 13)
	for i := 3; i <= 15; i++ {
		demographic[i] = 0
	}
	var src cryptoSource
	rnd := rand.New(src)
	rand.Seed(rnd.Int63n(time.Now().UnixNano()))
	//var playerGet = make([][]int, 3)
	for i := 0; i < 6; i++ {
		ini := rand.Intn(8) + 3
		var count = 0
		var thisStraight6 []int
		for count < 6 {
			//playerGet[i] = append(playerGet[i], ini+count)
			thisStraight6 = append(thisStraight6, ini+count)
			count++
		}
		straight6s = append(straight6s, thisStraight6)
	}

	//straight6s = [][]int{{6, 7, 8, 9, 10, 11}, {3, 4, 5, 6, 7, 8}, {5, 6, 7, 8, 9, 10}, {7, 8, 9, 10, 11, 12}, {3, 4, 5, 6, 7, 8}, {8, 9, 10, 11, 12, 13}}
	//fmt.Println(straight6s)
	var rerun = 0
	for i := range straight6s {
		for j := range straight6s[i] {
			demographic[straight6s[i][j]]++
		}
	}
	//fmt.Println(straight6s)
	for s := range demographic {
		if demographic[s] >= 5 {
			rerun = 1
		}
	}
	var Rest [][]int
	if rerun == 0 {
		for i := 3; i <= 15; i++ {
			inversedemographic[i] = 4 - demographic[i]
		}
		//剩餘牌組容易產生四連對/三連對／三帶一/三帶二/順子五張
		RestPart := typeCollector(inversedemographic)
		updateDemographic(RestPart, inversedemographic)
		RestPart2 := typeCollector(inversedemographic)
		updateDemographic(RestPart2, inversedemographic)
		RestPart3 := typeCollector(inversedemographic)
		Rest = [][]int{RestPart, RestPart2, RestPart3}
		return straight6s, Rest
	}
	return Straight6Finder()
}

func Straigh6PairStraightOpenningTricked() []hand {
	straightPart, otherPart := Straight6Finder()
	fmt.Println(straightPart, otherPart)
	var lengthMap = make(map[int][]int)
	var lengthRank []int
	var usedMap = make(map[int]int)
	for i := range otherPart {
		lengthMap[len(otherPart[i])] = append(lengthMap[len(otherPart[i])], i)
		usedMap[len(otherPart[i])]++
		lengthRank = append(lengthRank, len(otherPart[i]))
	}
	sort.Slice(lengthRank, func(i, j int) bool { return lengthRank[i] > lengthRank[j] })
	//fmt.Println(lengthMap, lengthRank)
	//準備填入花色
	var thisSuit = make(map[int]int, 13)
	for i := 3; i <= 15; i++ {
		thisSuit[i] = 0
	}
	var playerCards = make([]hand, 3)

	for j := range straightPart[:3] {
		for k := range straightPart[:3][j] {
			playerCards[0].Poker = append(playerCards[0].Poker, (thisSuit[straightPart[:3][j][k]]+1)*SuitShift+straightPart[:3][j][k])
			thisSuit[straightPart[:3][j][k]]++
		}
	}

	for k := range straightPart[3] {
		playerCards[1].Poker = append(playerCards[1].Poker, (thisSuit[straightPart[3][k]]+1)*SuitShift+straightPart[3][k])
		thisSuit[straightPart[3][k]]++
	}

	for j := range straightPart[4:] {
		for k := range straightPart[4:][j] {
			playerCards[2].Poker = append(playerCards[2].Poker, (thisSuit[straightPart[4:][j][k]]+1)*SuitShift+straightPart[4:][j][k])
			thisSuit[straightPart[4:][j][k]]++
		}
	}

	//fmt.Println(usedMap)
	for i := 0; i < 3; i++ {
		if roleCards(i)-lengthRank[i] >= len(playerCards[i].Poker) && lengthRank[i] != 0 {
			for k := range otherPart[lengthMap[lengthRank[i]][usedMap[lengthRank[i]]-1]] {
				playerCards[i].Poker = append(playerCards[i].Poker, (thisSuit[otherPart[lengthMap[lengthRank[i]][usedMap[lengthRank[i]]-1]][k]]+1)*SuitShift+otherPart[lengthMap[lengthRank[i]][usedMap[lengthRank[i]]-1]][k])
				thisSuit[otherPart[lengthMap[lengthRank[i]][usedMap[lengthRank[i]]-1]][k]]++
			}
			//fmt.Println(otherPart[lengthMap[lengthRank[i]][usedMap[lengthRank[i]]-1]])
			usedMap[lengthRank[i]]--
		}
		//fmt.Println(playerCards[i].Poker)
	}
	var usedPoints = make(map[int]int, 13)
	for i := 3; i <= 15; i++ {
		usedPoints[i] = 0
	}
	for i := range playerCards {
		for j := range playerCards[i].Poker {
			usedPoints[playerCards[i].Poker[j]%SuitShift]++
		}
	}
	fmt.Println(usedPoints)
	var restCards []int
	for s := range usedPoints {
		for usedPoints[s] < 4 {
			restCards = append(restCards, (usedPoints[s]+1)*SuitShift+s)
			usedPoints[s]++
		}
	}
	restCards = append(restCards, 50030)
	restCards = append(restCards, 50031)
	//fmt.Println(checkExclusivity(playerCards))
	//fmt.Println(restCards)
	var src cryptoSource
	rnd := rand.New(src)
	rand.Seed(rnd.Int63n(time.Now().UnixNano()))
	rand.Shuffle(len(restCards), func(i, j int) { restCards[i], restCards[j] = restCards[j], restCards[i] })
	for i := 0; i <= 2; i++ {
		var PatchedCards = roleCards(i) - len(playerCards[i].Poker)
		//fmt.Println(PatchedCards)
		for s := range restCards[:PatchedCards] {
			playerCards[i].Poker = append(playerCards[i].Poker, restCards[s])
		}
		playerCards[i] = CardTypeConstructor(playerCards[i])
		printPokerInFormat(playerCards[i].Poker, i)
		restCards = restCards[PatchedCards:]
	}
	if !checkExclusivity(playerCards) {
		var pointchecker = make(map[int]int)
		for i := range playerCards {
			for j := range playerCards[i].Poker {
				pointchecker[playerCards[i].Poker[j]%SuitShift]++
			}
		}
		fmt.Println(pointchecker)
	}

	return playerCards
}

func Straigh6PairStraightOpenning() []hand {
	straightPart, otherPart := Straight6Finder()
	//fmt.Println(straightPart, otherPart)
	//straightPart = [][]int{{4, 5, 6, 7, 8, 9}, {3, 4, 5, 6, 7, 8}, {10, 11, 12, 13, 14, 15}, {9, 10, 11, 12, 13, 14}, {10, 11, 12, 13, 14, 15}, {10, 11, 12, 13, 14, 15}}
	//otherPart = [][]int{{3, 3, 4, 4, 5, 5, 6, 6}, {7, 7, 8, 8, 9, 9}, {}}
	fmt.Println(straightPart, otherPart)
	var lengthMap = make(map[int][]int)
	var lengthRank []int
	var usedMap = make(map[int]int)
	for i := range otherPart {
		lengthMap[len(otherPart[i])] = append(lengthMap[len(otherPart[i])], i)
		usedMap[len(otherPart[i])]++
		lengthRank = append(lengthRank, len(otherPart[i]))
	}
	sort.Slice(lengthRank, func(i, j int) bool { return lengthRank[i] > lengthRank[j] })
	//fmt.Println(lengthMap, lengthRank)
	//準備填入花色
	var thisSuit = make(map[int]int, 13)
	for i := 3; i <= 15; i++ {
		thisSuit[i] = 0
	}
	var playerCards = make([]hand, 3)

	for i := 0; i < 3; i++ {
		for j := range straightPart[i*2 : (i+1)*2] {
			//fmt.Println(straightPart[i*2 : (i+1)*2][j])
			for k := range straightPart[i*2 : (i+1)*2][j] {
				playerCards[i].Poker = append(playerCards[i].Poker, (thisSuit[straightPart[i*2 : (i+1)*2][j][k]]+1)*SuitShift+straightPart[i*2 : (i+1)*2][j][k])
				thisSuit[straightPart[i*2 : (i+1)*2][j][k]]++
			}
		}
	}
	//fmt.Println(usedMap)
	for i := 0; i < 3; i++ {
		if roleCards(i)-lengthRank[i] >= len(playerCards[i].Poker) && lengthRank[i] != 0 {
			for k := range otherPart[lengthMap[lengthRank[i]][usedMap[lengthRank[i]]-1]] {
				playerCards[i].Poker = append(playerCards[i].Poker, (thisSuit[otherPart[lengthMap[lengthRank[i]][usedMap[lengthRank[i]]-1]][k]]+1)*SuitShift+otherPart[lengthMap[lengthRank[i]][usedMap[lengthRank[i]]-1]][k])
				thisSuit[otherPart[lengthMap[lengthRank[i]][usedMap[lengthRank[i]]-1]][k]]++
			}
			//fmt.Println(otherPart[lengthMap[lengthRank[i]][usedMap[lengthRank[i]]-1]])
			usedMap[lengthRank[i]]--
		}
		//fmt.Println(playerCards[i].Poker)
	}
	var usedPoints = make(map[int]int, 13)
	for i := 3; i <= 15; i++ {
		usedPoints[i] = 0
	}
	for i := range playerCards {
		for j := range playerCards[i].Poker {
			usedPoints[playerCards[i].Poker[j]%SuitShift]++
		}
	}
	fmt.Println(usedPoints)
	var restCards []int
	for s := range usedPoints {
		for usedPoints[s] < 4 {
			restCards = append(restCards, (usedPoints[s]+1)*SuitShift+s)
			usedPoints[s]++
		}
	}
	restCards = append(restCards, 50030)
	restCards = append(restCards, 50031)
	//fmt.Println(checkExclusivity(playerCards))
	//fmt.Println(restCards)
	var src cryptoSource
	rnd := rand.New(src)
	rand.Seed(rnd.Int63n(time.Now().UnixNano()))
	rand.Shuffle(len(restCards), func(i, j int) { restCards[i], restCards[j] = restCards[j], restCards[i] })
	for i := 0; i <= 2; i++ {
		var PatchedCards = roleCards(i) - len(playerCards[i].Poker)
		//fmt.Println(PatchedCards)
		for s := range restCards[:PatchedCards] {
			playerCards[i].Poker = append(playerCards[i].Poker, restCards[s])
		}
		playerCards[i] = CardTypeConstructor(playerCards[i])
		printPokerInFormat(playerCards[i].Poker, i)
		restCards = restCards[PatchedCards:]
	}
	if !checkExclusivity(playerCards) {
		var pointchecker = make(map[int]int)
		for i := range playerCards {
			for j := range playerCards[i].Poker {
				pointchecker[playerCards[i].Poker[j]%SuitShift]++
			}
		}
		fmt.Println(pointchecker)
	}

	return playerCards
}

func updateDemographic(RestPart []int, demographic map[int]int) map[int]int {
	for i := range RestPart {
		demographic[RestPart[i]]--
	}
	return demographic
}

func typeCollector(inversedemographic map[int]int) []int {
	//var sequence []int
	//for i := range inversedemographic {
	//	sequence = append(sequence, i)
	//}
	//sort.Slice(sequence, func(i, j int) bool { return sequence[i] < sequence[j] })
	//fmt.Println(sequence)
	for i := 3; i < len(inversedemographic); i++ {
		if inversedemographic[i] >= 2 && inversedemographic[i+1] >= 2 && inversedemographic[i+2] >= 2 && inversedemographic[i+3] >= 2 {
			//fmt.Println(i, i+1, i+2, i+3)
			return []int{i, i, i + 1, i + 1, i + 2, i + 2, i + 3, i + 3}
		}
		if inversedemographic[i] >= 2 && inversedemographic[i+1] >= 2 && inversedemographic[i+2] >= 2 {
			//fmt.Println(i, i+1, i+2)
			return []int{i, i, i + 1, i + 1, i + 2, i + 2}
		}
		if inversedemographic[i] >= 1 && inversedemographic[i+1] >= 1 && inversedemographic[i+2] >= 1 && inversedemographic[i+3] >= 1 && inversedemographic[i+4] >= 1 {
			//fmt.Println(i, i+1, i+2, i+3, i+4)
			return []int{i, i + 1, i + 2, i + 3, i + 4}
		}
		if inversedemographic[i] >= 3 {
			return []int{i, i, i}
		}
	}
	return nil
}

func StraightPlaneBombProtoTypeGen() [][]int {
	var demographic = make(map[int]int, 13)
	var inversedemographic = make(map[int]int, 13)
	for i := 3; i <= 15; i++ {
		demographic[i] = 0
	}
	var src cryptoSource
	rnd := rand.New(src)
	rand.Seed(rnd.Int63n(time.Now().UnixNano()))
	var playerGet = make([][]int, 3)
	for i := 0; i < 3; i++ {
		ini := rand.Intn(5) + 3
		var count = 0
		for count < 8 {
			playerGet[i] = append(playerGet[i], ini+count)
			count++
		}
		for s := range playerGet[i] {
			demographic[playerGet[i][s]]++
		}
	}
	for i := 3; i <= 15; i++ {
		inversedemographic[i] = 4 - demographic[i]
	}

	//playerGet = [][]int{{3, 4, 5, 6, 7, 8, 9, 10}, {7, 8, 9, 10, 11, 12, 13, 14}, {3, 4, 5, 6, 7, 8, 9, 10}}
	//for i := 0; i < 3; i++ {
	//	for s := range playerGet[i] {
	//		demographic[playerGet[i][s]]++
	//	}
	//}
	for i := 3; i <= 15; i++ {
		inversedemographic[i] = 4 - demographic[i]
	}
	var MakingPlanes, MakingBombs = make([][]int, 0), make([][]int, 0)
	for i := 3; i <= 15; i++ {
		if inversedemographic[i] == 3 {
			MakingPlanes = append(MakingPlanes, []int{i, i, i})
		} else if inversedemographic[i] == 4 {
			MakingBombs = append(MakingBombs, []int{i, i, i, i})
		}
	}
	if len(duplicateSequence(inversedemographic, 4)) >= 3 {
		if len(duplicateSequenceForOpening(inversedemographic, 3)) != 0 {
			for j := range duplicateSequenceForOpening(inversedemographic, 3)[0] {
				playerGet[0] = append(playerGet[0], duplicateSequenceForOpening(inversedemographic, 3)[0][j])
				playerGet[0] = append(playerGet[0], duplicateSequenceForOpening(inversedemographic, 3)[0][j])
				playerGet[0] = append(playerGet[0], duplicateSequenceForOpening(inversedemographic, 3)[0][j])
			}
		}
		for i := 0; i < 3; i++ {
			for j := range MakingBombs[len(MakingBombs)-i-1] {
				playerGet[i] = append(playerGet[i], MakingBombs[len(MakingBombs)-i-1][j])
			}
		}
	} else if len(duplicateSequenceForOpening(inversedemographic, 3)) >= 1 && len(MakingBombs) == 2 {
		var count = 1
		for i := 0; i < 3; i++ {
			if i == 1 {
				for j := range duplicateSequenceForOpening(inversedemographic, 3)[0] {
					playerGet[i] = append(playerGet[i], duplicateSequenceForOpening(inversedemographic, 3)[0][j])
					playerGet[i] = append(playerGet[i], duplicateSequenceForOpening(inversedemographic, 3)[0][j])
					playerGet[i] = append(playerGet[i], duplicateSequenceForOpening(inversedemographic, 3)[0][j])
				}
			} else if i != 1 {
				for j := range MakingBombs[count] {
					playerGet[i] = append(playerGet[i], MakingBombs[count][j])
				}
				count--
			}
		}
	} else if len(duplicateSequenceForOpening(inversedemographic, 3)) >= 3 && len(MakingBombs) == 1 {
		//fmt.Println(playerGet, duplicateSequenceForOpening(inversedemographic, 3), MakingBombs)
		var count = 0
		for i := 0; i < 3; i++ {
			if i == 1 {
				for j := range duplicateSequenceForOpening(inversedemographic, 3)[0] {
					playerGet[i] = append(playerGet[i], duplicateSequenceForOpening(inversedemographic, 3)[0][j])
					playerGet[i] = append(playerGet[i], duplicateSequenceForOpening(inversedemographic, 3)[0][j])
					playerGet[i] = append(playerGet[i], duplicateSequenceForOpening(inversedemographic, 3)[0][j])
				}
			} else if i == 2 {
				for j := range MakingBombs[count] {
					playerGet[i] = append(playerGet[i], MakingBombs[count][j])
				}
			} else {
				for j := range duplicateSequenceForOpening(inversedemographic, 3)[2] {
					playerGet[i] = append(playerGet[i], duplicateSequenceForOpening(inversedemographic, 3)[2][j])
					playerGet[i] = append(playerGet[i], duplicateSequenceForOpening(inversedemographic, 3)[2][j])
					playerGet[i] = append(playerGet[i], duplicateSequenceForOpening(inversedemographic, 3)[2][j])
				}
			}
		}
	}
	return playerGet
}

func StraightPlaneBombOpening() []hand {
	PlayerComp := StraightPlaneBombProtoTypeGen()
	//準備填入花色
	var thisSuit = make(map[int]int, 13)
	for i := 3; i <= 15; i++ {
		thisSuit[i] = 0
	}
	var playerCards = make([]hand, 3)
	for i := 0; i < 3; i++ {
		for j := range PlayerComp[i] {
			playerCards[i].Poker = append(playerCards[i].Poker, (thisSuit[PlayerComp[i][j]]+1)*SuitShift+PlayerComp[i][j])
			thisSuit[PlayerComp[i][j]]++
		}
	}
	var usedPoints = make(map[int]int, 13)
	for i := 3; i <= 15; i++ {
		usedPoints[i] = 0
	}
	for j := range PlayerComp {
		for k := range PlayerComp[j] {
			usedPoints[PlayerComp[j][k]]++
		}
	}
	var restCards []int
	for s := range usedPoints {
		for usedPoints[s] < 4 {
			restCards = append(restCards, (usedPoints[s]+1)*SuitShift+s)
			usedPoints[s]++
		}
	}
	restCards = append(restCards, 50030)
	restCards = append(restCards, 50031)
	var src cryptoSource
	rnd := rand.New(src)
	rand.Seed(rnd.Int63n(time.Now().UnixNano()))
	rand.Shuffle(len(restCards), func(i, j int) { restCards[i], restCards[j] = restCards[j], restCards[i] })
	for i := 0; i <= 2; i++ {
		thisLeng := len(PlayerComp[i])
		if i == 0 {
			playerCards[i].roleType = "地主"
		} else {
			playerCards[i].roleType = "農夫"
		}
		for s := range restCards[:roleCards(i)-thisLeng] {
			playerCards[i].Poker = append(playerCards[i].Poker, restCards[:roleCards(i)-thisLeng][s])
		}
		playerCards[i] = CardTypeConstructor(playerCards[i])
		printPokerInFormat(playerCards[i].Poker, i)
		restCards = restCards[roleCards(i)-thisLeng:]
	}
	return playerCards
}

func PlaneProtoTypeGen(layer int) [][]int {
	var sq []int
	for i := 3; i <= 15; i++ {
		sq = append(sq, i)
	}
	var planebody [][]int
	for i := 0; i < int(math.Floor(float64(13.0/layer))); i++ {
		planebody = append(planebody, sq[layer*i:layer*i+layer])
	}
	return planebody
}

func PlaneOpening(layer int) []hand {
	planebody := PlaneProtoTypeGen(layer)
	var src cryptoSource
	rnd := rand.New(src)
	rand.Seed(rnd.Int63n(time.Now().UnixNano()))
	rand.Shuffle(len(planebody), func(i, j int) { planebody[i], planebody[j] = planebody[j], planebody[i] })
	c := len(planebody) / 3
	var playerGet = make([][][]int, 3)
	for i := 0; i < 3; i++ {
		playerGet[i] = planebody[i*c : i*c+c]
	}
	var usedPoints []int
	var restCards []int
	var playerCards = make([]hand, 3)
	for i := 0; i < 3; i++ {
		for j := range playerGet[i] {
			for k := range playerGet[i][j] {
				usedPoints = append(usedPoints, playerGet[i][j][k])
				for s := 1; s <= 3; s++ {
					playerCards[i].Poker = append(playerCards[i].Poker, s*SuitShift+playerGet[i][j][k])
				}
				restCards = append(restCards, 4*SuitShift+playerGet[i][j][k])
			}
		}
	}
	for i := 3; i <= 15; i++ {
		if notIn(i, usedPoints) {
			for s := 1; s <= 4; s++ {
				restCards = append(restCards, s*SuitShift+i)
			}
		}
	}

	restCards = append(restCards, 50030)
	restCards = append(restCards, 50031)
	rand.Seed(rnd.Int63n(time.Now().UnixNano()))
	rand.Shuffle(len(restCards), func(i, j int) { restCards[i], restCards[j] = restCards[j], restCards[i] })
	for i := 0; i <= 2; i++ {
		thisLeng := len(playerCards[i].Poker)
		if i == 0 {
			playerCards[i].roleType = "地主"
		} else {
			playerCards[i].roleType = "農夫"
		}
		for s := range restCards[:roleCards(i)-thisLeng] {
			playerCards[i].Poker = append(playerCards[i].Poker, restCards[:roleCards(i)-thisLeng][s])
		}
		//printPokerInFormat(playerCards[i].Poker, i)
		//fmt.Println(getCardsName(playerCards[i].Poker))
		restCards = restCards[roleCards(i)-thisLeng:]
	}
	return playerCards
}
