package awardcentertest

import (
	"fmt"
	"math"
	"sort"
	"time"
)

func childNodeGen(tree []Layer, thisNode Nodes) {
	for e := range thisNode.ChildInfo {
		fmt.Println("--- ChildNode", tree[thisNode.ChildInfo[e]%1000].Nodes[thisNode.ChildInfo[e]/1000])
	}
}

//func (t *Layer) isoNodeGen(thisNode Nodes) {
//	if thisNode.Isopoint != -999 {
//		fmt.Println("--- IsoNode", t.Nodes[thisNode.Isopoint/1000])
//	}
//}

func retrieveInvOutcome(tree []Layer, thisLayer []Nodes, inventory map[InvNodeKeyBeta]Isopoints) {
	for i := range thisLayer {
		fmt.Println(thisLayer[i])
		//childNodeGen(tree, thisLayer[i])
		//isoNodeGen(thisLayer[i])
	}
}

//backwardsFilling 反向填入節點結局
func bfWOi(tree []Layer) {
	//this for loop, max has around 150 elements, Order of magnitude = 2
	for i := len(tree) - 3; i >= 1; i-- {
		if len(tree[i].Nodes) != 0 {
			//this for loop, max elements 3 to 10-folds per extra options
			//heavy calculations, mainly should be this part
			for e := range tree[i-1].Nodes {
				var thisNodeOutcome [][]int
				childRange := tree[i-1].Nodes[e].ChildInfo
				if len(childRange) != 0 {
					if len(childRange) == 1 {
						thisNodeOutcome = append(thisNodeOutcome, tree[i].Nodes[childRange[0]/1000].Outcome)
					} else if len(childRange) > 1 {
						//this loop has 2~ few elements per node for last loop
						for j := range tree[i].Nodes[childRange[0]/1000 : childRange[0]/1000+len(childRange)] {
							thisNodeOutcome = append(thisNodeOutcome, tree[i].Nodes[childRange[0]/1000 : childRange[0]/1000+len(childRange)][j].Outcome)
						}
					}
				}
				distinct := duplicatesRemover(thisNodeOutcome)
				if len(distinct) == 1 {
					tree[i-1].Nodes[e].Outcome = distinct[0]
				} else if len(distinct) == 2 && (tree[i-1].Nodes[e].Order)%3 != 0 { //下一手非地主
					tree[i-1].Nodes[e].Outcome = []int{0, 1}
				} else if len(distinct) == 2 && (tree[i-1].Nodes[e].Order)%3 == 0 { //下一手為地主
					tree[i-1].Nodes[e].Outcome = []int{1, 0}
				}
			}
		}
	}
}

func dummy(beta map[InvNodeKeyBeta]Isopoints) {

}

func emptinessCheck(get []int) bool {
	if len(get) == 0 {
		return true
	}
	return false
}

func DoubleEmptinessCheck(get []int) {
	if len(get) == 0 {
		fmt.Println("IT's EMPTY, Again!!", get)
	}
}

func CompleteFilledCheck(NodesOutCome Nodes) {
	if len(NodesOutCome.Outcome) == 0 {
		fmt.Println(NodesOutCome)
	}
}

//func CompleteFilledCheck(NodesOutCome Nodes, tree []Layer) {
//	if len(NodesOutCome.Outcome) == 0 {
//		fmt.Println(NodesOutCome)
//	}
//}

//backwardsFilling 反向填入節點結局
func backwardsFilling(tree []Layer) {
	defer timeTrack(time.Now(), "Backwards induction Process")
	//this for loop, max has around 150 elements, Order of magnitude = 2
	//從最底層往上倒推上一層的結果
	for i := len(tree) - 2; i >= 1; i-- {
		//往上推的低層節點數不為零
		if len(tree[i].Nodes) != 0 {
			//把這一層的節點結果推完
			for e := range tree[i-1].Nodes {
				var thisNodeOutcome [][]int
				childRange := tree[i-1].Nodes[e].ChildInfo
				//fmt.Println(childRange)
				if len(childRange) != 0 {
					if len(childRange) == 1 {
						thisNodeOutcome = append(thisNodeOutcome, tree[i].Nodes[childRange[0]/1000].Outcome)
					} else if len(childRange) > 1 {
						//this loop has 2~ few elements per node for last loop
						for j := range childRange {
							if len(tree[i].Nodes[childRange[0]/1000 : childRange[0]/1000+len(childRange)][j].Outcome) != 0 {
								thisNodeOutcome = append(thisNodeOutcome, tree[i].Nodes[childRange[0]/1000 : childRange[0]/1000+len(childRange)][j].Outcome)
							}
						}
					}
					distinct := duplicatesRemover(thisNodeOutcome)
					if len(distinct) == 1 {
						if len(distinct[0]) != 0 {
							tree[i-1].Nodes[e].Outcome = distinct[0]
						}
					} else if len(distinct) == 2 && (tree[i-1].Nodes[e].Order)%3 != 0 { //下一手非地主
						tree[i-1].Nodes[e].Outcome = []int{0, 1}
					} else if len(distinct) == 2 && (tree[i-1].Nodes[e].Order)%3 == 0 { //下一手為地主
						tree[i-1].Nodes[e].Outcome = []int{1, 0}
					}
				}
				CompleteFilledCheck(tree[i-1].Nodes[e])
			}
		}
	}
}

func (t *Layer) iteratedGeneratingProcess(tree []Layer, lastStepNodes []Nodes, inventory map[InvNodeKeyBeta]Isopoints, l int, role int) {
	var nodeCount = 0
	for thisNode := range lastStepNodes {
		//fmt.Println(bool(lastStepNodes[thisNode].Index == tree.Nodes[lastNodesIndices[thisNode]].Index))
		total := getNodeRestCardsTotal(lastStepNodes[thisNode].RestCards)
		if len(lastStepNodes[thisNode].Outcome) == 0 && !checkingInventoryBeta(tree, lastStepNodes[thisNode], inventory, total) {
			//fillingToInventoryBeta(lastStepNodes[thisNode], inventory)
			//and parsing inventory not in
			var parentNodeIndex = lastStepNodes[thisNode].Index
			var thisHand hand
			thisHand.Poker = lastStepNodes[thisNode].RestCards[role]
			handCards := tree[l-1].newNodesGen(CardTypeConstructor(thisHand), lastStepNodes[thisNode].CardSetInfo.CardSet, lastStepNodes[thisNode].RestCards, l, parentNodeIndex) //組成這一手玩家的節點
			//檢視下層節點是否可以連結到母節點 parent node
			for s := 0; s < len(handCards); s++ {
				if lastStepNodes[thisNode].PassCount == 2 {
					if lastStepNodes[thisNode].CardSetInfo.CardTypeSerial == -1 { //開局：前一手是PASS且累積兩次PASS
						handCards[s].Index = nodeCount*1000 + l
						nodeCount++
						lastStepNodes[thisNode].ChildInfo = append(lastStepNodes[thisNode].ChildInfo, handCards[s].Index)
						tree[l].Nodes = append(tree[l].Nodes, handCards[s])
						//fillingIsoNodeToInventory(handCards[s], inventory)
					} else if handCards[s].CardSetInfo.CardTypeSerial != -1 {
						handCards[s].Index = nodeCount*1000 + l
						nodeCount++
						lastStepNodes[thisNode].ChildInfo = append(lastStepNodes[thisNode].ChildInfo, handCards[s].Index)
						tree[l].Nodes = append(tree[l].Nodes, handCards[s])
						//fillingIsoNodeToInventory(handCards[s], inventory)
					}
				} else if handCards[s].CardSetInfo.CardTypeSerial == lastStepNodes[thisNode].CardSetInfo.CardTypeSerial && handCards[s].CardSetInfo.CardTypeSerial != -1 { //同性質的牌組非PASS
					if lastStepNodes[thisNode].PassCount < 2 && typeComparision(handCards[s].CardSetInfo.CardSet, lastStepNodes[thisNode].CardSetInfo.CardSet, handCards[s].CardSetInfo.CardTypeSerial) {
						//前一節點PASS累積數少於2-->比較排型大小
						handCards[s].Index = nodeCount*1000 + l
						nodeCount++
						lastStepNodes[thisNode].ChildInfo = append(lastStepNodes[thisNode].ChildInfo, handCards[s].Index)
						tree[l].Nodes = append(tree[l].Nodes, handCards[s])
						//fillingIsoNodeToInventory(handCards[s], inventory)
					} else if lastStepNodes[thisNode].PassCount == 2 {
						//前一節點PASS累積數＝2-->可直接出下一個
						handCards[s].Index = nodeCount*1000 + l
						nodeCount++
						lastStepNodes[thisNode].ChildInfo = append(lastStepNodes[thisNode].ChildInfo, handCards[s].Index)
						tree[l].Nodes = append(tree[l].Nodes, handCards[s])
						//fillingIsoNodeToInventory(handCards[s], inventory)
					}
				} else if handCards[s].CardSetInfo.CardTypeSerial == 3 && (lastStepNodes[thisNode].CardSetInfo.CardTypeSerial != 3 && lastStepNodes[thisNode].CardSetInfo.CardTypeSerial != 4) {
					//炸彈
					handCards[s].Index = nodeCount*1000 + l
					nodeCount++
					lastStepNodes[thisNode].ChildInfo = append(lastStepNodes[thisNode].ChildInfo, handCards[s].Index)
					tree[l].Nodes = append(tree[l].Nodes, handCards[s])
					//fillingIsoNodeToInventory(handCards[s], inventory)
				} else if handCards[s].CardSetInfo.CardTypeSerial == 4 {
					//王炸
					handCards[s].Index = nodeCount*1000 + l
					nodeCount++
					lastStepNodes[thisNode].ChildInfo = append(lastStepNodes[thisNode].ChildInfo, handCards[s].Index)
					tree[l].Nodes = append(tree[l].Nodes, handCards[s])
					//fillingIsoNodeToInventory(handCards[s], inventory)
				} else if handCards[s].CardSetInfo.CardTypeSerial == -1 && lastStepNodes[thisNode].PassCount < 2 {
					//這一手PASS，總PASS數<=2 (還沒繞一輪) 繼承前一手的牌型
					handCards[s].CardSetInfo.CardSet = lastStepNodes[thisNode].CardSetInfo.CardSet
					handCards[s].CardSetInfo.CardTypeSerial = lastStepNodes[thisNode].CardSetInfo.CardTypeSerial
					handCards[s].Index = nodeCount*1000 + l
					handCards[s].PassCount = lastStepNodes[thisNode].PassCount + 1
					nodeCount++
					lastStepNodes[thisNode].ChildInfo = append(lastStepNodes[thisNode].ChildInfo, handCards[s].Index)
					tree[l].Nodes = append(tree[l].Nodes, handCards[s])
					//fillingIsoNodeToInventory(handCards[s], inventory)
				}
			}
		}
	}
}

func threePartyEqual(thisNodeRestCards map[int][]int, inventoryComparedRestCards map[int][]int) bool {
	var checkall = 1
	for j := 0; j < 3; j++ {
		if len(thisNodeRestCards[j]) == len(inventoryComparedRestCards[j]) {
			checkall *= 1
		} else {
			checkall *= 0
		}
	}
	if checkall == 1 {
		return true
	}
	return false

}

//使得牌組跟所對應的Key是一對一映射
func convertSet2Code(numberSet []int) float64 {
	var code float64
	for j := 0; j < len(numberSet); j++ {
		if numberSet[j]/10 != 3 {
			code += float64(numberSet[j]) * math.Pow(13, float64(j))
		} else {
			code += float64(numberSet[j]%10+1) * math.Pow(2, -2)
		}
	}
	return code
}

func convertHandCard2Code(thisHand []int) float64 {
	var numberSet []int
	for i := range thisHand {
		numberSet = append(numberSet, thisHand[i]%10000)
	}
	sort.Ints(numberSet)
	return convertSet2Code(numberSet)
}

func node2Key(thisNode Nodes) InvNodeKeyBeta {
	HouseCode := convertHandCard2Code(thisNode.RestCards[0])
	Farmer1Code := convertHandCard2Code(thisNode.RestCards[1])
	Farmer2Code := convertHandCard2Code(thisNode.RestCards[2])
	CardSetCode := convertHandCard2Code(thisNode.CardSetInfo.CardSet)
	//thisKey := InvNodeKeyBeta{thisNode.Order % 3, thisNode.PassCount, CardSetCode, thisNode.CardSetInfo.CardTypeSerial, HouseCode, Farmer1Code, Farmer2Code}
	//return thisKey
	if thisNode.PassCount != 2 {
		thisKey := InvNodeKeyBeta{(thisNode.Order - 1) % 3, thisNode.PassCount, CardSetCode, thisNode.CardSetInfo.CardTypeSerial, HouseCode, Farmer1Code, Farmer2Code}
		return thisKey
	}
	thisKey := InvNodeKeyBeta{thisNode.Order % 3, thisNode.PassCount, 0, thisNode.CardSetInfo.CardTypeSerial, HouseCode, Farmer1Code, Farmer2Code}
	return thisKey
}

func checkingInventoryBeta(tree []Layer, thisNode Nodes, inventory map[InvNodeKeyBeta]Isopoints, totalCards int) bool {
	if totalCards > 14 {
		//passCount ==2 不需要考慮目前節點牌面 ALL=0
		//passCount!=2 需要考慮目前節點牌面
		thisKey := node2Key(thisNode)
		if _, exists := inventory[thisKey]; exists {
			var x = inventory[thisKey]
			x.matchingTimes += 1
			inventory[thisKey] = x
			//fmt.Println("Before", tree[thisNode.Index%1000].Nodes[thisNode.Index/1000].Isopoint)
			//var y =tree[thisNode.Index%1000]
			//y.Nodes[thisNode.Index/1000].Isopoint=x.Index
			//tree[thisNode.Index%1000].Nodes[thisNode.Index/1000].Isopoint = x.Index
			//fmt.Println(tree[thisNode.Index%1000].Nodes[thisNode.Index/1000], "|||", inventory[thisKey])
			return true
		}

		thisIsoNode := Isopoints{thisNode.Index, thisNode.CardSetInfo.CardSet, thisNode.RestCards, thisNode.Outcome, 0}
		inventory[thisKey] = thisIsoNode

		return false
	}
	return false
}

func fillingToInventoryBeta(thisNode Nodes, inventory map[InvNodeKeyBeta]Isopoints) {
	thisKey := node2Key(thisNode)
	thisIsoNode := Isopoints{thisNode.Index, thisNode.CardSetInfo.CardSet, thisNode.RestCards, thisNode.Outcome, 0}
	if inventory[thisKey].matchingTimes > 0 {
		var x = inventory[thisKey]
		x.matchingTimes += 1
		inventory[thisKey] = x
		//inventory[thisKey] = append(inventory[thisKey], thisIsoNode)
	} else {
		inventory[thisKey] = thisIsoNode
	}
}

func (t *Layer) iGPBrutal(tree []Layer, lastStepNodes []Nodes, l int, role int) {
	var nodeCount = 0
	for thisNode := range lastStepNodes {
		//fmt.Println(bool(lastStepNodes[thisNode].Index == tree.Nodes[lastNodesIndices[thisNode]].Index))
		if len(lastStepNodes[thisNode].Outcome) == 0 {
			//and parsing inventory not in
			var parentNodeIndex = lastStepNodes[thisNode].Index
			var thisHand hand
			thisHand.Poker = lastStepNodes[thisNode].RestCards[role%3]
			handCards := tree[l-1].newNodesGen(CardTypeConstructor(thisHand), lastStepNodes[thisNode].CardSetInfo.CardSet, lastStepNodes[thisNode].RestCards, l, parentNodeIndex) //組成這一手玩家的節點
			for s := 0; s < len(handCards); s++ {
				if lastStepNodes[thisNode].PassCount == 2 {
					if lastStepNodes[thisNode].CardSetInfo.CardTypeSerial == -1 { //開局：前一手是PASS且累積兩次PASS
						handCards[s].Index = nodeCount*1000 + l
						nodeCount++
						lastStepNodes[thisNode].ChildInfo = append(lastStepNodes[thisNode].ChildInfo, handCards[s].Index)
						tree[l].Nodes = append(tree[l].Nodes, handCards[s])
						//fillingIsoNodeToInventory(handCards[s], inventory)
					} else if handCards[s].CardSetInfo.CardTypeSerial != -1 {
						handCards[s].Index = nodeCount*1000 + l
						nodeCount++
						lastStepNodes[thisNode].ChildInfo = append(lastStepNodes[thisNode].ChildInfo, handCards[s].Index)
						tree[l].Nodes = append(tree[l].Nodes, handCards[s])
						//fillingIsoNodeToInventory(handCards[s], inventory)
					}
				} else if handCards[s].CardSetInfo.CardTypeSerial == lastStepNodes[thisNode].CardSetInfo.CardTypeSerial && handCards[s].CardSetInfo.CardTypeSerial != -1 { //同性質的牌組非PASS
					if lastStepNodes[thisNode].PassCount < 2 && typeComparision(handCards[s].CardSetInfo.CardSet, lastStepNodes[thisNode].CardSetInfo.CardSet, handCards[s].CardSetInfo.CardTypeSerial) {
						//前一節點PASS累積數少於2-->比較排型大小
						handCards[s].Index = nodeCount*1000 + l
						nodeCount++
						lastStepNodes[thisNode].ChildInfo = append(lastStepNodes[thisNode].ChildInfo, handCards[s].Index)
						tree[l].Nodes = append(tree[l].Nodes, handCards[s])
						//fillingIsoNodeToInventory(handCards[s], inventory)
					} else if lastStepNodes[thisNode].PassCount == 2 {
						//前一節點PASS累積數＝2-->可直接出下一個
						handCards[s].Index = nodeCount*1000 + l
						nodeCount++
						lastStepNodes[thisNode].ChildInfo = append(lastStepNodes[thisNode].ChildInfo, handCards[s].Index)
						tree[l].Nodes = append(tree[l].Nodes, handCards[s])
						//fillingIsoNodeToInventory(handCards[s], inventory)
					}
				} else if handCards[s].CardSetInfo.CardTypeSerial == 3 && (lastStepNodes[thisNode].CardSetInfo.CardTypeSerial != 3 && lastStepNodes[thisNode].CardSetInfo.CardTypeSerial != 4) {
					//炸彈
					handCards[s].Index = nodeCount*1000 + l
					nodeCount++
					lastStepNodes[thisNode].ChildInfo = append(lastStepNodes[thisNode].ChildInfo, handCards[s].Index)
					tree[l].Nodes = append(tree[l].Nodes, handCards[s])
					//fillingIsoNodeToInventory(handCards[s], inventory)
				} else if handCards[s].CardSetInfo.CardTypeSerial == 4 {
					//王炸
					handCards[s].Index = nodeCount*1000 + l
					nodeCount++
					lastStepNodes[thisNode].ChildInfo = append(lastStepNodes[thisNode].ChildInfo, handCards[s].Index)
					tree[l].Nodes = append(tree[l].Nodes, handCards[s])
					//fillingIsoNodeToInventory(handCards[s], inventory)
				} else if handCards[s].CardSetInfo.CardTypeSerial == -1 && lastStepNodes[thisNode].PassCount < 2 {
					//這一手PASS，總PASS數<=2 (還沒繞一輪) 繼承前一手的牌型
					handCards[s].CardSetInfo.CardSet = lastStepNodes[thisNode].CardSetInfo.CardSet
					handCards[s].CardSetInfo.CardTypeSerial = lastStepNodes[thisNode].CardSetInfo.CardTypeSerial
					handCards[s].Index = nodeCount*1000 + l
					handCards[s].PassCount = lastStepNodes[thisNode].PassCount + 1
					nodeCount++
					lastStepNodes[thisNode].ChildInfo = append(lastStepNodes[thisNode].ChildInfo, handCards[s].Index)
					tree[l].Nodes = append(tree[l].Nodes, handCards[s])
					//fillingIsoNodeToInventory(handCards[s], inventory)
				}
			}
		}
	}
}

func (t *Layer) iGPShrinked(tree []Layer, lastStepNodes []Nodes, l int, role int) {
	var nodeCount = 0
	for thisNode := range lastStepNodes {
		//fmt.Println(bool(lastStepNodes[thisNode].Index == tree.Nodes[lastNodesIndices[thisNode]].Index))
		if len(lastStepNodes[thisNode].Outcome) == 0 {
			//and parsing inventory not in
			var parentNodeIndex = lastStepNodes[thisNode].Index
			var thisHand hand
			thisHand.Poker = lastStepNodes[thisNode].RestCards[role%3]
			//handCards := tree[l-1].newNodesGenWithCon(CardTypeConstructor(thisHand), lastStepNodes[thisNode].CardSetInfo.CardSet, lastStepNodes[thisNode].RestCards, l, parentNodeIndex) //組成這一手玩家的節點
			handCards := tree[l-1].newNodesGenWithCon(CardTypeConstructor(thisHand), lastStepNodes[thisNode].CardSetInfo.CardSet,
				lastStepNodes[thisNode].RestCards, l, parentNodeIndex, lastStepNodes[thisNode].PassCount,
				lastStepNodes[thisNode].CardSetInfo.CardTypeSerial, 1) //組成這一手玩家的節點
			for s := 0; s < len(handCards); s++ {
				if lastStepNodes[thisNode].PassCount == 2 {
					if lastStepNodes[thisNode].CardSetInfo.CardTypeSerial == -1 { //開局：前一手是PASS且累積兩次PASS
						handCards[s].Index = nodeCount*1000 + l
						nodeCount++
						lastStepNodes[thisNode].ChildInfo = append(lastStepNodes[thisNode].ChildInfo, handCards[s].Index)
						tree[l].Nodes = append(tree[l].Nodes, handCards[s])
						//fillingIsoNodeToInventory(handCards[s], inventory)
					} else if handCards[s].CardSetInfo.CardTypeSerial != -1 {
						handCards[s].Index = nodeCount*1000 + l
						nodeCount++
						lastStepNodes[thisNode].ChildInfo = append(lastStepNodes[thisNode].ChildInfo, handCards[s].Index)
						tree[l].Nodes = append(tree[l].Nodes, handCards[s])
						//fillingIsoNodeToInventory(handCards[s], inventory)
					}
				} else if handCards[s].CardSetInfo.CardTypeSerial == lastStepNodes[thisNode].CardSetInfo.CardTypeSerial && handCards[s].CardSetInfo.CardTypeSerial != -1 { //同性質的牌組非PASS
					if lastStepNodes[thisNode].PassCount < 2 && typeComparision(handCards[s].CardSetInfo.CardSet, lastStepNodes[thisNode].CardSetInfo.CardSet, handCards[s].CardSetInfo.CardTypeSerial) {
						//前一節點PASS累積數少於2-->比較排型大小
						handCards[s].Index = nodeCount*1000 + l
						nodeCount++
						lastStepNodes[thisNode].ChildInfo = append(lastStepNodes[thisNode].ChildInfo, handCards[s].Index)
						tree[l].Nodes = append(tree[l].Nodes, handCards[s])
						//fillingIsoNodeToInventory(handCards[s], inventory)
					} else if lastStepNodes[thisNode].PassCount == 2 {
						//前一節點PASS累積數＝2-->可直接出下一個
						handCards[s].Index = nodeCount*1000 + l
						nodeCount++
						lastStepNodes[thisNode].ChildInfo = append(lastStepNodes[thisNode].ChildInfo, handCards[s].Index)
						tree[l].Nodes = append(tree[l].Nodes, handCards[s])
						//fillingIsoNodeToInventory(handCards[s], inventory)
					}
				} else if handCards[s].CardSetInfo.CardTypeSerial == 3 && (lastStepNodes[thisNode].CardSetInfo.CardTypeSerial != 3 && lastStepNodes[thisNode].CardSetInfo.CardTypeSerial != 4) {
					//炸彈
					handCards[s].Index = nodeCount*1000 + l
					nodeCount++
					lastStepNodes[thisNode].ChildInfo = append(lastStepNodes[thisNode].ChildInfo, handCards[s].Index)
					tree[l].Nodes = append(tree[l].Nodes, handCards[s])
					//fillingIsoNodeToInventory(handCards[s], inventory)
				} else if handCards[s].CardSetInfo.CardTypeSerial == 4 {
					//王炸
					handCards[s].Index = nodeCount*1000 + l
					nodeCount++
					lastStepNodes[thisNode].ChildInfo = append(lastStepNodes[thisNode].ChildInfo, handCards[s].Index)
					tree[l].Nodes = append(tree[l].Nodes, handCards[s])
					//fillingIsoNodeToInventory(handCards[s], inventory)
				} else if handCards[s].CardSetInfo.CardTypeSerial == -1 && lastStepNodes[thisNode].PassCount < 2 {
					//這一手PASS，總PASS數<=2 (還沒繞一輪) 繼承前一手的牌型
					handCards[s].CardSetInfo.CardSet = lastStepNodes[thisNode].CardSetInfo.CardSet
					handCards[s].CardSetInfo.CardTypeSerial = lastStepNodes[thisNode].CardSetInfo.CardTypeSerial
					handCards[s].Index = nodeCount*1000 + l
					handCards[s].PassCount = lastStepNodes[thisNode].PassCount + 1
					nodeCount++
					lastStepNodes[thisNode].ChildInfo = append(lastStepNodes[thisNode].ChildInfo, handCards[s].Index)
					tree[l].Nodes = append(tree[l].Nodes, handCards[s])
					//fillingIsoNodeToInventory(handCards[s], inventory)
				}
			}
		}
	}
}
