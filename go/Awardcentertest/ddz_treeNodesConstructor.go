package awardcentertest

func nodeFillIn(thisHandPoker []int, CardType [][]int, i int, l int, count int, CardTypeSerial int, parentNodeIndex int) Nodes {
	var eachLayerElement Nodes
	eachLayerElement = contentFillIn(thisHandPoker, CardType, i, l, count, CardTypeSerial, parentNodeIndex) //newLayerNodes
	return eachLayerElement
}

func contentFillIn(thisHandPoker []int, CardType [][]int, i int, l int, count int, CardTypeSerial int, parentNodeIndex int) Nodes {
	newLayerNodes := Nodes{}
	newLayerNodes.CardSetInfo.CardSet = CardType[i]
	newLayerNodes.CardSetInfo.CardTypeSerial = CardTypeSerial
	newLayerNodes.Order = l
	newLayerNodes.ParentInfo = parentNodeIndex
	newLayerNodes.RestCards = make(map[int][]int, 3)
	for j := 0; j < len(thisHandPoker); j++ {
		if notIn(thisHandPoker[j], CardType[i]) {
			newLayerNodes.RestCards[(l-1)%3] = append(newLayerNodes.RestCards[(l-1)%3], thisHandPoker[j])
		}
	}
	return newLayerNodes
}

func (t *Layer) newNodesGen(h hand, received []int, inheritedRestCards map[int][]int, l int, PN int) []Nodes {
	var newLayer []Nodes
	var count = 0
	//node for pass
	if (l-1) != 0 || len(received) != 0 {
		newLayer = append(newLayer, nodeFillIn(h.Poker, h.void, 0, l, count, -1, PN))
		count++
	}
	//Nodes for singles
	for i := 0; i < len(h.singleCards); i++ {
		newLayer = append(newLayer, nodeFillIn(h.Poker, h.singleCards, i, l, count, 0, PN))
		count++
	}
	//Nodes for pairs
	for i := 0; i < len(h.pairs); i++ {
		newLayer = append(newLayer, nodeFillIn(h.Poker, h.pairs, i, l, count, 1, PN))
		count++
	}
	//Nodes for threes
	for i := 0; i < len(h.threes); i++ {
		newLayer = append(newLayer, nodeFillIn(h.Poker, h.threes, i, l, count, 2, PN))
		count++
	}
	//Nodes for bombs
	for i := 0; i < len(h.bombs); i++ {
		newLayer = append(newLayer, nodeFillIn(h.Poker, h.bombs, i, l, count, 3, PN))
		count++
	}
	//node for kBomb
	if len(h.kBomb) != 0 {
		newLayer = append(newLayer, nodeFillIn(h.Poker, h.kBomb, 0, l, count, 4, PN))
		count++

	}
	//Nodes for threeOnes
	for i := 0; i < len(h.threeOnes); i++ {
		newLayer = append(newLayer, nodeFillIn(h.Poker, h.threeOnes, i, l, count, 5, PN))
		count++
	}
	//Nodes for threeTwos
	for i := 0; i < len(h.threeTwos); i++ {
		newLayer = append(newLayer, nodeFillIn(h.Poker, h.threeTwos, i, l, count, 6, PN))
		count++
	}
	//Nodes for straight 5
	for i := 0; i < len(h.straight.straight5); i++ {
		newLayer = append(newLayer, nodeFillIn(h.Poker, h.straight.straight5, i, l, count, 7, PN))
		count++
	}
	//Nodes for straight 6
	for i := 0; i < len(h.straight.straight6); i++ {
		newLayer = append(newLayer, nodeFillIn(h.Poker, h.straight.straight6, i, l, count, 8, PN))
		count++
	}
	//Nodes for straight 7
	for i := 0; i < len(h.straight.straight7); i++ {
		newLayer = append(newLayer, nodeFillIn(h.Poker, h.straight.straight7, i, l, count, 9, PN))
		count++
	}
	//Nodes for straight 8
	for i := 0; i < len(h.straight.straight8); i++ {
		newLayer = append(newLayer, nodeFillIn(h.Poker, h.straight.straight8, i, l, count, 10, PN))
		count++
	}
	//Nodes for straight 9
	for i := 0; i < len(h.straight.straight9); i++ {
		newLayer = append(newLayer, nodeFillIn(h.Poker, h.straight.straight9, i, l, count, 11, PN))
		count++
	}
	//Nodes for straight 10
	for i := 0; i < len(h.straight.straight10); i++ {
		newLayer = append(newLayer, nodeFillIn(h.Poker, h.straight.straight10, i, l, count, 12, PN))
		count++
	}
	//Nodes for straight 11
	for i := 0; i < len(h.straight.straight11); i++ {
		newLayer = append(newLayer, nodeFillIn(h.Poker, h.straight.straight11, i, l, count, 13, PN))
		count++
	}
	//Nodes for straight 12
	for i := 0; i < len(h.straight.straight12); i++ {
		newLayer = append(newLayer, nodeFillIn(h.Poker, h.straight.straight12, i, l, count, 14, PN))
		count++
	}
	//Nodes for plane Layer 2 no wings
	for i := 0; i < len(h.plane.planeL2NoWing); i++ {
		newLayer = append(newLayer, nodeFillIn(h.Poker, h.plane.planeL2NoWing, i, l, count, 15, PN))
		count++
	}
	//Nodes for plane Layer 3 no wings
	for i := 0; i < len(h.plane.planeL3NoWing); i++ {
		newLayer = append(newLayer, nodeFillIn(h.Poker, h.plane.planeL3NoWing, i, l, count, 16, PN))
		count++
	}
	//Nodes for plane Layer 4 no wings
	for i := 0; i < len(h.plane.planeL4NoWing); i++ {
		newLayer = append(newLayer, nodeFillIn(h.Poker, h.plane.planeL4NoWing, i, l, count, 17, PN))
		count++
	}
	//Nodes for plane Layer 5 no wings
	for i := 0; i < len(h.plane.planeL5NoWing); i++ {
		newLayer = append(newLayer, nodeFillIn(h.Poker, h.plane.planeL5NoWing, i, l, count, 18, PN))
		count++
	}
	//Nodes for plane Layer 6 no wings
	for i := 0; i < len(h.plane.planeL6NoWing); i++ {
		newLayer = append(newLayer, nodeFillIn(h.Poker, h.plane.planeL6NoWing, i, l, count, 19, PN))
		count++
	}
	//Nodes for plane Layer 2 short wings
	for i := 0; i < len(h.plane.planeL2SWing); i++ {
		newLayer = append(newLayer, nodeFillIn(h.Poker, h.plane.planeL2SWing, i, l, count, 20, PN))
		count++
	}
	//Nodes for plane Layer 3 short wings
	for i := 0; i < len(h.plane.planeL3SWing); i++ {
		newLayer = append(newLayer, nodeFillIn(h.Poker, h.plane.planeL3SWing, i, l, count, 21, PN))
		count++
	}
	//Nodes for plane Layer 4 short wings
	for i := 0; i < len(h.plane.planeL4SWing); i++ {
		newLayer = append(newLayer, nodeFillIn(h.Poker, h.plane.planeL4SWing, i, l, count, 22, PN))
		count++
	}
	//Nodes for plane Layer 5 short wings
	for i := 0; i < len(h.plane.planeL5SWing); i++ {
		newLayer = append(newLayer, nodeFillIn(h.Poker, h.plane.planeL5SWing, i, l, count, 23, PN))
		count++
	}
	//Nodes for plane Layer 2 long wings
	for i := 0; i < len(h.plane.planeL2LWing); i++ {
		newLayer = append(newLayer, nodeFillIn(h.Poker, h.plane.planeL2LWing, i, l, count, 24, PN))
		count++
	}
	//Nodes for plane Layer 3 long wings
	for i := 0; i < len(h.plane.planeL3LWing); i++ {
		newLayer = append(newLayer, nodeFillIn(h.Poker, h.plane.planeL3LWing, i, l, count, 25, PN))
		count++
	}
	//Nodes for plane Layer 4 long wings
	for i := 0; i < len(h.plane.planeL4LWing); i++ {
		newLayer = append(newLayer, nodeFillIn(h.Poker, h.plane.planeL4LWing, i, l, count, 26, PN))
		count++
	}
	//Nodes for pairStraight 3 Layers
	for i := 0; i < len(h.pairStraight.pairStraight3); i++ {
		newLayer = append(newLayer, nodeFillIn(h.Poker, h.pairStraight.pairStraight3, i, l, count, 27, PN))
		count++
	}
	//Nodes for pairStraight 4 Layers
	for i := 0; i < len(h.pairStraight.pairStraight4); i++ {
		newLayer = append(newLayer, nodeFillIn(h.Poker, h.pairStraight.pairStraight4, i, l, count, 28, PN))
		count++
	}
	//Nodes for pairStraight 5 Layers
	for i := 0; i < len(h.pairStraight.pairStraight5); i++ {
		newLayer = append(newLayer, nodeFillIn(h.Poker, h.pairStraight.pairStraight5, i, l, count, 29, PN))
		count++
	}
	//Nodes for pairStraight 6 Layers
	for i := 0; i < len(h.pairStraight.pairStraight6); i++ {
		newLayer = append(newLayer, nodeFillIn(h.Poker, h.pairStraight.pairStraight6, i, l, count, 30, PN))
		count++
	}
	//Nodes for pairStraight 7 Layers
	for i := 0; i < len(h.pairStraight.pairStraight7); i++ {
		newLayer = append(newLayer, nodeFillIn(h.Poker, h.pairStraight.pairStraight7, i, l, count, 31, PN))
		count++
	}
	//Nodes for pairStraight 8 Layers
	for i := 0; i < len(h.pairStraight.pairStraight8); i++ {
		newLayer = append(newLayer, nodeFillIn(h.Poker, h.pairStraight.pairStraight8, i, l, count, 32, PN))
		count++
	}
	//Nodes for pairStraight 9 Layers
	for i := 0; i < len(h.pairStraight.pairStraight9); i++ {
		newLayer = append(newLayer, nodeFillIn(h.Poker, h.pairStraight.pairStraight9, i, l, count, 33, PN))
		count++
	}
	//Nodes for pairStraight 10 Layers
	for i := 0; i < len(h.pairStraight.pairStraight10); i++ {
		newLayer = append(newLayer, nodeFillIn(h.Poker, h.pairStraight.pairStraight10, i, l, count, 34, PN))
		count++
	}
	//Nodes for fourOnes
	for i := 0; i < len(h.fourOnes); i++ {
		newLayer = append(newLayer, nodeFillIn(h.Poker, h.fourOnes, i, l, count, 35, PN))
		count++
	}
	//Nodes for fourTwos
	for i := 0; i < len(h.fourTwos); i++ {
		newLayer = append(newLayer, nodeFillIn(h.Poker, h.fourTwos, i, l, count, 36, PN))
		count++
	}
	//檢視節點剩下牌組 誰已經沒有牌了，地主沒有牌給[1,0]，農夫任一沒有牌給[0,1]
	for i := 0; i < len(newLayer); i++ {
		if len(newLayer[i].RestCards[(l-1)%3]) == 0 && (l-1)%3 != 0 {
			newLayer[i].Outcome = []int{0, 1}
		} else if len(newLayer[i].RestCards[(l-1)%3]) == 0 && (l-1)%3 == 0 {
			newLayer[i].Outcome = []int{1, 0}
		}
		//t.Nodes = append(t.Nodes, newLayer[i])
	}
	if len(newLayer) != 0 {
		for r := 0; r < len(newLayer); r++ {
			for i := 0; i < 3; i++ {
				if (l-1)%3 != i {
					newLayer[r].RestCards[i] = inheritedRestCards[i]
				}
			}
			//newLayer[r].Isopoint = -999
			newLayer[r].ParentInfo = PN
		}
	}
	return newLayer
}
