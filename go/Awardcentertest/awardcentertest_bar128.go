package awardcentertest

import (
	"fmt"
	//"go/constant"
	"math/rand"
)

const (
	//SuitShift 花色在 uint32 中的位移
	SuitShift = 10000
	//PointShift 花色在 uint32 中的位移
	PointShift = 100
)

//EnumBar128Type ... all uint32 to int
var EnumBar128Type = map[int]string{
	0:  "EnumBar128Type_unknown",
	1:  "EnumBar128Type_bg3",
	2:  "EnumBar128Type_bg2",
	3:  "EnumBar128Type_bg1",
	4:  "EnumBar128Type_14",
	5:  "EnumBar128Type_13",
	6:  "EnumBar128Type_12",
	7:  "EnumBar128Type_11",
	8:  "EnumBar128Type_1half",
	9:  "EnumBar128Type_23",
	10: "EnumBar128Type_22",
	11: "EnumBar128Type_21",
	12: "EnumBar128Type_2half",
	13: "EnumBar128Type_34",
	14: "EnumBar128Type_33",
	15: "EnumBar128Type_32",
	16: "EnumBar128Type_31",
	17: "EnumBar128Type_3half",
	18: "EnumBar128Type_43",
	19: "EnumBar128Type_42",
	20: "EnumBar128Type_41",
	21: "EnumBar128Type_4half",
	22: "EnumBar128Type_54",
	23: "EnumBar128Type_53",
	24: "EnumBar128Type_52",
	25: "EnumBar128Type_51",
	26: "EnumBar128Type_5half",
	27: "EnumBar128Type_63",
	28: "EnumBar128Type_62",
	29: "EnumBar128Type_61",
	30: "EnumBar128Type_6half",
	31: "EnumBar128Type_74",
	32: "EnumBar128Type_73",
	33: "EnumBar128Type_72",
	34: "EnumBar128Type_71",
	35: "EnumBar128Type_7half",
	36: "EnumBar128Type_83",
	37: "EnumBar128Type_82",
	38: "EnumBar128Type_81",
	39: "EnumBar128Type_8half",
	40: "EnumBar128Type_94",
	41: "EnumBar128Type_93",
	42: "EnumBar128Type_92",
	43: "EnumBar128Type_91",
	44: "EnumBar128Type_9half",
	45: "EnumBar128Type_28gun",
	46: "EnumBar128Type_1Bao",
	47: "EnumBar128Type_2Bao",
	48: "EnumBar128Type_3Bao",
	49: "EnumBar128Type_4Bao",
	50: "EnumBar128Type_5Bao",
	51: "EnumBar128Type_6Bao",
	52: "EnumBar128Type_7Bao",
	53: "EnumBar128Type_8Bao",
	54: "EnumBar128Type_9Bao",
	55: "EnumBar128Type_king",
}

//給牌組排序分數
var ranking = map[int][]int{
	1: {404, 406}, 2: {403, 407}, 3: {401, 409}, 4: {405, 406}, 5: {404, 407}, 6: {403, 408}, 7: {402, 409},
	8: {203, 401}, 9: {405, 407}, 10: {404, 408}, 11: {403, 409}, 12: {203, 402}, 13: {401, 402}, 14: {406, 407},
	15: {405, 408}, 16: {404, 409}, 17: {203, 403}, 18: {401, 403}, 19: {406, 408}, 20: {405, 409}, 21: {203, 404},
	22: {402, 403}, 23: {401, 404}, 24: {407, 408}, 25: {406, 409}, 26: {203, 405}, 27: {402, 404}, 28: {401, 405},
	29: {407, 409}, 30: {203, 406}, 31: {403, 404}, 32: {402, 405}, 33: {401, 406}, 34: {408, 409}, 35: {203, 407},
	36: {403, 405}, 37: {402, 406}, 38: {401, 407}, 39: {203, 408}, 40: {404, 405}, 41: {403, 406}, 42: {402, 407},
	43: {401, 408}, 44: {203, 409}, 45: {402, 408}, 46: {401, 401}, 47: {402, 402}, 48: {403, 403}, 49: {404, 404},
	50: {405, 405}, 51: {406, 406}, 52: {407, 407}, 53: {408, 408}, 54: {409, 409}, 55: {203, 203},
}

//各排序分數對應的賠率
var odds = map[int]int{
	1: 1, 2: 1, 3: 1, 4: 1, 5: 1, 6: 1, 7: 1, 8: 1, 9: 2, 10: 2, 11: 2, 12: 2, 13: 3, 14: 3,
	15: 3, 16: 3, 17: 3, 18: 4, 19: 4, 20: 4, 21: 4, 22: 5, 23: 5, 24: 5, 25: 5, 26: 5, 27: 6, 28: 6,
	29: 6, 30: 6, 31: 7, 32: 7, 33: 7, 34: 7, 35: 7, 36: 8, 37: 8, 38: 8, 39: 8, 40: 9, 41: 9, 42: 9,
	43: 9, 44: 9, 45: 10, 46: 10, 47: 10, 48: 10, 49: 10, 50: 10, 51: 10, 52: 10, 53: 10, 54: 10, 55: 10,
}

//var ranking = map[int][]int{
//	1: {406, 404}, 2: {407, 403}, 3: {409, 401}, 4: {406, 405}, 5: {407, 404}, 6: {408, 403}, 7: {409, 402},
//	8: {401, 203}, 9: {407, 405}, 10: {408, 404}, 11: {409, 403}, 12: {402, 203}, 13: {402, 401}, 14: {407, 406},
//	15: {408, 405}, 16: {409, 404}, 17: {403, 203}, 18: {403, 401}, 19: {408, 406}, 20: {409, 405}, 21: {404, 203},
//	22: {403, 402}, 23: {404, 401}, 24: {408, 407}, 25: {409, 406}, 26: {405, 203}, 27: {404, 402}, 28: {405, 401},
//	29: {409, 407}, 30: {406, 203}, 31: {404, 403}, 32: {405, 402}, 33: {406, 401}, 34: {409, 408}, 35: {407, 203},
//	36: {405, 403}, 37: {406, 402}, 38: {407, 401}, 39: {408, 203}, 40: {405, 404}, 41: {406, 403}, 42: {407, 402},
//	43: {408, 401}, 44: {409, 203}, 45: {408, 402}, 46: {401, 401}, 47: {402, 402}, 48: {403, 403}, 49: {404, 404},
//	50: {405, 405}, 51: {406, 406}, 52: {407, 407}, 53: {408, 408}, 54: {409, 409}, 55: {203, 203},
//}

type CardSetInfoBar128 struct {
	CardSet    []uint32
	SetPoint   []uint32
	score      int
	crtp       float64 //complement of RTP
	rateDiffer float64
}

//CookBaiRen28 ... 殺數計算
func CookBaiRen28() {
	var TongDeck []uint32
	//var bet = []int{1, 1, 0, 0}
	//var sumBet = bet[0] + bet[1] + bet[2] + bet[3]
	//var target = -200.00
	//var current = 0.44
	//var allowedRange = 0.01
	//筒牌 1~9點的牌
	for i := 1; i <= 9; i++ {
		for j := 1; j <= 4; j++ {
			newCard := uint32(4*SuitShift + i*PointShift + j)
			TongDeck = append(TongDeck, newCard)
		}
	}
	//white card/203=white
	for i := 1; i <= 4; i++ {
		newCard := uint32(2*SuitShift + 3*PointShift + i)
		TongDeck = append(TongDeck, newCard)
	}
	var src cryptoSource
	rnd := rand.New(src)
	rand.Seed(int64(rnd.Intn(10000000000000000)))
	//rand.Seed(time.Now().UnixNano())
	//洗牌，並且建立本局預算的table，分成天地玄黃位置，跟莊家待配對位置
	rand.Shuffle(len(TongDeck), func(i, j int) { TongDeck[i], TongDeck[j] = TongDeck[j], TongDeck[i] })
	var position [][]uint32
	for j := 0; j < 4; j++ {
		position = append(position, TongDeck[j*2:j*2+2])
	}
	fmt.Println()
	//var HouseCandidate [][]int
	var CardSetInfosOfBar128 []CardSetInfoBar128
	var index = 0
	for i := range TongDeck {
		for j := range TongDeck {
			if i < j {
				CardSetInfosOfBar128[index].CardSet = []uint32{TongDeck[i], TongDeck[j]}
			}
			index++
		}
	}
	for i := 0; i < len(CardSetInfosOfBar128); i++ {
		fmt.Println(CardSetInfosOfBar128[i])
	}

	//var lastResort [][]int
	//var lastResortCards []string
	//for j := 0; j < 5; j++ {
	//	lastResort = append(lastResort, TongDeck[j*2:j*2+2])
	//}
	//
	//for i := 0; i < 5; i++ {
	//	a := sort.IntSlice([]int{lastResort[i][0] / 100, lastResort[i][1] / 100})
	//	for k, v := range ranking {
	//		if (v[0] == a[0] && v[1] == a[1]) || (v[0] == a[1] && v[1] == a[0]) {
	//			lastResortCards = append(lastResortCards, EnumBar128Type[k])
	//
	//		}
	//	}
	//}
	//fmt.Println(lastResortCards)

	TongDeck = TongDeck[8:]
	var HouseCandidate [][]uint32
	for i := range TongDeck {
		for j := range TongDeck {
			if i < j {
				HouseCandidate = append(HouseCandidate, []uint32{TongDeck[i], TongDeck[j]})
			}
		}
	}
	//獲取預算table裡面的各種資訊
	//var count = 0
	//var crtpMap = make(map[int]float64)
	//var rateDiffer = make(map[int]float64)
	//var scoreMap = make(map[int][]int)
	//var cardsMap = make(map[int][][]int)
	//var selectedcRtpMap = make(map[int]float64)
	//var selectedCardsMap = make(map[int][]string)
	//var w [][]int
	//開始配對，並且計算各組合的RTP
	//for index1 := range HouseCandidate {
	//	w = append(position, HouseCandidate[index1])
	//	cardsMap[count] = w
	//	//獲取天、地、玄、黃、莊家的牌組分數
	//	for i := 0; i < 5; i++ {
	//		a := sort.IntSlice([]int{w[i][0] / 100, w[i][1] / 100})
	//		for k, v := range ranking {
	//			if (v[0] == a[0] && v[1] == a[1]) || (v[0] == a[1] && v[1] == a[0]) {
	//				scoreMap[count] = append(scoreMap[count], k)
	//			}
	//		}
	//	}

	//Calculate HouseGain (out or in)
	//正值代表掉入莊家口袋淨值，負值代表掉出莊家口袋淨值
	//	var houseGain = 0
	//	for i := 0; i < 4; i++ {
	//		if scoreMap[count][i] > scoreMap[count][4] {
	//			//莊家損失
	//			houseGain += (-odds[scoreMap[count][i]] + 1) * bet[i]
	//		} else if scoreMap[count][i] <= scoreMap[count][4] {
	//			//莊家獲得
	//			houseGain += (odds[scoreMap[count][4]] + 1) * bet[i]
	//		}
	//	}
	//	crtpMap[count] = float64(houseGain) / float64(sumBet)
	//	rateDiffer[count] = math.Abs(crtpMap[count] - target)
	//	count++
	//}
	//for i := 0; i < len(crtpMap); i++ {
	//	fmt.Println(crtpMap[i])
	//}
	////建立候選table
	//if current < target {
	//	//fmt.Println("Pull up")
	//	for selected := range crtpMap {
	//		if crtpMap[selected] > target {
	//			selectedcRtpMap[selected] = crtpMap[selected]
	//			for i := 0; i < 5; i++ {
	//				selectedCardsMap[selected] = append(selectedCardsMap[selected], EnumBar128Type[scoreMap[selected][i]])
	//			}
	//		}
	//	}
	//} else if current >= target+allowedRange {
	//	//fmt.Println("Pull down")
	//	for selected := range crtpMap {
	//		if crtpMap[selected] < target {
	//			selectedcRtpMap[selected] = crtpMap[selected]
	//			//selectedCardsMap[selected] = scoreMap[selected]
	//			for i := 0; i < 5; i++ {
	//				selectedCardsMap[selected] = append(selectedCardsMap[selected], EnumBar128Type[scoreMap[selected][i]])
	//			}
	//		}
	//	}
	//}
	////finding closest ones! 找不到，找尋最接近目標殺數的牌組
	//if len(selectedcRtpMap) == 0 {
	//	var smallestDiffer = math.Inf(+1)
	//	for i := 0; i < len(rateDiffer); i++ {
	//		if rateDiffer[i] < smallestDiffer {
	//			smallestDiffer = rateDiffer[i]
	//		}
	//	}
	//	for i := 0; i < len(rateDiffer); i++ {
	//		if rateDiffer[i] == smallestDiffer {
	//			selectedcRtpMap[i] = crtpMap[i]
	//			for j := 0; j < 5; j++ {
	//				selectedCardsMap[i] = append(selectedCardsMap[i], EnumBar128Type[scoreMap[i][j]])
	//			}
	//		}
	//	}
	//}
	//for s := range selectedcRtpMap {
	//	fmt.Println(selectedcRtpMap[s])
	//}
	//
	////確定拿到的預算table非空
	//if len(selectedcRtpMap) != 0 {
	//	var getKey = randomKey(selectedcRtpMap)
	//	fmt.Println("-_-_-_", selectedcRtpMap[getKey])
	//	fmt.Println("天", selectedCardsMap[getKey][0])
	//	fmt.Println("地", selectedCardsMap[getKey][1])
	//	fmt.Println("玄", selectedCardsMap[getKey][2])
	//	fmt.Println("黃", selectedCardsMap[getKey][3])
	//	fmt.Println("莊家", selectedCardsMap[getKey][4])
	//} else {
	//	//如果是空的
	//	fmt.Println("pre-calculation map is empty!Randomly throw out a set of cards")
	//	//fmt.Println("天", lastResort[0])
	//	//fmt.Println("地", lastResort[1])
	//	//fmt.Println("玄", lastResort[2])
	//	//fmt.Println("黃", lastResort[3])
	//	//fmt.Println("莊家", lastResort[4])
	//	// random throw out some cards
	//}
}
