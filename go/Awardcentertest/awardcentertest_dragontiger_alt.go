package awardcentertest

import (
	"fmt"
	"math/rand"
)

type CardSetInfoDT struct {
	CardSet  []int
	SetPoint []int
	crtp     float64 //complement of RTP
	bonus    float64
}

func randPoolDT(m []CardSetInfoDT) int {
	var src cryptoSource
	rnd := rand.New(src)
	rand.Seed(int64(rnd.Intn(10000000000000000)))
	throw := rand.Intn(len(m))
	fmt.Println(len(m), throw)

	return throw //rand.Intn(len(m))
}

func transPoker(Poker []int) []int {
	var TransPoker []int
	for i := 0; i < len(Poker); i++ {
		if Poker[i]/SuitShift == 1 {
			TransPoker = append(TransPoker, Poker[i]+10000)
		} else if Poker[i]/SuitShift == 2 {
			TransPoker = append(TransPoker, Poker[i]-10000)
		} else {
			TransPoker = append(TransPoker, Poker[i])
		}
	}
	Poker = TransPoker
	return Poker
}

//CookDragonTigerAlter 龍虎鬥做牌
func CookDragonTigerAlter() {
	var Poker []int

	Poker = []int{30011, 10007, 40002, 20003, 10002, 10005, 30013, 30008, 40006, 30005}
	var src cryptoSource
	rnd := rand.New(src)
	rand.Seed(int64(rnd.Intn(10000000000000000)))
	rand.Shuffle(len(Poker), func(i, j int) { Poker[i], Poker[j] = Poker[j], Poker[i] })
	var DTCardSetInfos []CardSetInfoDT
	var bet = []float64{4, 8, 12}
	//bet = append(bet, (bet[0]+bet[1])/10)
	var sumBet = bet[0] + bet[1] + bet[2]
	var target = 0.10
	var current = 0.1579
	var allowedRange = 0.01
	var HouseGain = 2380.0
	var abundance = (current - target) * HouseGain
	//fmt.Println(Poker)
	//Poker = transPoker(Poker)
	//fmt.Println(Poker)
	for index := range Poker {
		for index1 := range Poker {
			if index != index1 && sumBet != 0 {
				newCardSetInfo := CardSetInfoDT{}
				newCardSetInfo.CardSet = []int{Poker[index], Poker[index1]}
				newCardSetInfo.SetPoint = []int{((Poker[index]%SuitShift-1)%13+1)*100 + Poker[index]/SuitShift, ((Poker[index1]%SuitShift-1)%13+1)*100 + Poker[index1]/SuitShift}
				DTCardSetInfos = append(DTCardSetInfos, newCardSetInfo)

			}
		}
	}

	for i := 0; i < len(DTCardSetInfos); i++ {
		if DTCardSetInfos[i].SetPoint[0] > DTCardSetInfos[i].SetPoint[1] {
			DTCardSetInfos[i].crtp = 1 - bet[0]*2/sumBet
			DTCardSetInfos[i].bonus = bet[0] * 2

		} else if DTCardSetInfos[i].SetPoint[0] < DTCardSetInfos[i].SetPoint[1] {
			DTCardSetInfos[i].crtp = 1 - bet[1]*2/sumBet
			DTCardSetInfos[i].bonus = bet[1] * 2
		} else {
			DTCardSetInfos[i].crtp = 1 - bet[2]*20/sumBet
			DTCardSetInfos[i].bonus = bet[2] * 20
		}
	}
	for i := 0; i < len(DTCardSetInfos); i++ {
		fmt.Println(DTCardSetInfos[i])
	}
	var pool []CardSetInfoDT
	if len(DTCardSetInfos) != 0 {
		if current < target {
			for _, element := range DTCardSetInfos {
				if element.crtp > target {
					pool = append(pool, element)
				}
			}
			if len(pool) == 0 {
				fmt.Println("failed at normal selection procedure")
				for i := 0; i < len(DTCardSetInfos); i++ {
					if DTCardSetInfos[i].crtp > current {
						pool = append(pool, DTCardSetInfos[i])
					}
				}
			}
		} else if current >= target+allowedRange { //增加條件
			for _, element := range DTCardSetInfos {
				if element.crtp < target {
					pool = append(pool, element)
				}
			}
			if len(pool) == 0 {
				fmt.Println("failed at normal selection procedure")
				for i := 0; i < len(DTCardSetInfos); i++ {
					if DTCardSetInfos[i].crtp < current && DTCardSetInfos[i].bonus < abundance {
						pool = append(pool, DTCardSetInfos[i])
					}
				}
			}
		}

		if len(pool) != 0 {
			fmt.Println("候選清單")
			for index, value := range pool {
				fmt.Println(index, value)
			}
			var i = randPoolDT(pool) // 選中的一組
			fmt.Println("選出的組合", pool[i].bonus)
			var Thrown = cardThrown(Poker, pool[i].CardSet) // 除選中的那一組以外隨便丟掉一組
			fmt.Println("Everything goes as planned!")
			fmt.Println(append(pool[i].CardSet, Thrown))
		} else {
			var i = randPoolDT(DTCardSetInfos)
			var Thrown = cardThrown(Poker, DTCardSetInfos[i].CardSet)
			fmt.Println("Randomly throw out a set of cards")
			fmt.Println(append(DTCardSetInfos[i].CardSet, Thrown))
		}
	} else {
		random(Poker)
	}
}
