package awardcentertest

import (
	"fmt"
	"math"
	"math/rand"
	"sort"
	"time"
)

func randChoose(size int) int {
	var src cryptoSource
	rnd := rand.New(src)
	dice := rnd.Intn(size)
	return dice
}
func (h *hand) updatePoker(taken []int) hand {
	var newPoker []int
	for i := range h.Poker {
		if notIn(h.Poker[i], taken) {
			newPoker = append(newPoker, h.Poker[i])
		}
	}
	newAllCardSets := hand{}
	newAllCardSets.Poker = newPoker
	newAllCardSets = CardTypeConstructor(newAllCardSets)
	return newAllCardSets
}
func (c *cardsSetsOp) Pick() []int {
	var sequence []int
	editorPick := randChoose(c.counter)
	sequence = c.pile[editorPick]
	delete(c.pile, editorPick)
	return sequence
}

//func getCardSets(allCardSets hand, length int) []int {
//	collector := cardsSetsOp{}
//	collector.openingCooking(allCardSets, length)
//	if collector.counter > 0 {
//		return collector.Pick()
//	} else {
//		return nil
//	}
//}

//func getMaxLengthCardSets(allCardSets hand) (map[int][]int, int) {
//	collector := cardsSetsOp{}
//	for i := 0; i < 20; i++ {
//		collector.openingCooking(allCardSets, i)
//	}
//	var highest int
//	for i := range collector.pile {
//		if len(collector.pile[i]) > highest {
//			highest = len(collector.pile[i])
//		}
//	}
//	newCollector := cardsSetsOp{}
//	newCollector.openingCooking(allCardSets, highest)
//	return newCollector.pile, highest
//
//}

func (h *hand) maxIndexFinder() int {
	var reference = 0
	for i := range h.typeExistence {
		if h.typeExistence[i] == true && i > reference {
			reference = i
		}
	}
	return reference
}

func (h *hand) index2length(index int) int {
	var length int
	if index == 0 {
		length = 1
	} else if index == 1 {
		length = 2
	} else if index == 2 {
		length = 3
	} else if index == 3 {
		length = 4
	} else if index == 4 {
		length = 2
	} else if index == 5 {
		length = 4
	} else if index == 6 {
		length = 5
	} else if index == 7 {
		length = 5
	} else if index == 8 {
		length = 6
	} else if index == 9 {
		length = 7
	} else if index == 10 {
		length = 8
	} else if index == 11 {
		length = 9
	} else if index == 12 {
		length = 10
	} else if index == 13 {
		length = 11
	} else if index == 14 {
		length = 12
	} else if index == 15 {
		length = 6
	} else if index == 16 {
		length = 9
	} else if index == 17 {
		length = 12
	} else if index == 18 {
		length = 15
	} else if index == 19 {
		length = 18
	} else if index == 20 {
		length = 8
	} else if index == 21 {
		length = 12
	} else if index == 22 {
		length = 16
	} else if index == 23 {
		length = 20
	} else if index == 24 {
		length = 10
	} else if index == 25 {
		length = 15
	} else if index == 26 {
		length = 20
	} else if index == 27 {
		length = 6
	} else if index == 28 {
		length = 8
	} else if index == 29 {
		length = 10
	} else if index == 30 {
		length = 12
	} else if index == 31 {
		length = 14
	} else if index == 32 {
		length = 16
	} else if index == 33 {
		length = 18
	} else if index == 34 {
		length = 20
	} else if index == 35 {
		length = 6
	} else if index == 36 {
		length = 8
	}

	return length
}

func (h *hand) length(enterTimes int, roleCards int, thisRoleLen int) int {
	var value int
	switch enterTimes {
	case 0:
		value = 9
	case 1:
		if h.typeExistence[8] == true {
			value = 6
		} else if h.typeExistence[7] == true {
			value = 5
		}
		//fmt.Println(h.index2length(h.maxIndexFinder()))
	case 2:
		if h.typeExistence[3] == true {
			value = 4
		} else if h.typeExistence[3] == true || h.typeExistence[5] == true {
			value = 4
		}
	case 3:
		if roleCards-thisRoleLen > 0 {
			value = roleCards - thisRoleLen
		} else {
			value = 1
		}

	default:
		value = 1 //h.index2length(h.maxIndexFinder())
	}
	return value
}

func roleCards(i int) int {
	if i == 0 {
		return 20
	} else {
		return 17
	}
}

func (h *hand) fillInRest(RestPoker []int, upperBound int) []int {
	var takenAway []int
	for i := range RestPoker {
		if notIn(RestPoker[i], h.Poker) && len(h.Poker) <= upperBound {
			h.Poker = append(h.Poker, RestPoker[i])
			takenAway = append(takenAway, RestPoker[i])
		}
	}
	return takenAway
}

func iteratedCookingProcess(preSettings []hand, allCardSets hand) {
	var taken []int
	for i := 0; i < 3; i++ {
		var enterTimes = 0
		for len(preSettings[i].Poker) < (roleCards(i) * 1 / 2) {
			//fmt.Println(i, len(allCardSets.typeExistence))
			//length := allCardSets.length(enterTimes, roleCards(i), len(preSettings[i].Poker))
			//taken = getCardSets(allCardSets)
			allCardSets = allCardSets.updatePoker(taken)
			enterTimes += 1
			if taken != nil {
				preSettings[i].fillInCards(taken)
			}
		}
	}
	var src cryptoSource
	rnd := rand.New(src)
	rand.Seed(rnd.Int63n(time.Now().UnixNano()))
	rand.Shuffle(len(allCardSets.Poker), func(i, j int) {
		allCardSets.Poker[i], allCardSets.Poker[j] = allCardSets.Poker[j], allCardSets.Poker[i]
	})
	for j := 0; j < 3; j++ {
		taken = allCardSets.Poker[:roleCards(j)-len(preSettings[j].Poker)]
		allCardSets = allCardSets.updatePoker(taken)
		preSettings[j].fillInCards(taken)
	}
	//allCardSets = allCardSets.updatePoker(taken)
	//fmt.Println(allCardSets.Poker)
	//preSettings[2].fillInCards(allCardSets.Poker[:roleCards(2)])
	sort.Slice(preSettings[2].Poker, func(i, j int) bool { return preSettings[2].Poker[i]%SuitShift < preSettings[2].Poker[j]%SuitShift })

}

func checkExclusivity(hands []hand) bool {
	var checkall = 1
	for i := range hands {
		for j := range hands {
			if i < j {
				if !notInS2S(hands[i].Poker, hands[j].Poker) {
					checkall *= 0
				}
			}
		}
	}
	if checkall == 1 {
		return true
	}
	return false
}

//揀選開局
func pickOpenings() ([]int, []hand, int) {
	allCardSets := hand{}
	allCardSets.Poker, _ = fullPackPoker()
	allCardSets = CardTypeConstructor(allCardSets)
	var preSettings = make([]hand, 3)
	iteratedCookingProcess(preSettings, allCardSets)
	if checkExclusivity(preSettings) {
		fmt.Println("Perfectly constructed opening!")
	} else {
		fmt.Println("failure!")
	}
	var Poker []int
	for i := 0; i < len(preSettings); i++ {
		for j := range preSettings[i].Poker {
			Poker = append(Poker, preSettings[i].Poker[j])
		}
	}
	var cardsTaken = 0
	for i := range preSettings {
		cardsTaken += len(preSettings[i].Poker)
	}
	return Poker, preSettings, cardsTaken
}

func (h *hand) fillInCards(taken []int) {
	for i := range taken {
		h.Poker = append(h.Poker, taken[i])
	}
}

func setRemover(takenSet []int, takenFrom []int) []int {
	var restPoker []int
	for i := range takenFrom {
		if notIn(takenFrom[i], takenSet) {
			restPoker = append(restPoker, takenFrom[i])
		}
	}
	return restPoker
}

func (c *cardsSetsOp) getCardSequence() []int {
	var sequence []int
	editorPick := randChoose(c.counter)
	sequence = c.pile[editorPick]
	delete(c.pile, editorPick)
	return sequence
}

func (h *hand) cardsMaterialization(sets []int) []int {
	var Poker []int
	for i := range sets {
		if sets[i]/10000 != 5 {
			if h.cardRepetitions[sets[i]] < 4 {
				h.Poker = append(h.Poker, (h.cardRepetitions[sets[i]]+1)*SuitShift+sets[i])
				Poker = append(Poker, (h.cardRepetitions[sets[i]]+1)*SuitShift+sets[i])
				h.repetitions()
			}
		} else {
			h.Poker = append(h.Poker, sets[i])
			Poker = append(Poker, sets[i])
			h.repetitions()
		}
	}
	return Poker

}

func (c *cardsSetsOp) Straight5s(h hand) {
	c.pile = make(map[int][]int)
	for i := range h.straight.straight5 { //5 cards
		c.pile[c.counter] = h.straight.straight5[i]
		c.counter++
	}
}

func (c *cardsSetsOp) openingCooking(h hand) {
	c.pile = make(map[int][]int)
	for i := range h.singleCards { //1 cards type:0
		c.pile[c.counter] = h.singleCards[i]
		c.counter++
	}
	for i := range h.pairs { //2 cards type:1
		c.pile[c.counter] = h.pairs[i]
		c.counter++
	}
	for i := range h.kBomb {
		c.pile[c.counter] = h.kBomb[i]
		c.counter++
	}
	for i := range h.threes { //3 cards
		c.pile[c.counter] = h.threes[i]
		c.counter++
	}
	for i := range h.bombs { //4 cards type:3
		c.pile[c.counter] = h.bombs[i]
		c.counter++
	}
	for i := range h.threeOnes { //4 cards
		c.pile[c.counter] = h.threeOnes[i]
		c.counter++
	}
	for i := range h.threeTwos { //5 cards
		c.pile[c.counter] = h.threeTwos[i]
		c.counter++
	}
	for i := range h.straight.straight5 { //5 cards
		c.pile[c.counter] = h.straight.straight5[i]
		c.counter++
	}
	//飛機系列
	for i := range h.plane.planeL2NoWing { //6 cards
		c.pile[c.counter] = h.plane.planeL2NoWing[i]
		c.counter++
	}
	for i := range h.fourOnes { //6 cards
		c.pile[c.counter] = h.fourOnes[i]
		c.counter++
	}
	//單雙順系列
	for i := range h.pairStraight.pairStraight3 { //6 cards
		c.pile[c.counter] = h.pairStraight.pairStraight3[i]
		c.counter++
	}
	for i := range h.straight.straight6 { //6 cards
		c.pile[c.counter] = h.straight.straight6[i]
		c.counter++
	}
	for i := range h.straight.straight7 { //7 cards
		c.pile[c.counter] = h.straight.straight7[i]
		c.counter++
	}
	for i := range h.straight.straight8 { //8 cards
		c.pile[c.counter] = h.straight.straight8[i]
		c.counter++
	}
	//連順
	for i := range h.pairStraight.pairStraight4 { //8 cards
		c.pile[c.counter] = h.pairStraight.pairStraight4[i]
		c.counter++
	}
	for i := range h.plane.planeL4NoWing { //8 cards
		c.pile[c.counter] = h.plane.planeL4NoWing[i]
		c.counter++
	}
	for i := range h.plane.planeL2SWing { //8 cards
		c.pile[c.counter] = h.plane.planeL2SWing[i]
		c.counter++
	}
	for i := range h.fourTwos { //8 cards
		c.pile[c.counter] = h.fourTwos[i]
		c.counter++
	}
	for i := range h.plane.planeL3NoWing { //9 cards
		c.pile[c.counter] = h.plane.planeL3NoWing[i]
		c.counter++
	}
	for i := range h.straight.straight9 { //9 cards
		c.pile[c.counter] = h.straight.straight9[i]
		c.counter++
	}
	for i := range h.straight.straight10 { //10 cards
		c.pile[c.counter] = h.straight.straight10[i]
		c.counter++
	}
	for i := range h.pairStraight.pairStraight5 { //10 cards
		c.pile[c.counter] = h.pairStraight.pairStraight5[i]
		c.counter++
	}
	for i := range h.plane.planeL5NoWing { //10 cards
		c.pile[c.counter] = h.plane.planeL5NoWing[i]
		c.counter++
	}
	for i := range h.plane.planeL2LWing { //10 cards
		c.pile[c.counter] = h.plane.planeL2LWing[i]
		c.counter++
	}
	for i := range h.straight.straight11 { //11 cards
		c.pile[c.counter] = h.straight.straight11[i]
		c.counter++
	}
	for i := range h.straight.straight12 { //12 cards
		c.pile[c.counter] = h.straight.straight12[i]
		c.counter++
	}
	for i := range h.pairStraight.pairStraight6 { //12 cards
		c.pile[c.counter] = h.pairStraight.pairStraight6[i]
		c.counter++
	}
	for i := range h.plane.planeL3SWing { //12 cards
		c.pile[c.counter] = h.plane.planeL3SWing[i]
		c.counter++
	}
	for i := range h.pairStraight.pairStraight7 { //14 cards
		c.pile[c.counter] = h.pairStraight.pairStraight7[i]
		c.counter++
	}
	for i := range h.plane.planeL3LWing { //15 cards
		c.pile[c.counter] = h.plane.planeL3LWing[i]
		c.counter++
	}
	for i := range h.pairStraight.pairStraight8 { //16 cards
		c.pile[c.counter] = h.pairStraight.pairStraight8[i]
		c.counter++
	}
	for i := range h.plane.planeL4SWing { //16 cards
		c.pile[c.counter] = h.plane.planeL4SWing[i]
		c.counter++
	}
	for i := range h.pairStraight.pairStraight9 { //18 cards
		c.pile[c.counter] = h.pairStraight.pairStraight9[i]
		c.counter++
	}
	for i := range h.pairStraight.pairStraight10 { //20 cards
		c.pile[c.counter] = h.pairStraight.pairStraight10[i]
		c.counter++
	}
	for i := range h.plane.planeL4LWing { //20 cards
		c.pile[c.counter] = h.plane.planeL4LWing[i]
		c.counter++
	}
	for i := range h.plane.planeL5SWing { //20 cards
		c.pile[c.counter] = h.plane.planeL5SWing[i]
		c.counter++
	}
}

func fullPackPoker() ([]int, int) {
	var Poker []int
	for i := 1; i <= 4; i++ {
		for j := 3; j <= 15; j++ { //upper bound should be 13, set lower to make it easier to check result
			Poker = append(Poker, i*SuitShift+j)
		}
	}
	distance := int(math.Floor(float64(len(Poker)) / 3))
	Poker = append(Poker, 50030)
	Poker = append(Poker, 50031)
	return Poker, distance
}

func initialSettings() ([]int, []hand, int) {
	//製牌 3~15 represent 1~13
	Poker, distance := fullPackPoker()
	////洗牌
	var src cryptoSource
	rnd := rand.New(src)
	rand.Seed(rnd.Int63n(time.Now().UnixNano()))
	rand.Shuffle(len(Poker), func(i, j int) { Poker[i], Poker[j] = Poker[j], Poker[i] })
	////發牌
	var tablePLayers = make([]hand, 3)
	for i := 0; i < 3; i++ {
		newHand := hand{}
		newHand.Poker = Poker[i*distance : (i+1)*distance]
		tablePLayers[i] = newHand
	}
	var cardsTaken = 0
	for i := range tablePLayers {
		cardsTaken += len(tablePLayers[i].Poker)
	}
	return Poker, tablePLayers, cardsTaken
}

func printPokerInFormat(Poker []int, role int) {
	for i := 0; i < len(Poker); i++ {
		if i != len(Poker)-1 && i != 0 {
			fmt.Print(Poker[i], ",")
		} else if i == 0 {
			fmt.Print("inGamePlayer[", role, "].Poker = []int{", Poker[i], ",")
		} else {
			fmt.Println(Poker[i], "}")
		}
	}
}

//把撲克牌填入各角色位置
func rolePointer(tablePLayers []hand, cardsTaken int, Poker []int) []hand {
	var inGamePLayer = make([]hand, 3)
	for i := 0; i < 3; i++ {
		if i == 0 {
			for j := cardsTaken; j < len(Poker); j++ {
				inGamePLayer[i].Poker = append(inGamePLayer[i].Poker, Poker[j])
			}
			for s := 0; s < len(tablePLayers[i].Poker); s++ {
				inGamePLayer[i].Poker = append(inGamePLayer[i].Poker, tablePLayers[i].Poker[s])
			}
			inGamePLayer[i].roleType = "地主"
		} else {
			for s := 0; s < len(tablePLayers[i].Poker); s++ {
				inGamePLayer[i].Poker = append(inGamePLayer[i].Poker, tablePLayers[i].Poker[s])
			}
			inGamePLayer[i].roleType = "農夫"
		}
		fmt.Println(getCardsName(inGamePLayer[i].Poker))
		fmt.Print(len(inGamePLayer[i].Poker), " ")
		printPokerInFormat(inGamePLayer[i].Poker, i)
		//inGamePLayer[i] = CardTypeConstructor(inGamePLayer[i])
	}
	return inGamePLayer
}
