package awardcentertest

import (
	"fmt"
	"sync"
	"time"
)

func Dodizhu() {
	//建立開局
	//defer timeTrack(time.Now(), "鬥地主主線計算時間")
	//拿到洗好的撲克牌，分成三堆，並且計算總撲克牌張數
	_, inGamePlayer, _ := initialSettings()
	//拿到做牌好的撲克牌，分成三堆，並且計算總撲克牌張數
	//Poker, tablePLayers, cardsCount := pickOpenings()
	//inGamePlayer := rolePointer(PlayerCards, cardsCount, Poker)
	//inGamePlayer := make([]hand, 3)
	//飛機開局，Layer=2,3,4
	//inGamePlayer := PlaneOpening(2)
	//地主：八張順 ＋ 炸彈2
	//農夫一：八張順 ＋ 炸彈A
	//農夫二：八張順 ＋ 炸彈4
	//inGamePlayer := StraightPlaneBombOpening()
	//六張順＋對子順/三帶一/三帶二的開局
	//inGamePlayer := Straigh6PairStraightOpenningTricked()
	//inGamePlayer := Straigh6PairStraightOpenning()

	for i := 0; i < 3; i++ {
		inGamePlayer[i].Poker = inGamePlayer[i].Poker[:8]
	}
	//分割種類數，分割強度，分割數量
	//BrutalExpansion(inGamePlayer, 1)
	ShrinkingExpansion(inGamePlayer, 1)
}

//展開
func BrutalExpansion(inGamePlayer []hand, inventoryConstruct int) {
	for i := range inGamePlayer {
		fmt.Println(inGamePlayer[i].roleType, len(inGamePlayer[i].Poker), getCardsName(inGamePlayer[i].Poker), inGamePlayer[i].Poker)
		inGamePlayer[i] = CardTypeConstructor(inGamePlayer[i])
	}
	ConstructTree(inGamePlayer, inventoryConstruct)
}

//DecisionTreeMaking ... Construct decision tree for backwards induction
func ConstructTree(inGamePLayer []hand, inventoryConstruct int) {
	defer timeTrack(time.Now(), "Decision-Tree Construction Time")
	//建造tree
	var tree = make([]Layer, 1)
	//建造初始節點
	tree[0].Nodes = append(tree[0].Nodes, Nodes{0, 0, 2, -999, []int{}, CardType{[]int{}, -1}, map[int][]int{}, []int{}})
	for i := 0; i < 3; i++ {
		tree[0].Nodes[0].RestCards[i] = inGamePLayer[i].Poker
	}
	var inventory = make(map[InvNodeKeyBeta]Isopoints)
	//迭代展開
	//recurrently generating child Nodes for each current node
	var l = 1
	for {
		var newLayer Layer
		tree = append(tree, newLayer)
		if len(tree[l-1].Nodes) != 0 {
			tree[l-1].iteratedGeneratingProcess(tree, tree[l-1].Nodes, inventory, l, (l-1)%3) //開始分佈節點，因為l從1開始，所以這邊role=(l-1)%3
			//fmt.Println(l-1, len(tree[l-1].Nodes))
		} else {
			break
		}
		l++
	}
	//計算總節點數
	TreeInfo(tree)
	//展開同構點倉庫內的節點建造subtree
	var resultinventory = make(map[InvNodeKeyBeta]Isopoints)
	if inventoryConstruct == 1 {
		getinventory := parsingInventory(inventory, tree)
		resultinventory = subtreeExpansion(getinventory)
		checkingInventory(resultinventory)
	}
	//反向從底層往上層填節點結局
	backwardsFilling(tree)
	//列出所有節點
	totalLayers(tree, 0, 6)
}

func getTotalRestCards(restcards map[int][]int) int {
	var total = 0
	for i := range restcards {
		total += len(restcards[i])
	}
	return total
}

func parsingInventory(invent map[InvNodeKeyBeta]Isopoints, tree []Layer) map[InvNodeKeyBeta]Isopoints {
	var distinct = make(map[int]int)
	var totalDuplicatePointsCount int
	var SingleDuplicateCount int
	var newInventory = make(map[InvNodeKeyBeta]Isopoints)
	//group := sync.WaitGroup{}
	//mu := sync.Mutex{}
	for i := range invent {
		if invent[i].matchingTimes != 0 {
			newInventory[i] = invent[i]
			length := getTotalRestCards(invent[i].RestCards)
			totalDuplicatePointsCount += invent[i].matchingTimes
			if invent[i].matchingTimes == 1 {
				SingleDuplicateCount++
			}
			if distinct[length] != 0 {
				distinct[length] += 1
			} else {
				distinct[length] = 1
			}
			if length == 30 {
				fmt.Println(tree[invent[i].Index%1000].Nodes[invent[i].Index/1000])
			}
		}
	}
	//group.Wait()
	fmt.Println("This Tree has", len(newInventory), "isomorphic points")
	fmt.Println("TotalCardsLeft:", distinct, "TotalTimesCount：", totalDuplicatePointsCount, "僅重複一次(總共兩個subtree)：", SingleDuplicateCount)
	return newInventory
}

//同構點的subtree展開
func subtreeExpansion(getinvent map[InvNodeKeyBeta]Isopoints) map[InvNodeKeyBeta]Isopoints {
	defer timeTrack(time.Now(), "獲取all subtree結果建造時間")
	var resultInventory = make(map[InvNodeKeyBeta]Isopoints)
	group := sync.WaitGroup{}
	mu := sync.Mutex{}
	for i := range getinvent {
		//WriteInvetoryNodeInMongo(i, resultInventory[i], "inventory")
		group.Add(1)
		go i.BrutalExpandSubTree(i, getinvent[i], resultInventory, &group, &mu)
		//go i.ShrinkedExpandSubTree(i, getinvent[i], resultInventory, &group, &mu)
	}
	group.Wait()
	return resultInventory
}

//檢視是否所有同構點展開subtree之後
//
//都有拿到結果
func checkingInventory(getinvent map[InvNodeKeyBeta]Isopoints) {
	for i := range getinvent {
		if len(getinvent[i].Outcome) == 0 {
			fmt.Println("Not completed", i)
		}
	}
}
