package awardcentertest

import (
	"fmt"
	"math/rand"
)

type conTypeCRTP struct {
	Single3Balls  float64
	Double3Balls  float64
	Single4Balls  float64
	Single5Balls  float64
	NoConnections float64
}

type conTypeBonus struct {
	Single3Balls  float64
	Double3Balls  float64
	Single4Balls  float64
	Single5Balls  float64
	NoConnections float64
}

const (
	//ThreeConnectionPortion 三連線比例
	ThreeConnectionPortion = 1738
	//DoubleThreeConnectionPortion 雙三連線比例
	DoubleThreeConnectionPortion = 66
	//FourConnectionPortion 四連線比例
	FourConnectionPortion = 107
	//FiveConnectionPortion 五連線比例
	FiveConnectionPortion = 3
	//NoConnectionsPortion 無連線比例
	NoConnectionsPortion = 8086
)

func sumOf(portion []uint32) uint32 {
	var total uint32
	for i := 0; i < len(portion); i++ {
		total += portion[i]
	}
	return total
}

//照比例取樣
func randPick(result [][]int, portion []uint32) []int {
	var delivered []int
	var src cryptoSource
	rnd := rand.New(src)
	dice := uint32(rnd.Intn(int(sumOf(portion))))
	if dice < portion[0] {
		delivered = result[0]
	} else if dice >= portion[0] && dice < portion[1]+portion[0] {
		delivered = result[1]
	} else if dice >= portion[1]+portion[0] && dice < portion[2]+portion[1]+portion[0] {
		delivered = result[2]
	} else if dice >= portion[2]+portion[1]+portion[0] && dice < portion[3]+portion[2]+portion[1]+portion[0] {
		delivered = result[3]
	}
	//if len(result) == 1 {
	//	delivered = result[0]
	//} else if len(result) == 2 {
	//	if dice < portion[0] {
	//		delivered = result[0]
	//	} else if dice >= portion[0] && dice < portion[1]+portion[0] {
	//		delivered = result[1]
	//	}
	//} else if len(result) == 3 {
	//	if dice < portion[0] {
	//		delivered = result[0]
	//	} else if dice >= portion[0] && dice < portion[1]+portion[0] {
	//		delivered = result[1]
	//	} else if dice >= portion[1]+portion[0] && dice < portion[2]+portion[1]+portion[0] {
	//		delivered = result[2]
	//	}
	//} else if len(result) == 4 {
	//	if dice < portion[0] {
	//		delivered = result[0]
	//	} else if dice >= portion[0] && dice < portion[1]+portion[0] {
	//		delivered = result[1]
	//	} else if dice >= portion[1]+portion[0] && dice < portion[2]+portion[1]+portion[0] {
	//		delivered = result[2]
	//	} else if dice >= portion[2]+portion[1]+portion[0] && dice < portion[3]+portion[2]+portion[1]+portion[0] {
	//		delivered = result[3]
	//	}
	//}
	return delivered
}

func resultGenerator(result [][]int, newConTypeCRTP conTypeCRTP, newConTypeBonus conTypeBonus, target float64, abundance float64, i int) ([][]int, []uint32) {
	var bound []uint32
	switch i {
	case 1: //phase 1 & 2 殺
		if newConTypeCRTP.Single3Balls > target {
			result = append(result, []int{1, 0, 0, 0})
			bound = append(bound, ThreeConnectionPortion)
		}
		if newConTypeCRTP.Double3Balls > target {
			result = append(result, []int{2, 0, 0, 0})
			bound = append(bound, DoubleThreeConnectionPortion)
		}
		if newConTypeCRTP.Single4Balls > target {
			result = append(result, []int{0, 1, 0, 0})
			bound = append(bound, FourConnectionPortion)
		}
		if newConTypeCRTP.Single5Balls > target {
			result = append(result, []int{0, 0, 1, 0})
			bound = append(bound, FiveConnectionPortion)
		}
		if newConTypeCRTP.NoConnections > target {
			result = append(result, []int{0, 0, 0, 1})
			bound = append(bound, NoConnectionsPortion)
		}
	case 2: //phase 1 吐
		if newConTypeCRTP.Single3Balls < target && newConTypeBonus.Single3Balls < abundance {
			result = append(result, []int{1, 0, 0, 0})
			bound = append(bound, ThreeConnectionPortion)
		}
		if newConTypeCRTP.Double3Balls < target && newConTypeBonus.Double3Balls < abundance {
			result = append(result, []int{2, 0, 0, 0})
			bound = append(bound, DoubleThreeConnectionPortion)
		}
		if newConTypeCRTP.Single4Balls < target && newConTypeBonus.Single4Balls < abundance {
			result = append(result, []int{0, 1, 0, 0})
			bound = append(bound, FourConnectionPortion)
		}
		if newConTypeCRTP.Single5Balls < target && newConTypeBonus.Single5Balls < abundance {
			result = append(result, []int{0, 0, 1, 0})
			bound = append(bound, FiveConnectionPortion)
		}
		if newConTypeCRTP.NoConnections < target && newConTypeBonus.NoConnections < abundance {
			result = append(result, []int{0, 0, 0, 1})
			bound = append(bound, NoConnectionsPortion)
		}
	}
	return result, bound
}

//LineFinder 計算連球數
func LineFinder() {
	var (
		awardPhase  string  //award award_one_more
		betAmount   float64 = 1
		threePayoff float64 = 4
		fourPayoff  float64 = 16
		fivePayoff  float64 = 500
		current             = -68000.0 / 10000
		target              = -70000.0 / 10000
		fluctuation         = 100.0 / 10000
		commission          = 0.0 / 100
		HouseGain           = 100.0
		abundance           = (current - target) * HouseGain
	)
	//Get all Type bonus
	newConTypeBonus := conTypeBonus{}
	newConTypeBonus.Single3Balls = threePayoff * (1 - commission) * betAmount
	newConTypeBonus.Double3Balls = threePayoff * 2 * (1 - commission) * betAmount
	newConTypeBonus.Single4Balls = fourPayoff * (1 - commission) * betAmount
	newConTypeBonus.Single5Balls = fivePayoff * (1 - commission) * betAmount
	newConTypeBonus.NoConnections = 0
	newConTypeCRTP := conTypeCRTP{}
	newConTypeCRTP.Single3Balls = 1 - newConTypeBonus.Single3Balls/betAmount
	newConTypeCRTP.Double3Balls = 1 - newConTypeBonus.Double3Balls/betAmount
	newConTypeCRTP.Single4Balls = 1 - newConTypeBonus.Single4Balls/betAmount
	newConTypeCRTP.Single5Balls = 1 - newConTypeBonus.Single5Balls/betAmount
	newConTypeCRTP.NoConnections = 1
	awardPhase = "award"
	var result = make([][]int, 0)
	var bound []uint32
	if awardPhase == "award" {
		if current < target {
			result, bound = resultGenerator(result, newConTypeCRTP, newConTypeBonus, target, abundance, 1)
		} else if current >= target+fluctuation {
			result, bound = resultGenerator(result, newConTypeCRTP, newConTypeBonus, target, abundance, 2)
		}
		if len(result) == 0 {
			if current < target {
				result, bound = resultGenerator(result, newConTypeCRTP, newConTypeBonus, current, abundance, 1)
			} else if current >= target+fluctuation {
				result, bound = resultGenerator(result, newConTypeCRTP, newConTypeBonus, current, abundance, 2)
			}
		}
		fmt.Println(result)
		var release = make([]int, 4)
		if len(result) != 0 {
			release = randPick(result, bound)
		}
		fmt.Println(result, "---->", release)
	} else if awardPhase == "award_one_more" {
		if abundance <= 0 {
			abundance = 0
		}
		fmt.Println(abundance)
	}
}
