package awardcentertest

import (
	"fmt"
	"sort"
)

//雙層飛機沒機翼
func plane2NoWing(threes [][]int) [][]int {
	var plane [][]int
	if len(threes) > 1 {
		for i := 0; i < len(threes); i++ {
			for j := 0; j < len(threes); j++ {
				var planeTwo []int
				if i < j && threes[i][0]%10000 != threes[j][0]%10000 {
					planeTwo = []int{threes[i][0], threes[i][1], threes[i][2], threes[j][0], threes[j][1], threes[j][2]}
					a := planeBody(planeTwo)
					sort.Sort(sort.IntSlice(a))
					if straightCheck(a) {
						plane = append(plane, planeTwo)
					}
				}
			}
		}
	}
	return plane
}

//三層飛機沒機翼
func plane3NoWing(threes [][]int) [][]int {
	var plane [][]int
	if len(threes) > 2 {
		for i := 0; i < len(threes); i++ {
			for j := 0; j < len(threes); j++ {
				for l := 0; l < len(threes); l++ {
					var planeThree []int
					if i < j && j < l && threes[i][0]%10000 != threes[j][0]%10000 && threes[j][0]%10000 != threes[l][0]%10000 && threes[i][0]%10000 != threes[l][0]%10000 {
						planeThree = []int{threes[i][0], threes[i][1], threes[i][2], threes[j][0], threes[j][1], threes[j][2], threes[l][0], threes[l][1], threes[l][2]}
						a := planeBody(planeThree)
						sort.Sort(sort.IntSlice(a))
						if straightCheck(a) {
							plane = append(plane, planeThree)
						}
					}
				}
			}
		}
	}
	return plane
}

func pairCompare(a int, b int) int {
	if a == b {
		return 0
	}
	return 1

}

func pairWiseNotEqual(s []int) bool {
	var factor = 1
	for i := 0; i < len(s); i++ {
		for j := 0; j < len(s); j++ {
			if i != j {
				factor *= pairCompare(s[i]%10000, s[j]%10000)
			}
		}
	}
	if factor == 0 {
		return false
	}
	return true

}

//四層飛機沒機翼
func plane4NoWing(threes [][]int) [][]int {
	var plane [][]int
	if len(threes) > 3 {
		for i := 0; i < len(threes); i++ {
			for j := 0; j < len(threes); j++ {
				for m := 0; m < len(threes); m++ {
					for n := 0; n < len(threes); n++ {
						var planeFour []int
						if i < j && j < m && m < n &&
							pairWiseNotEqual([]int{threes[i][0], threes[j][0], threes[m][0], threes[n][0]}) {
							planeFour = []int{threes[i][0], threes[i][1], threes[i][2], threes[j][0], threes[j][1], threes[j][2], threes[m][0], threes[m][1], threes[m][2], threes[n][0], threes[n][1], threes[n][2]}
							a := planeBody(planeFour)
							sort.Sort(sort.IntSlice(a))
							if straightCheck(a) {
								plane = append(plane, planeFour)
							}
						}
					}
				}
			}
		}
	}
	return plane
}

//五層飛機沒機翼
func plane5NoWing(threes [][]int) [][]int {
	var plane [][]int
	if len(threes) > 1 {
		for i := 0; i < len(threes); i++ {
			for j := 0; j < len(threes); j++ {
				for l := 0; l < len(threes); l++ {
					for m := 0; m < len(threes); m++ {
						for n := 0; n < len(threes); n++ {
							var planeFive []int
							if i < j && j < l && l < m && m < n &&
								pairWiseNotEqual([]int{threes[i][0], threes[j][0], threes[l][0], threes[m][0], threes[n][0]}) {
								planeFive = []int{threes[i][0], threes[i][1], threes[i][2], threes[j][0], threes[j][1], threes[j][2], threes[l][0], threes[l][1], threes[l][2], threes[m][0], threes[m][1], threes[m][2], threes[n][0], threes[n][1], threes[n][2]}
								a := planeBody(planeFive)
								sort.Sort(sort.IntSlice(a))
								if straightCheck(a) {
									plane = append(plane, planeFive)
								}
							}
						}
					}
				}
			}
		}
	}
	return plane
}

func straightThreesFinder(threes [][]int, rep map[int]int, n int) {
	var threesInSequence = duplicateSequence(rep, 3)
	var straightThrees [][]int
	for i := 0; i < len(threesInSequence); i++ {
		for j := 0; j < len(threesInSequence)-i; j++ {
			if len(threesInSequence)-j-i == n && straightCheck(threesInSequence[i:len(threesInSequence)-j]) {
				straightThrees = append(straightThrees, threesInSequence[i:len(threesInSequence)-j])
			}
		}
	}
	fmt.Println(straightThrees)
	straightThreesGenerator(threes, straightThrees, n)
}

func straightThreesGenerator(threes [][]int, threesSets [][]int, n int) {
	for i := 0; i < len(threesSets); i++ {
		var threeRepo = make([][][]int, n)
		for j := 0; j < len(threesSets[i]); j++ {
			for l := 0; l < len(threes); l++ {
				if threes[l][0]%10000 == threesSets[i][j] {
					threeRepo[j] = append(threeRepo[j], threes[l])
				}
			}
		}
		straightThreesConstructor(threeRepo, []int{}, []int{}, 0)
	}
}

func straightThreesConstructor(threesRepo [][][]int, assembledThrees []int, completed []int, nth int) {
	for i := 0; i < len(threesRepo[nth]); i++ {
		if notIn(threesRepo[nth][i][1]%10000, completed) {
			assembledThrees = append(assembledThrees, threesRepo[nth][i][0])
			assembledThrees = append(assembledThrees, threesRepo[nth][i][1])
			assembledThrees = append(assembledThrees, threesRepo[nth][i][2])
			completed = append(completed, threesRepo[nth][i][1]%10000)
		}
		if nth < len(threesRepo)-1 {
			nth := nth + 1
			straightThreesConstructor(threesRepo, assembledThrees, completed, nth)
		} else {
			fmt.Println("final assembled Planes", assembledThrees) //丟出去的牌組
		}
		completed = completed[:len(completed)-1]
		assembledThrees = assembledThrees[:len(assembledThrees)-3]
	}
}

func straightPairsFinder(pairs [][]int, rep map[int]int, n int) {
	var pairInSequence = duplicateSequence(rep, 2)
	var straightPairs [][]int
	for i := 0; i < len(pairInSequence); i++ {
		for j := 0; j < len(pairInSequence)-i; j++ {
			if len(pairInSequence)-j-i == n && straightCheck(pairInSequence[i:len(pairInSequence)-j]) {
				straightPairs = append(straightPairs, pairInSequence[i:len(pairInSequence)-j])
			}
		}
	}
	straightPairsGenerator(pairs, straightPairs, n)
}

func straightPairsGenerator(pairs [][]int, pairSets [][]int, n int) {
	for i := 0; i < len(pairSets); i++ {
		var pairRepo = make([][][]int, n)
		for j := 0; j < len(pairSets[i]); j++ {
			for l := 0; l < len(pairs); l++ {
				if pairs[l][0]%10000 == pairSets[i][j] {
					pairRepo[j] = append(pairRepo[j], pairs[l])
				}
			}
		}
		straightPairsConstructor(pairRepo, []int{}, []int{}, 0)
	}
}

func straightPairsConstructor(pairRepo [][][]int, assembledPairs []int, completed []int, nth int) {
	for i := 0; i < len(pairRepo[nth]); i++ {
		if notIn(pairRepo[nth][i][1]%10000, completed) {
			assembledPairs = append(assembledPairs, pairRepo[nth][i][0])
			assembledPairs = append(assembledPairs, pairRepo[nth][i][1])
			completed = append(completed, pairRepo[nth][i][1]%10000)
		}
		if nth < len(pairRepo)-1 {
			nth := nth + 1
			straightPairsConstructor(pairRepo, assembledPairs, completed, nth)
		} else {
			fmt.Println("final assembledPairs", assembledPairs) //丟出去的牌組
		}
		completed = completed[:len(completed)-1]
		assembledPairs = assembledPairs[:len(assembledPairs)-2]
	}
}
