package awardcentertest

//揀選一張單張
func (h *hand) singlePick() {
	var singleCount = make(map[int]int)
	for i := range h.cardRepetitions {
		singleCount[i] = 0
	}
	for i := 0; i < len(h.Poker); i++ {
		if singleCount[h.Poker[i]%10000] == 0 {
			h.singleCards = append(h.singleCards, []int{h.Poker[i]})
			singleCount[h.Poker[i]%10000]++
		}
	}
}

//對每一個點數，揀選一組pairs
func (h *hand) pairPick() {
	var pairMap = make(map[int]int)
	for i, v := range h.cardRepetitions {
		if v > 1 {
			pairMap[i] = v
		}
	}
	for s := range pairMap {
		var count = 0
		var eachPair []int
		for i := 0; i < len(h.Poker); i++ {
			if h.Poker[i]%10000 == s {
				count++
				eachPair = append(eachPair, h.Poker[i])
			}
		}
		if len(eachPair) != 0 {
			h.pairs = append(h.pairs, []int{eachPair[0], eachPair[1]}) //取第一第二個組成的pair
		}
	}
}

//揀選一個三條
func (h *hand) threePick() {
	var threeMap = make(map[int]int)
	for i, v := range h.cardRepetitions {
		if v > 2 {
			threeMap[i] = v
		}
	}
	for s := range threeMap {
		var count = 0
		var eachThree []int
		for i := 0; i < len(h.Poker); i++ {
			if h.Poker[i]%10000 == s {
				count++
				eachThree = append(eachThree, h.Poker[i])
			}
		}
		if len(eachThree) != 0 {
			h.threes = append(h.threes, []int{eachThree[0], eachThree[1], eachThree[2]}) //取第一第二第三個組成的three
		}
	}
}

//雙層飛機有小翼
func (h *hand) plane2ShortWingPick() {
	if len(h.plane.planeL2NoWing) == 0 {
		return
	}
	for r := 0; r < len(h.plane.planeL2NoWing); r++ {
		//機翼是兩張單張不同點數的
		for i := 0; i < len(h.singleCards); i++ {
			for j := 0; j < len(h.singleCards); j++ {
				var eachCase []int
				if i < j && notIn(h.singleCards[i][0]%10000, planeBody(h.plane.planeL2NoWing[r])) &&
					notIn(h.singleCards[j][0]%10000, planeBody(h.plane.planeL2NoWing[r])) {
					for el := range h.plane.planeL2NoWing[r] {
						eachCase = append(eachCase, h.plane.planeL2NoWing[r][el])
					}
					eachCase = append(eachCase, h.singleCards[i][0])
					eachCase = append(eachCase, h.singleCards[j][0])
					h.plane.planeL2SWing = append(h.plane.planeL2SWing, eachCase)
				}
			}
		}
		//機翼是對子
		for u := 0; u < len(h.pairs); u++ {
			var eachCase []int
			if notIn(h.pairs[u][0]%10000, planeBody(h.plane.planeL2NoWing[r])) {
				for el := range h.plane.planeL2NoWing[r] {
					eachCase = append(eachCase, h.plane.planeL2NoWing[r][el])
				}
				for el := range h.pairs[u] {
					eachCase = append(eachCase, h.pairs[u][el])
				}
				h.plane.planeL2SWing = append(h.plane.planeL2SWing, eachCase)
			}
		}
	}
}

//三層飛機有小翼
func (h *hand) plane3ShortWingPick() {
	if len(h.plane.planeL3NoWing) < 1 || len(h.plane.planeL3NoWing[0]) != 9 {
		return
	}
	for r := 0; r < len(h.plane.planeL3NoWing); r++ {
		//機翼是三張單張不同點數的
		for i := 0; i < len(h.singleCards); i++ {
			for j := 0; j < len(h.singleCards); j++ {
				for k := 0; k < len(h.singleCards); k++ {
					var eachCase []int
					if i < j && j < k &&
						notIn(h.singleCards[i][0]%10000, planeBody(h.plane.planeL3NoWing[r])) &&
						notIn(h.singleCards[j][0]%10000, planeBody(h.plane.planeL3NoWing[r])) &&
						notIn(h.singleCards[k][0]%10000, planeBody(h.plane.planeL3NoWing[r])) {
						for el := range h.plane.planeL3NoWing[r] {
							eachCase = append(eachCase, h.plane.planeL3NoWing[r][el])
						}
						eachCase = append(eachCase, h.singleCards[i][0])
						eachCase = append(eachCase, h.singleCards[j][0])
						eachCase = append(eachCase, h.singleCards[k][0])
						h.plane.planeL3SWing = append(h.plane.planeL3SWing, eachCase)
					}
				}
			}
		}
		//機翼是一張單張跟一對子兩個點數不同
		for u := 0; u < len(h.pairs); u++ {
			for v := 0; v < len(h.singleCards); v++ {
				var eachCase []int
				if notIn(h.pairs[u][0]%10000, planeBody(h.plane.planeL3NoWing[r])) &&
					notIn(h.singleCards[v][0]%10000, planeBody(h.plane.planeL3NoWing[r])) &&
					h.singleCards[v][0]%10000 != h.pairs[u][0]%10000 {
					for el := range h.plane.planeL3NoWing[r] {
						eachCase = append(eachCase, h.plane.planeL3NoWing[r][el])
					}
					for el := range h.pairs[u] {
						eachCase = append(eachCase, h.pairs[u][el])
					}
					eachCase = append(eachCase, h.singleCards[v][0])
					h.plane.planeL3SWing = append(h.plane.planeL3SWing, eachCase)
				}
			}
		}
	}
}

//四層飛機有小翼
func (h *hand) plane4ShortWingPick() {
	if len(h.plane.planeL4NoWing) < 1 || len(h.plane.planeL4NoWing[0]) != 12 {
		return
	}
	for r := 0; r < len(h.plane.planeL4NoWing); r++ {
		//四張小翼point不同
		for i := 0; i < len(h.singleCards); i++ {
			for j := 0; j < len(h.singleCards); j++ {
				for k := 0; k < len(h.singleCards); k++ {
					for l := 0; l < len(h.singleCards); l++ {
						var eachCase []int
						if i < j && j < k && k < l &&
							notIn(h.singleCards[i][0]%10000, planeBody(h.plane.planeL4NoWing[r])) &&
							notIn(h.singleCards[j][0]%10000, planeBody(h.plane.planeL4NoWing[r])) &&
							notIn(h.singleCards[k][0]%10000, planeBody(h.plane.planeL4NoWing[r])) &&
							notIn(h.singleCards[l][0]%10000, planeBody(h.plane.planeL4NoWing[r])) {
							for el := range h.plane.planeL4NoWing[r] {
								eachCase = append(eachCase, h.plane.planeL4NoWing[r][el])
							}
							eachCase = append(eachCase, h.singleCards[i][0])
							eachCase = append(eachCase, h.singleCards[j][0])
							eachCase = append(eachCase, h.singleCards[k][0])
							eachCase = append(eachCase, h.singleCards[l][0])
							h.plane.planeL4SWing = append(h.plane.planeL4SWing, eachCase)
						}
					}
				}
			}
		}
		//四張小翼分為兩對
		for u := 0; u < len(h.pairs); u++ {
			for v := 0; v < len(h.pairs); v++ {
				var eachCase []int
				if u < v && notIn(h.pairs[u][1], h.pairs[v]) && notIn(h.pairs[u][0], h.pairs[v]) && notIn(h.pairs[u][0]%10000, planeBody(h.plane.planeL4NoWing[r])) &&
					notIn(h.pairs[v][0]%10000, planeBody(h.plane.planeL4NoWing[r])) {
					for el := range h.plane.planeL4NoWing[r] {
						eachCase = append(eachCase, h.plane.planeL4NoWing[r][el])
					}
					for el := range h.pairs[u] {
						eachCase = append(eachCase, h.pairs[u][el])
					}
					for el := range h.pairs[v] {
						eachCase = append(eachCase, h.pairs[v][el])
					}
					h.plane.planeL4SWing = append(h.plane.planeL4SWing, eachCase)
				}
			}
		}
		//四張小翼兩point不同，外加一對
		for u := 0; u < len(h.pairs); u++ {
			for s := 0; s < len(h.singleCards); s++ {
				for t := 0; t < len(h.singleCards); t++ {
					var eachCase []int
					if s < t && notIn(h.pairs[u][0]%10000, planeBody(h.plane.planeL4NoWing[r])) &&
						notIn(h.singleCards[s][0]%10000, planeBody(h.plane.planeL4NoWing[r])) &&
						notIn(h.singleCards[t][0]%10000, planeBody(h.plane.planeL4NoWing[r])) &&
						h.singleCards[s][0]%10000 != h.pairs[u][0]%10000 &&
						h.singleCards[t][0]%10000 != h.pairs[u][0]%10000 {
						for el := range h.plane.planeL4NoWing[r] {
							eachCase = append(eachCase, h.plane.planeL4NoWing[r][el])
						}
						for el := range h.pairs[u] {
							eachCase = append(eachCase, h.pairs[u][el])
						}
						eachCase = append(eachCase, h.singleCards[s][0])
						eachCase = append(eachCase, h.singleCards[t][0])
						h.plane.planeL4SWing = append(h.plane.planeL4SWing, eachCase)
					}
				}
			}
		}
		//小翼為三條＋單張
		for u := 0; u < len(h.threes); u++ {
			for s := 0; s < len(h.singleCards); s++ {
				var eachCase []int
				if notIn(h.singleCards[s][0], h.threes[u]) &&
					notIn(h.threes[u][0]%10000, planeBody(h.plane.planeL4NoWing[r])) &&
					notIn(h.singleCards[s][0]%10000, planeBody(h.plane.planeL4NoWing[r])) {
					for el := range h.plane.planeL4NoWing[r] {
						eachCase = append(eachCase, h.plane.planeL4NoWing[r][el])
					}
					for el := range h.threes[u] {
						eachCase = append(eachCase, h.threes[u][el])
					}
					eachCase = append(eachCase, h.singleCards[s][0])
					h.plane.planeL4SWing = append(h.plane.planeL4SWing, eachCase)
				}
			}
		}

	}
}

//五層飛機有小翼
func (h *hand) plane5ShortWingPick() {
	if len(h.plane.planeL5NoWing) < 1 || len(h.plane.planeL5NoWing[0]) != 15 {
		return
	}
	for r := 0; r < len(h.plane.planeL5NoWing); r++ {
		//機翼是五張單張不同點數
		for i := 0; i < len(h.singleCards); i++ {
			for j := 0; j < len(h.singleCards); j++ {
				for k := 0; k < len(h.singleCards); k++ {
					for l := 0; l < len(h.singleCards); l++ {
						for m := 0; m < len(h.singleCards); m++ {
							var eachCase []int
							if i < j && j < k && k < l && l < m &&
								notIn(h.singleCards[i][0]%10000, planeBody(h.plane.planeL5NoWing[r])) &&
								notIn(h.singleCards[j][0]%10000, planeBody(h.plane.planeL5NoWing[r])) &&
								notIn(h.singleCards[k][0]%10000, planeBody(h.plane.planeL5NoWing[r])) &&
								notIn(h.singleCards[l][0]%10000, planeBody(h.plane.planeL5NoWing[r])) &&
								notIn(h.singleCards[m][0]%10000, planeBody(h.plane.planeL5NoWing[r])) {

								for el := range h.plane.planeL5NoWing[r] {
									eachCase = append(eachCase, h.plane.planeL5NoWing[r][el])
								}
								eachCase = append(eachCase, h.singleCards[i][0])
								eachCase = append(eachCase, h.singleCards[j][0])
								eachCase = append(eachCase, h.singleCards[k][0])
								eachCase = append(eachCase, h.singleCards[l][0])
								eachCase = append(eachCase, h.singleCards[m][0])
								h.plane.planeL5SWing = append(h.plane.planeL5SWing, eachCase)
							}
						}
					}
				}
			}
		}
		//機翼是一張單張跟兩個對子
		for s := 0; s < len(h.singleCards); s++ {
			for u := 0; u < len(h.pairs); u++ {
				for v := 0; v < len(h.pairs); v++ {
					var eachCase []int
					if u < v && notIn(h.singleCards[s][0], h.pairs[u]) && notIn(h.singleCards[s][0], h.pairs[v]) &&
						notIn(h.pairs[u][1], h.pairs[v]) && notIn(h.pairs[u][0], h.pairs[v]) &&
						notIn(h.singleCards[s][0]%10000, planeBody(h.plane.planeL5NoWing[r])) &&
						notIn(h.pairs[u][0]%10000, planeBody(h.plane.planeL5NoWing[r])) &&
						notIn(h.pairs[v][0]%10000, planeBody(h.plane.planeL5NoWing[r])) {
						for el := range h.plane.planeL5NoWing[r] {
							eachCase = append(eachCase, h.plane.planeL5NoWing[r][el])
						}
						eachCase = append(eachCase, h.singleCards[s][0])
						for el := range h.pairs[u] {
							eachCase = append(eachCase, h.pairs[u][el])
						}
						for el := range h.pairs[v] {
							eachCase = append(eachCase, h.pairs[v][el])
						}
						h.plane.planeL5SWing = append(h.plane.planeL5SWing, eachCase)
					}
				}
			}
		}
		//機翼是兩張單張跟一個三條
		for u := 0; u < len(h.threes); u++ {
			for s := 0; s < len(h.singleCards); s++ {
				for t := 0; t < len(h.singleCards); t++ {
					var eachCase []int
					if s < t && notIn(h.threes[u][0]%10000, planeBody(h.plane.planeL5NoWing[r])) &&
						notIn(h.singleCards[s][0]%10000, planeBody(h.plane.planeL5NoWing[r])) &&
						notIn(h.singleCards[t][0]%10000, planeBody(h.plane.planeL5NoWing[r])) &&
						notIn(h.singleCards[s][0], h.threes[u]) && notIn(h.singleCards[t][0], h.threes[u]) {
						for el := range h.plane.planeL5NoWing[r] {
							eachCase = append(eachCase, h.plane.planeL5NoWing[r][el])
						}
						for el := range h.threes[u] {
							eachCase = append(eachCase, h.threes[u][el])
						}
						eachCase = append(eachCase, h.singleCards[s][0])
						eachCase = append(eachCase, h.singleCards[t][0])
						h.plane.planeL5SWing = append(h.plane.planeL5SWing, eachCase)
					}
				}
			}
		}
		//機翼是一個對子張跟一個三條
		for u := 0; u < len(h.threes); u++ {
			for v := 0; v < len(h.pairs); v++ {
				var eachCase []int
				if notIn(h.threes[u][0]%10000, planeBody(h.plane.planeL5NoWing[r])) &&
					notIn(h.pairs[v][0]%10000, planeBody(h.plane.planeL5NoWing[r])) &&
					notIn(h.pairs[v][0], h.threes[u]) && notIn(h.pairs[v][1], h.threes[u]) {
					for el := range h.plane.planeL5NoWing[r] {
						eachCase = append(eachCase, h.plane.planeL5NoWing[r][el])
					}
					for el := range h.threes[u] {
						eachCase = append(eachCase, h.threes[u][el])
					}
					for el := range h.pairs[v] {
						eachCase = append(eachCase, h.pairs[v][el])
					}
					h.plane.planeL5SWing = append(h.plane.planeL5SWing, eachCase)
				}
			}
		}
		//機翼是三張單張一個對子
		for u := 0; u < len(h.pairs); u++ {
			for s := 0; s < len(h.singleCards); s++ {
				for t := 0; t < len(h.singleCards); t++ {
					for l := 0; l < len(h.singleCards); l++ {
						var eachCase []int
						if s < t && t < l && notIn(h.pairs[u][0]%10000, planeBody(h.plane.planeL5NoWing[r])) &&
							notIn(h.singleCards[s][0]%10000, planeBody(h.plane.planeL5NoWing[r])) &&
							notIn(h.singleCards[t][0]%10000, planeBody(h.plane.planeL5NoWing[r])) &&
							notIn(h.singleCards[l][0]%10000, planeBody(h.plane.planeL5NoWing[r])) &&
							h.singleCards[s][0]%10000 != h.pairs[u][0]%10000 &&
							h.singleCards[t][0]%10000 != h.pairs[u][0]%10000 &&
							h.singleCards[l][0]%10000 != h.pairs[u][0]%10000 {
							for el := range h.plane.planeL5NoWing[r] {
								eachCase = append(eachCase, h.plane.planeL5NoWing[r][el])
							}
							for el := range h.pairs[u] {
								eachCase = append(eachCase, h.pairs[u][el])
							}
							eachCase = append(eachCase, h.singleCards[s][0])
							eachCase = append(eachCase, h.singleCards[t][0])
							eachCase = append(eachCase, h.singleCards[l][0])
							h.plane.planeL5SWing = append(h.plane.planeL5SWing, eachCase)
						}
					}
				}
			}
		}
	}
}

//雙層飛機有大翼
func (h *hand) plane2LongWingPick() {
	if len(h.plane.planeL2NoWing) < 1 || len(h.plane.planeL2NoWing[0]) != 6 || len(h.pairs) < 4 {
		return
	}
	for r := 0; r < len(h.plane.planeL2NoWing); r++ {
		//兩對不同的pairs
		for i := 0; i < len(h.pairs); i++ {
			for j := 0; j < len(h.pairs); j++ {
				var eachCase []int
				if i < j && notIn(h.pairs[i][0], h.pairs[j]) && notIn(h.pairs[i][1], h.pairs[j]) &&
					notIn(h.pairs[i][0]%10000, planeBody(h.plane.planeL2NoWing[r])) &&
					notIn(h.pairs[j][0]%10000, planeBody(h.plane.planeL2NoWing[r])) {
					for el := range h.plane.planeL2NoWing[r] {
						eachCase = append(eachCase, h.plane.planeL2NoWing[r][el])
					}
					for el := range h.pairs[i] {
						eachCase = append(eachCase, h.pairs[i][el])
					}
					for el := range h.pairs[j] {
						eachCase = append(eachCase, h.pairs[j][el])
					}
					//eachCase = append(h.plane.planeL2NoWing[r], h.pairs[i][0], h.pairs[i][1], h.pairs[j][0], h.pairs[j][1])
					h.plane.planeL2LWing = append(h.plane.planeL2LWing, eachCase)
				}
			}
		}
		//一個bomb
		for b := 0; b < len(h.bombs); b++ {
			var eachCase []int
			for el := range h.plane.planeL2NoWing[r] {
				eachCase = append(eachCase, h.plane.planeL2NoWing[r][el])
			}
			for el := range h.bombs[b] {
				eachCase = append(eachCase, h.bombs[b][el])
			}
			h.plane.planeL2LWing = append(h.plane.planeL2LWing, eachCase)
		}
	}
}

//三層飛機有大翼
func (h *hand) plane3LongWingPick() {
	if len(h.plane.planeL3NoWing) < 1 || len(h.plane.planeL3NoWing[0]) != 9 || len(h.pairs) < 6 {
		return
	}
	for r := 0; r < len(h.plane.planeL3NoWing); r++ {
		//三個不同的pairs
		for i := 0; i < len(h.pairs); i++ {
			for j := 0; j < len(h.pairs); j++ {
				for k := 0; k < len(h.pairs); k++ {
					var eachCase []int
					if i < j && j < k && notIn(h.pairs[i][0], h.pairs[j]) && notIn(h.pairs[i][1], h.pairs[j]) && notIn(h.pairs[i][0], h.pairs[k]) &&
						notIn(h.pairs[i][1], h.pairs[k]) && notIn(h.pairs[j][0], h.pairs[k]) && notIn(h.pairs[j][1], h.pairs[k]) &&
						notIn(h.pairs[i][0]%10000, planeBody(h.plane.planeL3NoWing[r])) && notIn(h.pairs[j][0]%10000, planeBody(h.plane.planeL3NoWing[r])) &&
						notIn(h.pairs[k][0]%10000, planeBody(h.plane.planeL3NoWing[r])) {
						for el := range h.plane.planeL3NoWing[r] {
							eachCase = append(eachCase, h.plane.planeL3NoWing[r][el])
						}
						for el := range h.pairs[i] {
							eachCase = append(eachCase, h.pairs[i][el])
						}
						for el := range h.pairs[j] {
							eachCase = append(eachCase, h.pairs[j][el])
						}
						for el := range h.pairs[k] {
							eachCase = append(eachCase, h.pairs[k][el])
						}

						//eachCase = append(h.plane.planeL3NoWing[r], h.pairs[i][0], h.pairs[i][1], h.pairs[j][0], h.pairs[j][1], h.pairs[k][0], h.pairs[k][1])
						h.plane.planeL3LWing = append(h.plane.planeL3LWing, eachCase)
					}
				}
			}
		}
		//一個炸彈一對pairs
		for b := 0; b < len(h.bombs); b++ {
			for u := 0; u < len(h.pairs); u++ {
				var eachCase []int
				if notIn(h.pairs[u][0], h.bombs[b]) && notIn(h.pairs[u][1], h.bombs[b]) &&
					notIn(h.bombs[b][0]%10000, planeBody(h.plane.planeL3NoWing[r])) &&
					notIn(h.pairs[u][0]%10000, planeBody(h.plane.planeL3NoWing[r])) {
					for el := range h.plane.planeL3NoWing[r] {
						eachCase = append(eachCase, h.plane.planeL3NoWing[r][el])
					}
					for el := range h.bombs[b] {
						eachCase = append(eachCase, h.bombs[b][el])
					}
					for el := range h.pairs[u] {
						eachCase = append(eachCase, h.pairs[u][el])
					}
					h.plane.planeL3LWing = append(h.plane.planeL3LWing, eachCase)
				}
			}
		}
	}
}

//四層飛機有大翼
func (h *hand) plane4LongWingPick() {
	if len(h.plane.planeL4NoWing) < 1 || len(h.plane.planeL4NoWing[0]) != 12 || len(h.pairs) < 8 {
		return
	}
	for r := 0; r < len(h.plane.planeL4NoWing); r++ {
		//四個不同的pairs
		for i := 0; i < len(h.pairs); i++ {
			for j := 0; j < len(h.pairs); j++ {
				for k := 0; k < len(h.pairs); k++ {
					for l := 0; l < len(h.pairs); l++ {
						var eachCase []int
						if i < j && j < k && k < l && pairNotIn(h.pairs[i], flattenPairs(h.pairs[j], h.pairs[k], h.pairs[l])) &&
							pairNotIn(h.pairs[j], flattenPairs(h.pairs[k], h.pairs[l])) && pairNotIn(h.pairs[k], h.pairs[l]) &&
							notIn(h.pairs[i][0]%10000, planeBody(h.plane.planeL4NoWing[r])) && notIn(h.pairs[j][0]%10000, planeBody(h.plane.planeL4NoWing[r])) &&
							notIn(h.pairs[k][0]%10000, planeBody(h.plane.planeL4NoWing[r])) && notIn(h.pairs[l][0]%10000, planeBody(h.plane.planeL4NoWing[r])) {
							eachCase = append(h.plane.planeL4NoWing[r], h.pairs[i][0], h.pairs[i][1], h.pairs[j][0], h.pairs[j][1], h.pairs[k][0], h.pairs[k][1], h.pairs[l][0], h.pairs[l][1])
							h.plane.planeL4LWing = append(h.plane.planeL4LWing, eachCase)
						}
					}
				}
			}
		}
		//兩個bombs
		for b := 0; b < len(h.bombs); b++ {
			for c := 0; c < len(h.bombs); c++ {
				var eachCase []int
				if b < c && notIn(h.bombs[c][0], h.bombs[b]) && notIn(h.bombs[c][1], h.bombs[b]) &&
					notIn(h.bombs[c][2], h.bombs[b]) && notIn(h.bombs[c][3], h.bombs[b]) &&
					notIn(h.bombs[b][0]%10000, planeBody(h.plane.planeL4NoWing[r])) &&
					notIn(h.bombs[c][0]%10000, planeBody(h.plane.planeL4NoWing[r])) {
					for el := range h.plane.planeL3NoWing[r] {
						eachCase = append(eachCase, h.plane.planeL4NoWing[r][el])
					}
					for el := range h.bombs[b] {
						eachCase = append(eachCase, h.bombs[b][el])
					}
					for el := range h.bombs[c] {
						eachCase = append(eachCase, h.bombs[c][el])
					}
					h.plane.planeL4LWing = append(h.plane.planeL4LWing, eachCase)
				}
			}
		}

		//一個bombs 兩個pairs
		for b := 0; b < len(h.bombs); b++ {
			for u := 0; u < len(h.pairs); u++ {
				for v := 0; v < len(h.pairs); v++ {
					var eachCase []int
					if u < v && notIn(h.pairs[u][0]%10000, h.bombs[b]) && notIn(h.pairs[u][1], h.bombs[b]) &&
						notIn(h.pairs[v][0], h.bombs[b]) && notIn(h.pairs[v][1], h.bombs[b]) &&
						notIn(h.bombs[b][0]%10000, planeBody(h.plane.planeL4NoWing[r])) &&
						notIn(h.pairs[u][0]%10000, planeBody(h.plane.planeL4NoWing[r])) &&
						notIn(h.pairs[v][0]%10000, planeBody(h.plane.planeL4NoWing[r])) {
						for el := range h.plane.planeL4NoWing[r] {
							eachCase = append(eachCase, h.plane.planeL4NoWing[r][el])
						}
						for el := range h.bombs[b] {
							eachCase = append(eachCase, h.bombs[b][el])
						}
						for el := range h.pairs[u] {
							eachCase = append(eachCase, h.pairs[u][el])
						}
						for el := range h.pairs[v] {
							eachCase = append(eachCase, h.pairs[v][el])
						}
						h.plane.planeL4LWing = append(h.plane.planeL4LWing, eachCase)
					}
				}
			}
		}
	}
}
