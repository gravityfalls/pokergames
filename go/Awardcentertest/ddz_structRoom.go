package awardcentertest

type InventoryNode struct {
	Key     InvNodeKeyBeta
	IsoNode Isopoints
}

type SegLayer struct {
	SegLayer []SegNode
}

type SegNode struct {
	CardSet   []int
	RestCards []int
	Seg       int
}

//Detail 遊戲紀錄的mongoDB架構
type Tree struct {
	//NodeIndex float64
	BunchNodes []Nodes
}
type Layer struct {
	Nodes []Nodes
	//Nodes map[int]Nodes
}

type Nodes struct {
	Order       int //出牌順序
	Index       int //Index number for this hand's pLayer options at this point, unique Index for each node
	PassCount   int
	ParentInfo  int   //this node's parent node
	ChildInfo   []int //this node's child Nodes
	CardSetInfo CardType
	RestCards   map[int][]int
	Outcome     []int //this binary point represents which party wins/loses, either (1,0) or (0,1).
}

//search isomorphic point with characteristic structure
type InvNodeKey struct {
	handOrder      int //出牌順序 0,1,2 no more
	PassCount      int
	CardTypeSerial int
}

type InvNodeKeyBeta struct {
	handOrder      int     //出牌順序 0,1,2 no more
	PassCount      int     //if PassCount !=2 CardTypeSerial matter, else if PassCount ==2 CardTypeSerial Doesn't matter
	CardSetCode    float64 //這裡要研究怎樣設定
	CardTypeSerial int
	HouseCode      float64
	Farmer1Code    float64
	Farmer2Code    float64
}

type Isopoints struct {
	Index         int
	CardSet       []int
	RestCards     map[int][]int
	Outcome       []int
	matchingTimes int
}

type CardType struct {
	CardSet        []int
	CardTypeSerial int
}

type hand struct {
	roleType        string
	Poker           []int
	cardRepetitions map[int]int
	typeExistence   map[int]bool
	cardRepWOTwo    map[int]int
	void            [][]int //0 cards, CardType serial = -1
	singleCards     [][]int //1 card, CardType serial = 0
	pairs           [][]int //2 cards, CardType serial = 1
	pairsWOTwo      [][]int //serving as intermediate usage
	threes          [][]int //3 cards, CardType serial = 2
	threesWOTwo     [][]int //serving as intermediate usage
	bombs           [][]int //4 cards, CardType serial = 3
	bombsWOTwo      [][]int //serving as intermediate usage
	kBomb           [][]int //2 cards, CardType serial = 4
	threeOnes       [][]int //4 cards, CardType serial = 5
	threeTwos       [][]int //5 cards, CardType serial = 6
	straight        straight
	plane           plane
	pairStraight    pairStraight
	fourOnes        [][]int // 6 cards, CardType serial = 35
	fourTwos        [][]int //8 cards, CardType serial = 36
	totalOptions    int
}

//單順子系列
type straight struct {
	//straight map[int][][]int //might need stars
	straight5  [][]int //5 cards, CardType serial = 7
	straight6  [][]int //6 cards, CardType serial = 8
	straight7  [][]int //7 cards, CardType serial = 9
	straight8  [][]int //8 cards, CardType serial = 10
	straight9  [][]int //9 cards, CardType serial = 11
	straight10 [][]int //10 cards, CardType serial = 12
	straight11 [][]int //11 cards, CardType serial = 13
	straight12 [][]int //12 cards, CardType serial = 14
}

//飛機系列
type plane struct {
	//飛機無翼
	//planeNoWing   map[int]*[][]int //2~6
	planeL2NoWing [][]int //6 cards 	//CardType serial = 15
	planeL3NoWing [][]int //9 cards 	//CardType serial = 16
	planeL4NoWing [][]int //12 cards	//CardType serial = 17
	planeL5NoWing [][]int //15 cards	//CardType serial = 18
	planeL6NoWing [][]int //18 cards	//CardType serial = 19
	//飛機小翼
	//planeSWing map[int]*[][]int //2~5
	planeL2SWing [][]int //8 cards		//CardType serial = 20
	planeL3SWing [][]int //12 cards		//CardType serial = 21
	planeL4SWing [][]int //16 cards		//CardType serial = 22
	planeL5SWing [][]int //20 cards		//CardType serial = 23
	//飛機大翼
	//planeLWing map[int]*[][]int //2~4
	planeL2LWing [][]int //10 cards		//CardType serial = 24
	planeL3LWing [][]int //15 cards		//CardType serial = 25
	planeL4LWing [][]int //20 cards		//CardType serial = 26
}

//對子順系列
type pairStraight struct {
	//pairStraight map[int]*[][]int
	pairStraight3  [][]int //6 cards	//CardType serial = 27
	pairStraight4  [][]int //8 cards	//CardType serial = 28
	pairStraight5  [][]int //10 cards	//CardType serial = 29
	pairStraight6  [][]int //12 cards	//CardType serial = 30
	pairStraight7  [][]int //14 cards	//CardType serial = 31
	pairStraight8  [][]int //16 cards	//CardType serial = 32
	pairStraight9  [][]int //18 cards	//CardType serial = 33
	pairStraight10 [][]int //20 cards	//CardType serial = 34
}

type cardsSetsOp struct {
	pile          map[int][]int
	counter       int
	typeExistence map[int]bool
}

const (
	//SuitShift 花色在 uint32 中的位移
	SuitShift = 10000
	//PointShift 花色在 uint32 中的位移
	PointShift = 100
)
