package awardcentertest

import (
	"fmt"
	"math"
	"math/rand"
	"time"
)

func randPool(m []CardSetInfo) int {
	var src cryptoSource
	rnd := rand.New(src)
	rand.Seed(int64(rnd.Intn(10000000000000000)))
	return rand.Intn(len(m))
}

func sum(s []uint32) uint32 {
	var sum uint32
	sum = 0
	for i := 0; i < len(s); i++ {
		sum += s[i]
	}
	return sum % 10
}
func pointOfCards(cards []uint32) []uint32 {
	var pointOfCards []uint32
	for i := 0; i < len(cards); i++ {
		if cards[i]%10000 < 10 {
			pointOfCards = append(pointOfCards, cards[i]%10000)
		} else if cards[i]%10000 == 14 {
			pointOfCards = append(pointOfCards, 1)
		} else {
			pointOfCards = append(pointOfCards, 0)
		}
	}
	return pointOfCards
}

func notInSet(c uint32, s []uint32) bool {
	var checkAll = 1
	for i := 0; i < len(s); i++ {
		if c == s[i] {
			checkAll *= 0
		}
	}
	if checkAll == 1 {
		return true
	}
	return false

}

func createCardSets(CardSetInfos []CardSetInfo, allCardSets [][][]uint32) []CardSetInfo {
	for i := 0; i < len(allCardSets); i++ {
		newCardSetInfo := CardSetInfo{}
		newCardSetInfo.CardSet = allCardSets[i]
		CardSetInfos = append(CardSetInfos, newCardSetInfo)
	}
	return CardSetInfos
}

//func createCardSets(CardSetInfos []CardSetInfo, dealCards [][]uint32) []CardSetInfo {
//	for i := 0; i < len(dealCards); i++ {
//		for j := 0; j < len(dealCards); j++ {
//			if i != j {
//				newCardSetInfo := CardSetInfo{}
//				newCardSetInfo.CardSet = [][]uint32{dealCards[i], dealCards[j]}
//				CardSetInfos = append(CardSetInfos, newCardSetInfo)
//				//fmt.Println(len(CardSetInfos))
//			}
//		}
//	}
//	return CardSetInfos
//}

func fillCardsInfos(CardSetInfos []CardSetInfo, pLayerDraw uint32, bankerDraw uint32, bet []uint64, sumBet uint64, target float64) {
	for i := 0; i < len(CardSetInfos); i++ {
		if len(CardSetInfos) != 0 {
			CardSetInfos[i].SetPoint = []uint32{sum(pointOfCards(CardSetInfos[i].CardSet[0])), sum(pointOfCards(CardSetInfos[i].CardSet[1]))}
			if CardSetInfos[i].SetPoint[0] <= 5 {
				//把牌組塞進閒家位置
				CardSetInfos[i].CardSet[0] = []uint32{CardSetInfos[i].CardSet[0][0], CardSetInfos[i].CardSet[0][1], pLayerDraw}
			}
			if CardSetInfos[i].SetPoint[1] <= 2 {
				//把牌組塞進莊家位置
				CardSetInfos[i].CardSet[1] = []uint32{CardSetInfos[i].CardSet[1][0], CardSetInfos[i].CardSet[1][1], bankerDraw}
			} else if CardSetInfos[i].SetPoint[1] == 3 && pointOfCards([]uint32{pLayerDraw})[0] != 8 {
				//把牌組塞進莊家位置
				CardSetInfos[i].CardSet[1] = []uint32{CardSetInfos[i].CardSet[1][0], CardSetInfos[i].CardSet[1][1], bankerDraw}
			} else if CardSetInfos[i].SetPoint[1] == 4 && notInSet(pointOfCards([]uint32{pLayerDraw})[0], []uint32{0, 1, 8, 9}) {
				//把牌組塞進莊家位置
				CardSetInfos[i].CardSet[1] = []uint32{CardSetInfos[i].CardSet[1][0], CardSetInfos[i].CardSet[1][1], bankerDraw}
			} else if CardSetInfos[i].SetPoint[1] == 5 && notInSet(pointOfCards([]uint32{pLayerDraw})[0], []uint32{0, 1, 2, 3, 8, 9}) {
				//把牌組塞進莊家位置
				CardSetInfos[i].CardSet[1] = []uint32{CardSetInfos[i].CardSet[1][0], CardSetInfos[i].CardSet[1][1], bankerDraw}
			} else if CardSetInfos[i].SetPoint[1] == 6 && notInSet(pointOfCards([]uint32{pLayerDraw})[0], []uint32{0, 1, 2, 3, 4, 5, 8, 9}) {
				//把牌組塞進莊家位置
				CardSetInfos[i].CardSet[1] = []uint32{CardSetInfos[i].CardSet[1][0], CardSetInfos[i].CardSet[1][1], bankerDraw}
			}
			//補完牌之後計算閒莊的分數
			CardSetInfos[i].SetPoint = []uint32{sum(pointOfCards(CardSetInfos[i].CardSet[0])), sum(pointOfCards(CardSetInfos[i].CardSet[1]))}
			if CardSetInfos[i].SetPoint[0] > CardSetInfos[i].SetPoint[1] {
				CardSetInfos[i].bonus += float64(bet[0]) * 2 * 0.95 // 閒家贏
			} else if CardSetInfos[i].SetPoint[0] < CardSetInfos[i].SetPoint[1] {
				CardSetInfos[i].bonus += float64(bet[1]) * 1.95 * 0.95 //莊家贏
				//莊對直接比牌面
			} else {
				CardSetInfos[i].bonus += 9 * float64(bet[2]) * 0.95 //和局
				//fmt.Println("和局", CardSetInfos[i])
			}
			//閒對/莊對中獎
			if CardSetInfos[i].CardSet[0][0]%SuitShift == CardSetInfos[i].CardSet[0][1]%SuitShift {
				CardSetInfos[i].bonus += float64(bet[3]*12) * 0.95 //閒家對贏
				//fmt.Println("閒對", CardSetInfos[i].CardSet[0][0], CardSetInfos[i].CardSet[0][1])
			}
			if CardSetInfos[i].CardSet[1][0]%SuitShift == CardSetInfos[i].CardSet[1][1]%SuitShift {
				CardSetInfos[i].bonus += float64(bet[4]*12) * 0.95 //莊家對贏
				//fmt.Println("莊對", CardSetInfos[i].CardSet[1][0], CardSetInfos[i].CardSet[1][1])
			}
			CardSetInfos[i].crtp = 1 - CardSetInfos[i].bonus/float64(sumBet)
			CardSetInfos[i].rateDiffer = math.Abs(CardSetInfos[i].crtp - target)
		}
	}
}

type CardSetInfo struct {
	CardSet    [][]uint32
	SetPoint   []uint32
	bonus      float64
	crtp       float64 //complement of RTP
	rateDiffer float64
}

//CookBaccarat 百家樂做牌
func CookBaccarat() {
	var Poker []uint32
	var deckNumber = 8
	for k := 1; k <= deckNumber; k++ {
		for i := 1; i <= 4; i++ {
			for j := 2; j <= 14; j++ {
				Poker = append(Poker, uint32(i*SuitShift+j))
			}
		}
	}
	//Poker = []uint32{10003, 10014, 10004, 10008, 30012, 20013, 10008, 10008, 10007, 40002, 40010, 20008, 30010, 40011, 30005, 40002, 40014, 10011, 20002, 20009}
	var target = 0.10 //0.05
	var current = 0.25
	var allowedRange = 0.01
	var bet = []uint64{0, 0, 1, 0, 0}
	var sumBet uint64
	for j := 0; j < len(bet); j++ {
		sumBet += bet[j]
	}
	var src cryptoSource
	rnd := rand.New(src)
	rand.Seed(rnd.Int63n(time.Now().UnixNano()))
	rand.Shuffle(len(Poker), func(i, j int) { Poker[i], Poker[j] = Poker[j], Poker[i] })
	Poker = Poker[:10]

	var allCardSets [][][]uint32
	for i := 0; i < len(Poker)-2; i++ {
		for j := 0; j < len(Poker)-2; j++ {
			if i != j {
				for s := 0; s < len(Poker)-2; s++ {
					for t := 0; t < len(Poker)-2; t++ {
						var dealCards [][]uint32
						dealCards = append(dealCards, []uint32{Poker[i], Poker[j]})
						if s != t && notIn(s, []int{i, j}) && notIn(t, []int{i, j}) {
							dealCards = append(dealCards, []uint32{Poker[s], Poker[t]})
							allCardSets = append(allCardSets, dealCards)
						}
					}
				}
			}
		}
	}
	//依據抽出的牌組排列出來組合
	var CardSetInfos []CardSetInfo
	var CardSetInfosDuplicated []CardSetInfo
	CardSetInfos = createCardSets(CardSetInfos, allCardSets)
	CardSetInfosDuplicated = createCardSets(CardSetInfosDuplicated, allCardSets)
	//待用的抽牌
	var pLayerDraw = Poker[len(Poker)-2]
	var bankerDraw = Poker[len(Poker)-1]

	fillCardsInfos(CardSetInfos, pLayerDraw, bankerDraw, bet, sumBet, target)
	fillCardsInfos(CardSetInfosDuplicated, bankerDraw, pLayerDraw, bet, sumBet, target) //flip intake of above combinations
	//pack everything into one
	var fullPack []CardSetInfo
	for i := 0; i < len(CardSetInfos); i++ {
		fullPack = append(fullPack, CardSetInfos[i])
		fullPack = append(fullPack, CardSetInfosDuplicated[i])
	}
	if len(fullPack) == 0 {
		fmt.Errorf("for some reason, we don't have card sets")
	}
	var pool []CardSetInfo //This selection according to current and target
	if current < target {
		fmt.Println("殺")
		for i := 0; i < len(fullPack); i++ {
			if fullPack[i].crtp > target {
				pool = append(pool, fullPack[i])
			}
		}
	} else if current >= target+allowedRange {
		fmt.Println("吐")
		for i := 0; i < len(fullPack); i++ {
			if fullPack[i].crtp < target {
				pool = append(pool, fullPack[i])
			}
		}
	}

	if len(pool) != 0 {
		var i = randPool(pool)
		fmt.Println(i, pool[i].CardSet)
	} else if len(fullPack) != 0 {
		fmt.Println("隨便丟出去一張")
		var i = randPool(fullPack)
		fmt.Println(i, fullPack[i].CardSet)
	}
	fmt.Errorf("for some reason, we don't have card sets")
}
