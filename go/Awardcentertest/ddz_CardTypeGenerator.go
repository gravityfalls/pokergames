package awardcentertest

import (
	"errors"
	"fmt"
	"log"
	"sort"
	"time"
)

func timeTrack(start time.Time, name string) {
	elapsed := time.Since(start)
	log.Printf("%s took %s", name, elapsed)
}
func (h *hand) totalCardOptions() {
	h.totalOptions = len(h.singleCards) + len(h.pairs) + len(h.threes) + len(h.bombs) + len(h.kBomb) + len(h.threeOnes) + len(h.threeTwos) +
		len(h.straight.straight5) + len(h.straight.straight6) + len(h.straight.straight7) + len(h.straight.straight8) + len(h.straight.straight9) +
		len(h.straight.straight10) + len(h.straight.straight11) + len(h.straight.straight12) +
		len(h.plane.planeL2NoWing) + len(h.plane.planeL3NoWing) + len(h.plane.planeL4NoWing) + len(h.plane.planeL5NoWing) + len(h.plane.planeL6NoWing) +
		len(h.plane.planeL2SWing) + len(h.plane.planeL3SWing) + len(h.plane.planeL4SWing) + len(h.plane.planeL5SWing) +
		len(h.plane.planeL2LWing) + len(h.plane.planeL3LWing) + len(h.plane.planeL4LWing) + len(h.pairStraight.pairStraight3) +
		len(h.pairStraight.pairStraight4) + len(h.pairStraight.pairStraight5) + len(h.pairStraight.pairStraight6) + len(h.pairStraight.pairStraight7) +
		len(h.pairStraight.pairStraight8) + len(h.pairStraight.pairStraight9) + len(h.pairStraight.pairStraight10) + len(h.fourOnes) + len(h.fourTwos)
}

func notInS2S(s1 []int, s2 []int) bool {
	var checkAll = 1
	for i := range s1 {
		if !notIn(s1[i], s2) {
			checkAll *= 0
		}
	}
	if checkAll == 1 {
		return true
	}
	return false
}

func isInS2S(s1 []int, s2 []int) bool {
	var checkAll = 1
	for i := range s1 {
		if notIn(s1[i], s2) {
			checkAll *= 0
		}
	}
	if checkAll == 1 {
		return true
	}
	return false
}

func isIn(c int, s []int) bool {
	for i := 0; i < len(s); i++ {
		if c == s[i] {
			return true
		}
	}
	return false
}

func sliceEqual(s1 []int, s2 []int) bool {
	if len(s1) != len(s2) {
		return false
	}
	var checkall = 1
	for i := range s1 {
		if isIn(s1[i], s2) {
			checkall *= 1
		} else {
			checkall *= 0
		}
	}
	if checkall == 1 {
		return true
	}
	return false
}

func notIn(c int, s []int) bool {
	var checkAll = 1
	for i := 0; i < len(s); i++ {
		if c == s[i] {
			checkAll *= 0
		}
	}
	if checkAll == 1 {
		return true
	}
	return false
}

func findSmall(s []int) int {
	var reference = 100
	for i := 0; i < len(s); i++ {
		if s[i] < reference {
			reference = s[i]
		}
	}
	return reference
}

func (h *hand) repetitions() map[int]int {
	h.cardRepetitions = make(map[int]int)
	for i := 0; i < len(h.Poker); i++ {
		if h.cardRepetitions[h.Poker[i]%10000] != 0 {
			h.cardRepetitions[h.Poker[i]%10000]++
		} else {
			h.cardRepetitions[h.Poker[i]%10000] = 1
		}
	}
	return h.cardRepetitions
}

func (h *hand) repetitionsWOTwo() map[int]int {
	h.cardRepWOTwo = make(map[int]int)
	for i := 0; i < len(h.Poker); i++ {
		if h.Poker[i]%10000 != 15 {
			if h.cardRepWOTwo[h.Poker[i]%10000] != 0 {
				h.cardRepWOTwo[h.Poker[i]%10000]++
			} else {
				h.cardRepWOTwo[h.Poker[i]%10000] = 1
			}
		}
	}
	return h.cardRepWOTwo
}

func (h *hand) pass() {
	h.void = append(h.void, []int{})
}

//所有可能的單張
func (h *hand) single() {
	for i := 0; i < len(h.Poker); i++ {
		h.singleCards = append(h.singleCards, []int{h.Poker[i]})
	}
}

//所有可能的pairs
func (h *hand) pair() {
	var pairMap = make(map[int]int)
	for i, v := range h.cardRepetitions {
		if v > 1 {
			pairMap[i] = v
		}
	}
	for s := range pairMap {
		var count = 0
		var eachPair []int
		for i := 0; i < len(h.Poker); i++ {
			if h.Poker[i]%10000 == s {
				count++
				eachPair = append(eachPair, h.Poker[i])
			}
		}
		for l := 0; l < len(eachPair); l++ {
			for m := 0; m < len(eachPair); m++ {
				if l < m {
					h.pairs = append(h.pairs, []int{eachPair[l], eachPair[m]})
				}
			}
		}
	}
}

//所有可能的pairs without Two
func (h *hand) pairWOTwo() {
	var pairMap = make(map[int]int)
	for i, v := range h.cardRepWOTwo {
		if v > 1 {
			pairMap[i] = v
		}
	}
	for s := range pairMap {
		var count = 0
		var eachPair []int
		for i := 0; i < len(h.Poker); i++ {
			if h.Poker[i]%10000 == s {
				count++
				eachPair = append(eachPair, h.Poker[i])
			}
		}
		if len(eachPair) != 0 {
			h.pairsWOTwo = append(h.pairsWOTwo, []int{eachPair[0], eachPair[1]}) //取第一第二個組成的pair
		}
		//for l := 0; l < len(eachPair); l++ {
		//	for m := 0; m < len(eachPair); m++ {
		//		if l < m {
		//			h.pairsWOTwo = append(h.pairsWOTwo, []int{eachPair[l], eachPair[m]})
		//		}
		//	}
		//}
	}
}

//所有可能的三張
func (h *hand) three() {
	var threeMap = make(map[int]int)
	for i, v := range h.cardRepetitions {
		if v > 2 {
			threeMap[i] = v
		}
	}
	for s := range threeMap {
		var count = 0
		var eachThree []int
		for i := 0; i < len(h.Poker); i++ {
			if h.Poker[i]%10000 == s {
				count++
				eachThree = append(eachThree, h.Poker[i])
			}
		}
		for l := 0; l < len(eachThree); l++ {
			for m := 0; m < len(eachThree); m++ {
				for n := 0; n < len(eachThree); n++ {
					if l < m && m < n {
						h.threes = append(h.threes, []int{eachThree[l], eachThree[m], eachThree[n]})
					}
				}
			}
		}
	}
}

//揀選三條 without Two
func (h *hand) threeWOTwo() {
	var threeMap = make(map[int]int)
	for i, v := range h.cardRepWOTwo {
		if v > 2 {
			threeMap[i] = v
		}
	}
	for s := range threeMap {
		var count = 0
		var eachThree []int
		for i := 0; i < len(h.Poker); i++ {
			if h.Poker[i]%10000 == s {
				count++
				eachThree = append(eachThree, h.Poker[i])
			}
		}
		if len(eachThree) != 0 {
			h.threesWOTwo = append(h.threesWOTwo, []int{eachThree[0], eachThree[1], eachThree[2]}) //取第一第二第三個組成的three
		}
	}
}

//所有可能的三帶一
func (h *hand) threeOne(p []int, threes [][]int) {
	for i := 0; i < len(threes); i++ {
		var singleCount = make(map[int]int)
		for i := range h.cardRepetitions {
			singleCount[i] = 0
		}
		for j := 0; j < len(p); j++ {
			var eachThreeOne []int
			if p[j]%10000 != threes[i][0]%10000 && singleCount[p[j]%10000] == 0 {
				eachThreeOne = append(threes[i], p[j]) // could be something wrong here...
				h.threeOnes = append(h.threeOnes, eachThreeOne)
				singleCount[p[j]%10000]++
			}
		}
	}
}

//所有可能的三帶二
func (h *hand) threeTwo(twos [][]int, threes [][]int) {
	var pairMap = make(map[int]int)
	for i, v := range h.cardRepetitions {
		if v > 1 {
			pairMap[i] = v
		}
	}
	for i := 0; i < len(threes); i++ {
		var pairCount = make(map[int]int)
		for i := range h.cardRepetitions {
			pairCount[i] = 0
		}
		for j := 0; j < len(twos); j++ {
			var eachThreeTwo []int
			if notIn(twos[j][0], threes[i]) && notIn(twos[j][1], threes[i]) && pairCount[twos[j][0]] == 0 {
				eachThreeTwo = append(threes[i], twos[j][0], twos[j][1])
				h.threeTwos = append(h.threeTwos, eachThreeTwo)
				pairCount[twos[j][0]]++
			}
		}
	}
}

//王炸
func (h *hand) kingBomb() {
	var kings []int
	for i := 0; i < len(h.Poker); i++ {
		if h.Poker[i]/10000 == 5 {
			kings = append(kings, h.Poker[i])
		}
	}
	if len(kings) == 2 {
		h.kBomb = [][]int{kings}
	}
}

//所有可能的炸彈
func (h *hand) bomb() {
	var fourMap = make(map[int]int)
	for i, v := range h.cardRepetitions {
		if v > 3 {
			fourMap[i] = v
		}
	}
	for s := range fourMap {
		var count = 0
		var eachFour []int
		for i := 0; i < len(h.Poker); i++ {
			if h.Poker[i]%10000 == s {
				count++
				eachFour = append(eachFour, h.Poker[i])
			}
		}
		for l := 0; l < len(eachFour); l++ {
			for m := 0; m < len(eachFour); m++ {
				for n := 0; n < len(eachFour); n++ {
					for o := 0; o < len(eachFour); o++ {
						if l < m && m < n && n < o {
							h.bombs = append(h.bombs, []int{eachFour[l], eachFour[m], eachFour[n], eachFour[o]})
						}
					}
				}
			}
		}
	}
}

//所有可能的炸彈 without Two
func (h *hand) bombWOTwo() {
	var fourMap = make(map[int]int)
	for i, v := range h.cardRepWOTwo {
		if v > 3 {
			fourMap[i] = v
		}
	}
	for s := range fourMap {
		var count = 0
		var eachFour []int
		for i := 0; i < len(h.Poker); i++ {
			if h.Poker[i]%10000 == s {
				count++
				eachFour = append(eachFour, h.Poker[i])
			}
		}
		for l := 0; l < len(eachFour); l++ {
			for m := 0; m < len(eachFour); m++ {
				for n := 0; n < len(eachFour); n++ {
					for o := 0; o < len(eachFour); o++ {
						if l < m && m < n && n < o {
							h.bombsWOTwo = append(h.bombsWOTwo, []int{eachFour[l], eachFour[m], eachFour[n], eachFour[o]})
						}
					}
				}
			}
		}
	}
}

func (h *hand) fourOne() {
	for i := 0; i < len(h.bombs); i++ {
		for j := 0; j < len(h.singleCards); j++ {
			for l := 0; l < len(h.singleCards); l++ {
				var eachFourOne []int
				if j < l && notIn(h.singleCards[j][0], h.bombs[i]) && notIn(h.singleCards[l][0], h.bombs[i]) {
					eachFourOne = append(h.bombs[i], h.singleCards[j][0], h.singleCards[l][0])
					h.fourOnes = append(h.fourOnes, eachFourOne)
				}
			}
		}
		for u := 0; u < len(h.pairs); u++ {
			var eachFourOne []int
			if notIn(h.pairs[u][0], h.bombs[i]) && notIn(h.pairs[u][1], h.bombs[i]) {
				eachFourOne = append(h.bombs[i], h.pairs[u][0], h.pairs[u][1])
				h.fourOnes = append(h.fourOnes, eachFourOne)
			}
		}
	}
}

func (h *hand) fourTwo() {
	for i := 0; i < len(h.bombs); i++ {
		for j := 0; j < len(h.pairs); j++ {
			for l := 0; l < len(h.pairs); l++ {
				var eachFourTwo []int
				if j < l && notIn(h.pairs[j][0], h.pairs[l]) && notIn(h.pairs[j][1], h.pairs[l]) &&
					h.pairs[j][0]%10000 != h.bombs[i][0]%10000 && h.pairs[l][0]%10000 != h.bombs[i][0]%10000 {
					eachFourTwo = append(h.bombs[i], h.pairs[j][0], h.pairs[j][1], h.pairs[l][0], h.pairs[l][1])
					h.fourTwos = append(h.fourTwos, eachFourTwo)
				}
			}
		}
	}
}

func elementWiseEqual(s []int, c []int) bool {
	var chk = 1
	for i := 0; i < len(s); i++ {
		if s[i] != c[i] {
			chk = 0
		}
	}
	if chk == 1 {
		return true
	}
	return false

}

func (h *hand) straightFinder(thisType *[][]int, n int) {
	var rep = h.cardRepWOTwo
	var singlesInSequence = duplicateSequence(rep, 1)
	var singleStraights [][]int
	for i := 0; i < len(singlesInSequence); i++ {
		for j := 0; j < len(singlesInSequence)-i; j++ {
			if len(singlesInSequence)-j-i == n && straightCheck(singlesInSequence[i:len(singlesInSequence)-j]) {
				singleStraights = append(singleStraights, singlesInSequence[i:len(singlesInSequence)-j])
			}
		}
	}
	h.straightSinglesGenerator(singleStraights, thisType, n)
}

func (h *hand) straightSinglesGenerator(singleStraights [][]int, thisType *[][]int, n int) {
	for i := 0; i < len(singleStraights); i++ {
		if findSmall(singleStraights[i]) <= 2 && i != len(singleStraights)-1 {
			i = i + 1
		}
		var SingleRepo = make([][][]int, n)
		for j := 0; j < len(singleStraights[i]); j++ {
			for l := 0; l < len(h.singleCards); l++ {
				if h.singleCards[l][0]%10000 == singleStraights[i][j] {
					SingleRepo[j] = append(SingleRepo[j], h.singleCards[l])
				}
			}
		}
		straightSingleConstructor(SingleRepo, []int{}, thisType, []int{}, 0)
	}
}

func straightSingleConstructor(SingleRepo [][][]int, assembledSingles []int, thisType *[][]int, completed []int, nth int) {
	for i := 0; i < len(SingleRepo[nth]); i++ {
		if notIn(SingleRepo[nth][i][0]%10000, completed) {
			for j := 0; j < len(SingleRepo[nth][i]); j++ {
				assembledSingles = append(assembledSingles, SingleRepo[nth][i][j])
			}
			completed = append(completed, SingleRepo[nth][i][0]%10000)
		}
		if nth < len(SingleRepo)-1 {
			n := nth + 1
			straightSingleConstructor(SingleRepo, assembledSingles, thisType, completed, n)
		} else {
			//fmt.Println(assembledDuplicates) //丟出去的牌組
			var temp []int
			for _, v := range assembledSingles {
				temp = append(temp, v)
			}
			*thisType = append(*thisType, temp)
		}
		completed = completed[:len(completed)-1]
		assembledSingles = assembledSingles[:len(assembledSingles)-1]
	}
}

//duplicateSequence 找尋重複N次以上的數字來組成duplicates候選清單
func duplicateSequence(rep map[int]int, n int) []int {
	var duplicateSequence []int
	for i, v := range rep {
		if v > n-1 {
			duplicateSequence = append(duplicateSequence, i)
		}
	}
	sort.Sort(sort.IntSlice(duplicateSequence))
	return duplicateSequence
}

func duplicateSequenceForOpening(rep map[int]int, n int) [][]int {
	var duplicateSequence []int
	for i, v := range rep {
		if v == n {
			duplicateSequence = append(duplicateSequence, i)
		}
	}
	sort.Sort(sort.IntSlice(duplicateSequence))
	//return duplicateSequence
	var singlesInSequence = duplicateSequence
	var singleStraights [][]int
	for i := 0; i < len(singlesInSequence); i++ {
		for j := 0; j < len(singlesInSequence)-i; j++ {
			if len(singlesInSequence)-j-i == 2 && straightCheck(singlesInSequence[i:len(singlesInSequence)-j]) {
				singleStraights = append(singleStraights, singlesInSequence[i:len(singlesInSequence)-j])
			}
		}
	}
	return singleStraights
}

//straightDuplicatesFinder 找尋duplicates的順子
func (h *hand) straightDuplicatesFinder(duplicates [][]int, thisType *[][]int, n int) {
	if len(duplicates) == 0 {
		fmt.Println("No building blocks")
	}
	var multiples = len(duplicates[0])
	var rep = h.cardRepWOTwo
	var duplicatesInSequence = duplicateSequence(rep, multiples)
	var straightDuplicates [][]int
	for i := 0; i < len(duplicatesInSequence); i++ {
		for j := 0; j < len(duplicatesInSequence)-i; j++ {
			if len(duplicatesInSequence)-j-i == n && straightCheck(duplicatesInSequence[i:len(duplicatesInSequence)-j]) {
				straightDuplicates = append(straightDuplicates, duplicatesInSequence[i:len(duplicatesInSequence)-j])
			}
		}
	}
	straightDuplicatesGenerator(duplicates, straightDuplicates, thisType, multiples, n)
}

func straightDuplicatesGenerator(duplicates [][]int, duplicateSets [][]int, thisType *[][]int, multiples int, n int) {
	for i := 0; i < len(duplicateSets); i++ {
		var duplicateRepo = make([][][]int, n)
		for j := 0; j < len(duplicateSets[i]); j++ {
			for l := 0; l < len(duplicates); l++ {
				if duplicates[l][0]%10000 == duplicateSets[i][j] {
					duplicateRepo[j] = append(duplicateRepo[j], duplicates[l])
				}
			}
		}
		straightDuplicatesConstructor(duplicateRepo, []int{}, thisType, []int{}, multiples, 0)
	}
}

func straightDuplicatesConstructor(duplicateRepo [][][]int, assembledDuplicates []int, thisType *[][]int, completed []int, multiples int, nth int) {
	for i := 0; i < len(duplicateRepo[nth]); i++ {
		if notIn(duplicateRepo[nth][i][1]%10000, completed) {
			for j := 0; j < len(duplicateRepo[nth][i]); j++ {
				assembledDuplicates = append(assembledDuplicates, duplicateRepo[nth][i][j])
			}
			completed = append(completed, duplicateRepo[nth][i][0]%10000)
		}
		if nth < len(duplicateRepo)-1 {
			n := nth + 1
			straightDuplicatesConstructor(duplicateRepo, assembledDuplicates, thisType, completed, multiples, n)
		} else {
			//fmt.Println(assembledDuplicates) //丟出去的牌組
			var temp []int
			for _, v := range assembledDuplicates {
				temp = append(temp, v)
			}
			*thisType = append(*thisType, temp)
		}
		completed = completed[:len(completed)-1]
		assembledDuplicates = assembledDuplicates[:len(assembledDuplicates)-multiples]
	}
}

func notInPairs(s []int, t [][]int) bool {
	var check = 1
	for i := 0; i < len(t); i++ {
		if elementWiseEqual(s, t[i]) {
			check = 0
		}
	}
	if check == 0 {
		return false
	}
	return true
}

//順子檢測
func straightCheck(s []int) bool {
	var r = 1
	for i := 0; i < len(s)-1; i++ {
		if s[i+1]-s[i] == 1 {
			r *= 1
		} else {
			r *= 0
		}
	}
	if r == 1 {
		return true
	}
	return false

}

//機身數字
func planeBody(threeOne []int) []int {
	var bodyWingMap = make(map[int]int)
	for i := 0; i < len(threeOne); i++ {
		bodyWingMap[threeOne[i]%10000]++
	}
	var body []int
	for i, v := range bodyWingMap {
		if v >= 3 {
			body = append(body, i)
		}
	}
	return body
}

func getPoint(cards []int) []int {
	var points []int
	for i := range cards {
		points = append(points, cards[i])
	}
	return points
}

func windCards(given []int, thisBody []int) []int {
	var wingCards []int
	for i := range given {
		if notIn(given[i]%SuitShift, thisBody) {
			wingCards = append(wingCards, given[i])
		}
	}
	return wingCards
}

//機翼數字
func notPlaneBody(plane []int) []int {
	var bodyWingMap = make(map[int]int)
	for i := 0; i < len(plane); i++ {
		bodyWingMap[plane[i]%10000]++
	}
	var wings []int
	for i, v := range bodyWingMap {
		if v < 3 {
			wings = append(wings, i)
		}
	}
	return wings
}

func notPlaneBodyCards(plane []int) []int {
	var bodyWingMap = make(map[int]int)
	for i := 0; i < len(plane); i++ {
		bodyWingMap[plane[i]%10000]++
	}
	var wingCards []int
	for i := range plane {
		if bodyWingMap[plane[i]%SuitShift] < 3 {
			wingCards = append(wingCards, plane[i])
		}
	}
	return wingCards
}

func fourBody(threeOne []int) []int {
	var bodyWingMap = make(map[int]int)
	for i := 0; i < len(threeOne); i++ {
		bodyWingMap[threeOne[i]%10000]++
	}
	var body []int
	for i, v := range bodyWingMap {
		if v == 4 {
			body = append(body, i)
		}
	}
	return body
}

func planeBodyT(threeOne []int) []int {
	var bodyWingMap = make(map[int]int)
	for _, v := range threeOne {
		bodyWingMap[v%10000]++
		//bodyWingMap[threeOne[i]%10000]++
	}
	var body []int
	for i, v := range bodyWingMap {
		if v == 3 {
			body = append(body, i)
		}
	}
	return body
}

func (h *hand) planeWingConstructor(thisPlane []int) []int {
	var wings []int
	for i := 0; i < len(h.Poker); i++ {
		if notIn(h.Poker[i], thisPlane) {
			wings = append(wings, h.Poker[i])
		}
	}
	//fmt.Println(wings)
	return nil
}

//雙層飛機有小翼
func (h *hand) plane2ShortWing() {
	if len(h.plane.planeL2NoWing) == 0 {
		return
	}
	for r := 0; r < len(h.plane.planeL2NoWing); r++ {
		for i := 0; i < len(h.Poker); i++ {
			for j := 0; j < len(h.Poker); j++ {
				var eachCase []int
				if h.Poker[i] < h.Poker[j] && notIn(h.Poker[i]%10000, planeBody(h.plane.planeL2NoWing[r])) && notIn(h.Poker[j]%10000, planeBody(h.plane.planeL2NoWing[r])) {
					eachCase = append(h.plane.planeL2NoWing[r], h.Poker[i], h.Poker[j])
					h.plane.planeL2SWing = append(h.plane.planeL2SWing, eachCase)
				}
			}
		}
	}
}

//三層飛機有小翼
func (h *hand) plane3ShortWing() {
	if len(h.plane.planeL3NoWing) < 1 || len(h.plane.planeL3NoWing[0]) != 9 {
		return
	}
	for r := 0; r < len(h.plane.planeL3NoWing); r++ {
		for i := 0; i < len(h.Poker); i++ {
			for j := 0; j < len(h.Poker); j++ {
				for k := 0; k < len(h.Poker); k++ {
					var eachCase []int
					if h.Poker[i] < h.Poker[j] && h.Poker[j] < h.Poker[k] && notIn(h.Poker[i]%10000, planeBody(h.plane.planeL3NoWing[r])) &&
						notIn(h.Poker[j]%10000, planeBody(h.plane.planeL3NoWing[r])) && notIn(h.Poker[k]%10000, planeBody(h.plane.planeL3NoWing[r])) {
						eachCase = append(h.plane.planeL3NoWing[r], h.Poker[i], h.Poker[j], h.Poker[k])
						h.plane.planeL3SWing = append(h.plane.planeL3SWing, eachCase)
					}
				}
			}
		}
	}
}

//四層飛機有小翼
func (h *hand) plane4ShortWing() {
	if len(h.plane.planeL4NoWing) < 1 || len(h.plane.planeL4NoWing[0]) != 12 {
		return
	}
	for r := 0; r < len(h.plane.planeL4NoWing); r++ {
		for i := 0; i < len(h.Poker); i++ {
			for j := 0; j < len(h.Poker); j++ {
				for k := 0; k < len(h.Poker); k++ {
					for l := 0; l < len(h.Poker); l++ {
						var eachCase []int
						if h.Poker[i] < h.Poker[j] && h.Poker[j] < h.Poker[k] && h.Poker[k] < h.Poker[l] && notIn(h.Poker[i]%10000, planeBody(h.plane.planeL4NoWing[r])) &&
							notIn(h.Poker[j]%10000, planeBody(h.plane.planeL4NoWing[r])) && notIn(h.Poker[k]%10000, planeBody(h.plane.planeL4NoWing[r])) &&
							notIn(h.Poker[l]%10000, planeBody(h.plane.planeL4NoWing[r])) {
							eachCase = append(h.plane.planeL4NoWing[r], h.Poker[i], h.Poker[j], h.Poker[k], h.Poker[l])
							h.plane.planeL4SWing = append(h.plane.planeL4SWing, eachCase)
						}
					}
				}
			}
		}
	}
}

//雙層飛機有大翼
func (h *hand) plane2LongWing() {
	if len(h.plane.planeL2NoWing) < 1 || len(h.plane.planeL2NoWing[0]) != 6 || len(h.pairs) < 4 {
		return
	}
	for r := 0; r < len(h.plane.planeL2NoWing); r++ {
		for i := 0; i < len(h.pairs); i++ {
			for j := 0; j < len(h.pairs); j++ {
				var eachCase []int
				if i < j && notIn(h.pairs[i][0], h.pairs[j]) && notIn(h.pairs[i][1], h.pairs[j]) && notIn(h.pairs[i][0]%10000, planeBody(h.plane.planeL2NoWing[r])) &&
					notIn(h.pairs[j][0]%10000, planeBody(h.plane.planeL2NoWing[r])) {
					eachCase = append(h.plane.planeL2NoWing[r], h.pairs[i][0], h.pairs[i][1], h.pairs[j][0], h.pairs[j][1])
					h.plane.planeL2LWing = append(h.plane.planeL2LWing, eachCase)
				}
			}
		}
	}
}

//三層飛機有大翼
func (h *hand) plane3LongWing() {
	if len(h.plane.planeL3NoWing) < 1 || len(h.plane.planeL3NoWing[0]) != 9 || len(h.pairs) < 6 {
		return
	}
	for r := 0; r < len(h.plane.planeL3NoWing); r++ {
		for i := 0; i < len(h.pairs); i++ {
			for j := 0; j < len(h.pairs); j++ {
				for k := 0; k < len(h.pairs); k++ {
					var eachCase []int
					if i < j && j < k && notIn(h.pairs[i][0], h.pairs[j]) && notIn(h.pairs[i][1], h.pairs[j]) && notIn(h.pairs[i][0], h.pairs[k]) &&
						notIn(h.pairs[i][1], h.pairs[k]) && notIn(h.pairs[j][0], h.pairs[k]) && notIn(h.pairs[j][1], h.pairs[k]) &&
						notIn(h.pairs[i][0]%10000, planeBody(h.plane.planeL3NoWing[r])) && notIn(h.pairs[j][0]%10000, planeBody(h.plane.planeL3NoWing[r])) &&
						notIn(h.pairs[k][0]%10000, planeBody(h.plane.planeL3NoWing[r])) {
						eachCase = append(h.plane.planeL3NoWing[r], h.pairs[i][0], h.pairs[i][1], h.pairs[j][0], h.pairs[j][1], h.pairs[k][0], h.pairs[k][1])
						h.plane.planeL3LWing = append(h.plane.planeL3LWing, eachCase)
					}
				}
			}
		}
	}
}

func doFlatten(acc []int, arr interface{}) ([]int, error) {
	var err error

	switch v := arr.(type) {
	case []int:
		acc = append(acc, v...)
	case int:
		acc = append(acc, v)
	case []interface{}:
		for i := range v {
			acc, err = doFlatten(acc, v[i])
			if err != nil {
				return nil, errors.New("not int or []int given")
			}
		}
	default:
		return nil, errors.New("not int given")
	}

	return acc, nil
}

func flattenPairs(args ...interface{}) []int {
	var restPairs []int
	for s := 0; s < len(args); s++ {
		restPairs, _ = doFlatten(restPairs, args[s])
	}
	return restPairs
}

func flattenPairsEx(i int, args ...interface{}) []int {
	var restPairs []int
	for s := 0; s < len(args); s++ {
		if s != i {
			restPairs, _ = doFlatten(restPairs, args[s])
		}
	}
	return restPairs
}

func pairNotIn(pick []int, flatten []int) bool {
	var checkAll = 1
	for i := 0; i < len(pick); i++ {
		if !(notIn(pick[i], flatten)) {
			checkAll *= 0
		}
	}
	if checkAll == 1 {
		return true
	}
	return false

}

//四層飛機有大翼
func (h *hand) plane4LongWing() {
	if len(h.plane.planeL4NoWing) < 1 || len(h.plane.planeL4NoWing[0]) != 12 || len(h.pairs) < 8 {
		return
	}
	for r := 0; r < len(h.plane.planeL4NoWing); r++ {
		for i := 0; i < len(h.pairs); i++ {
			for j := 0; j < len(h.pairs); j++ {
				for k := 0; k < len(h.pairs); k++ {
					for l := 0; l < len(h.pairs); l++ {
						var eachCase []int
						if i < j && j < k && k < l && pairNotIn(h.pairs[i], flattenPairs(h.pairs[j], h.pairs[k], h.pairs[l])) &&
							pairNotIn(h.pairs[j], flattenPairs(h.pairs[k], h.pairs[l])) && pairNotIn(h.pairs[k], h.pairs[l]) &&
							notIn(h.pairs[i][0]%10000, planeBody(h.plane.planeL4NoWing[r])) && notIn(h.pairs[j][0]%10000, planeBody(h.plane.planeL4NoWing[r])) &&
							notIn(h.pairs[k][0]%10000, planeBody(h.plane.planeL4NoWing[r])) && notIn(h.pairs[l][0]%10000, planeBody(h.plane.planeL4NoWing[r])) {
							eachCase = append(h.plane.planeL4NoWing[r], h.pairs[i][0], h.pairs[i][1], h.pairs[j][0], h.pairs[j][1], h.pairs[k][0], h.pairs[k][1], h.pairs[l][0], h.pairs[l][1])
							h.plane.planeL4LWing = append(h.plane.planeL4LWing, eachCase)
						}
					}
				}
			}
		}
	}
}

//五層飛機有小翼
func (h *hand) plane5ShortWing() {
	if len(h.plane.planeL5NoWing) < 1 || len(h.plane.planeL5NoWing[0]) != 15 {
		return
	}
	for r := 0; r < len(h.plane.planeL5NoWing); r++ {
		for i := 0; i < len(h.singleCards); i++ {
			for j := 0; j < len(h.singleCards); j++ {
				for k := 0; k < len(h.singleCards); k++ {
					for l := 0; l < len(h.singleCards); l++ {
						for m := 0; m < len(h.singleCards); m++ {
							var eachCase []int
							if i < j && j < k && k < l && l < m &&
								notIn(h.singleCards[i][0]%10000, planeBody(h.plane.planeL5NoWing[r])) &&
								notIn(h.singleCards[j][0]%10000, planeBody(h.plane.planeL5NoWing[r])) &&
								notIn(h.singleCards[k][0]%10000, planeBody(h.plane.planeL5NoWing[r])) &&
								notIn(h.singleCards[l][0]%10000, planeBody(h.plane.planeL5NoWing[r])) &&
								notIn(h.singleCards[m][0]%10000, planeBody(h.plane.planeL5NoWing[r])) {
								eachCase = append(h.plane.planeL5NoWing[r], h.Poker[i], h.Poker[j], h.Poker[k], h.Poker[l], h.Poker[m])
								h.plane.planeL5SWing = append(h.plane.planeL5SWing, eachCase)
							}
						}
					}
				}
			}
		}
	}
}

//五層飛機有大翼 ---> 用不到
func plane5LongWing(pair [][]int, plane5NoWing [][]int) [][]int {
	if len(plane5NoWing) < 1 || len(plane5NoWing[0]) != 15 || len(pair) < 10 {
		return nil
	}
	var planeWithWing [][]int
	for r := 0; r < len(plane5NoWing); r++ {
		for i := 0; i < len(pair); i++ {
			for j := 0; j < len(pair); j++ {
				for k := 0; k < len(pair); k++ {
					for l := 0; l < len(pair); l++ {
						for m := 0; m < len(pair); m++ {
							var eachCase []int
							if i < j && j < k && k < l && l < m &&
								pairNotIn(pair[i], flattenPairs(pair[j], pair[k], pair[l], pair[m])) &&
								pairNotIn(pair[j], flattenPairs(pair[k], pair[l], pair[m])) &&
								pairNotIn(pair[k], flattenPairs(pair[l], pair[m])) && pairNotIn(pair[l], flattenPairs(pair[m])) &&
								notIn(pair[i][0]%10000, planeBody(plane5NoWing[r])) && notIn(pair[j][0]%10000, planeBody(plane5NoWing[r])) &&
								notIn(pair[k][0]%10000, planeBody(plane5NoWing[r])) && notIn(pair[l][0]%10000, planeBody(plane5NoWing[r])) &&
								notIn(pair[m][0]%10000, planeBody(plane5NoWing[r])) {
								eachCase = append(plane5NoWing[r], pair[i][0], pair[i][1], pair[j][0], pair[j][1], pair[k][0], pair[k][1], pair[l][0], pair[l][1], pair[m][0], pair[m][1])
								planeWithWing = append(planeWithWing, eachCase)
							}
						}
					}
				}
			}
		}
	}
	return planeWithWing
}

func (h *hand) existenceCheck() {
	h.typeExistence = make(map[int]bool)
	if len(h.singleCards) != 0 {
		h.typeExistence[0] = true
	} else {
		h.typeExistence[0] = false
	}
	if len(h.pairs) != 0 {
		h.typeExistence[1] = true
	} else {
		h.typeExistence[1] = false
	}
	if len(h.threes) != 0 {
		h.typeExistence[2] = true
	} else {
		h.typeExistence[2] = false
	}
	if len(h.bombs) != 0 {
		h.typeExistence[3] = true
	} else {
		h.typeExistence[3] = false
	}
	if len(h.kBomb) != 0 {
		h.typeExistence[4] = true
	} else {
		h.typeExistence[4] = false
	}
	if len(h.threeOnes) != 0 {
		h.typeExistence[5] = true
	} else {
		h.typeExistence[5] = false
	}
	if len(h.threeTwos) != 0 {
		h.typeExistence[6] = true
	} else {
		h.typeExistence[6] = false
	}
	if len(h.straight.straight5) != 0 {
		h.typeExistence[7] = true
	} else {
		h.typeExistence[7] = false
	}
	if len(h.straight.straight6) != 0 {
		h.typeExistence[8] = true
	} else {
		h.typeExistence[8] = false
	}
	if len(h.straight.straight7) != 0 {
		h.typeExistence[9] = true
	} else {
		h.typeExistence[9] = false
	}
	if len(h.straight.straight8) != 0 {
		h.typeExistence[10] = true
	} else {
		h.typeExistence[10] = false
	}
	if len(h.straight.straight9) != 0 {
		h.typeExistence[11] = true
	} else {
		h.typeExistence[11] = false
	}
	if len(h.straight.straight10) != 0 {
		h.typeExistence[12] = true
	} else {
		h.typeExistence[12] = false
	}
	if len(h.straight.straight11) != 0 {
		h.typeExistence[13] = true
	} else {
		h.typeExistence[13] = false
	}
	if len(h.straight.straight12) != 0 {
		h.typeExistence[14] = true
	} else {
		h.typeExistence[14] = false
	}
	if len(h.plane.planeL2NoWing) != 0 {
		h.typeExistence[15] = true
	} else {
		h.typeExistence[15] = false
	}
	if len(h.plane.planeL3NoWing) != 0 {
		h.typeExistence[16] = true
	} else {
		h.typeExistence[16] = false
	}
	if len(h.plane.planeL4NoWing) != 0 {
		h.typeExistence[17] = true
	} else {
		h.typeExistence[17] = false
	}
	if len(h.plane.planeL5NoWing) != 0 {
		h.typeExistence[18] = true
	} else {
		h.typeExistence[18] = false
	}
	if len(h.plane.planeL6NoWing) != 0 {
		h.typeExistence[19] = true
	} else {
		h.typeExistence[19] = false
	}
	if len(h.plane.planeL2SWing) != 0 {
		h.typeExistence[20] = true
	} else {
		h.typeExistence[20] = false
	}
	if len(h.plane.planeL3SWing) != 0 {
		h.typeExistence[21] = true
	} else {
		h.typeExistence[21] = false
	}
	if len(h.plane.planeL4SWing) != 0 {
		h.typeExistence[22] = true
	} else {
		h.typeExistence[22] = false
	}
	if len(h.plane.planeL5SWing) != 0 {
		h.typeExistence[23] = true
	} else {
		h.typeExistence[23] = false
	}
	if len(h.plane.planeL2LWing) != 0 {
		h.typeExistence[24] = true
	} else {
		h.typeExistence[24] = false
	}
	if len(h.plane.planeL3LWing) != 0 {
		h.typeExistence[25] = true
	} else {
		h.typeExistence[25] = false
	}
	if len(h.plane.planeL4LWing) != 0 {
		h.typeExistence[26] = true
	} else {
		h.typeExistence[26] = false
	}
	if len(h.pairStraight.pairStraight3) != 0 {
		h.typeExistence[27] = true
	} else {
		h.typeExistence[27] = false
	}
	if len(h.pairStraight.pairStraight4) != 0 {
		h.typeExistence[28] = true
	} else {
		h.typeExistence[28] = false
	}
	if len(h.pairStraight.pairStraight5) != 0 {
		h.typeExistence[29] = true
	} else {
		h.typeExistence[29] = false
	}
	if len(h.pairStraight.pairStraight6) != 0 {
		h.typeExistence[30] = true
	} else {
		h.typeExistence[30] = false
	}
	if len(h.pairStraight.pairStraight7) != 0 {
		h.typeExistence[31] = true
	} else {
		h.typeExistence[31] = false
	}
	if len(h.pairStraight.pairStraight8) != 0 {
		h.typeExistence[32] = true
	} else {
		h.typeExistence[32] = false
	}
	if len(h.pairStraight.pairStraight9) != 0 {
		h.typeExistence[33] = true
	} else {
		h.typeExistence[33] = false
	}
	if len(h.pairStraight.pairStraight10) != 0 {
		h.typeExistence[34] = true
	} else {
		h.typeExistence[34] = false
	}
	if len(h.fourOnes) != 0 {
		h.typeExistence[35] = true
	} else {
		h.typeExistence[35] = false
	}
	if len(h.fourTwos) != 0 {
		h.typeExistence[36] = true
	} else {
		h.typeExistence[36] = false
	}
}

func CardTypeConstructor(h hand) hand {
	h.repetitions()
	h.repetitionsWOTwo()
	h.pass()
	//同類型同號碼組合裡面只選一種組合，例如單張的選擇有：黑桃2, 紅心2，就只抓一張黑桃2
	h.singlePick()
	h.pairPick()
	h.threePick()
	//h.single()
	//h.pair()
	//h.three()
	h.pairWOTwo()
	h.threeWOTwo()
	h.bomb()
	h.bombWOTwo()
	h.kingBomb()
	h.threeOne(h.Poker, h.threes)
	h.threeTwo(h.pairs, h.threes)
	//3~10層對子順
	if len(h.pairsWOTwo) > 2 {
		h.straightDuplicatesFinder(h.pairsWOTwo, &h.pairStraight.pairStraight3, 3)
		h.straightDuplicatesFinder(h.pairsWOTwo, &h.pairStraight.pairStraight4, 4)
		h.straightDuplicatesFinder(h.pairsWOTwo, &h.pairStraight.pairStraight5, 5)
		h.straightDuplicatesFinder(h.pairsWOTwo, &h.pairStraight.pairStraight6, 6)
		h.straightDuplicatesFinder(h.pairsWOTwo, &h.pairStraight.pairStraight7, 7)
		h.straightDuplicatesFinder(h.pairsWOTwo, &h.pairStraight.pairStraight8, 8)
		h.straightDuplicatesFinder(h.pairsWOTwo, &h.pairStraight.pairStraight9, 9)
		h.straightDuplicatesFinder(h.pairsWOTwo, &h.pairStraight.pairStraight10, 10)
	}
	//飛機
	if len(h.threesWOTwo) > 1 {
		//飛機無翼
		h.straightDuplicatesFinder(h.threesWOTwo, &h.plane.planeL2NoWing, 2)
		h.straightDuplicatesFinder(h.threesWOTwo, &h.plane.planeL3NoWing, 3)
		h.straightDuplicatesFinder(h.threesWOTwo, &h.plane.planeL4NoWing, 4)
		h.straightDuplicatesFinder(h.threesWOTwo, &h.plane.planeL5NoWing, 5)
		h.straightDuplicatesFinder(h.threesWOTwo, &h.plane.planeL6NoWing, 6)

		//飛機小翼
		//h.plane2ShortWing()
		//h.plane3ShortWing()
		//h.plane4ShortWing()
		//h.plane5ShortWing()

		//飛機小翼 只選同類型同號碼組合裡面一種組合
		h.plane2ShortWingPick()
		h.plane3ShortWingPick()
		h.plane4ShortWingPick()
		h.plane5ShortWingPick()

		//飛機大翼
		//h.plane2LongWing()
		//h.plane3LongWing()
		//h.plane4LongWing()

		//飛機大翼 只選同類型同號碼組合裡面一種組合
		h.plane2LongWingPick()
		h.plane3LongWingPick()
		h.plane4LongWingPick()
	}
	//順子
	if len(h.singleCards) > 4 {
		h.straightFinder(&h.straight.straight5, 5)
		h.straightFinder(&h.straight.straight6, 6)
		h.straightFinder(&h.straight.straight7, 7)
		h.straightFinder(&h.straight.straight8, 8)
		h.straightFinder(&h.straight.straight9, 9)
		h.straightFinder(&h.straight.straight10, 10)
		h.straightFinder(&h.straight.straight11, 11)
		h.straightFinder(&h.straight.straight12, 12)
	}
	//四帶二
	h.fourOne()
	h.fourTwo()
	h.totalCardOptions()
	h.existenceCheck()
	return h
}

func ConstructOpenning(h hand) hand {
	h.repetitions()
	h.repetitionsWOTwo()
	h.pass()
	//同類型同號碼組合裡面只選一種組合，例如單張的選擇有：黑桃2, 紅心2，就只抓一張黑桃2
	//h.singlePick()
	//h.pairPick()
	//h.threePick()
	h.single()
	h.pair()
	h.three()
	h.pairWOTwo()
	h.threeWOTwo()
	h.bomb()
	h.bombWOTwo()
	h.kingBomb()
	h.threeOne(h.Poker, h.threes)
	h.threeTwo(h.pairs, h.threes)
	//3~10層對子順
	if len(h.pairsWOTwo) > 2 {
		h.straightDuplicatesFinder(h.pairsWOTwo, &h.pairStraight.pairStraight3, 3)
		h.straightDuplicatesFinder(h.pairsWOTwo, &h.pairStraight.pairStraight4, 4)
		h.straightDuplicatesFinder(h.pairsWOTwo, &h.pairStraight.pairStraight5, 5)
		h.straightDuplicatesFinder(h.pairsWOTwo, &h.pairStraight.pairStraight6, 6)
		h.straightDuplicatesFinder(h.pairsWOTwo, &h.pairStraight.pairStraight7, 7)
		h.straightDuplicatesFinder(h.pairsWOTwo, &h.pairStraight.pairStraight8, 8)
		h.straightDuplicatesFinder(h.pairsWOTwo, &h.pairStraight.pairStraight9, 9)
		h.straightDuplicatesFinder(h.pairsWOTwo, &h.pairStraight.pairStraight10, 10)
	}
	//飛機
	if len(h.threesWOTwo) > 1 {
		//飛機無翼
		h.straightDuplicatesFinder(h.threesWOTwo, &h.plane.planeL2NoWing, 2)
		h.straightDuplicatesFinder(h.threesWOTwo, &h.plane.planeL3NoWing, 3)
		h.straightDuplicatesFinder(h.threesWOTwo, &h.plane.planeL4NoWing, 4)
		h.straightDuplicatesFinder(h.threesWOTwo, &h.plane.planeL5NoWing, 5)
		h.straightDuplicatesFinder(h.threesWOTwo, &h.plane.planeL6NoWing, 6)

		//飛機小翼
		h.plane2ShortWing()
		h.plane3ShortWing()
		h.plane4ShortWing()
		h.plane5ShortWing()

		//飛機小翼 只選同類型同號碼組合裡面一種組合
		//h.plane2ShortWingPick()
		//h.plane3ShortWingPick()
		//h.plane4ShortWingPick()
		//h.plane5ShortWingPick()

		//飛機大翼
		h.plane2LongWing()
		h.plane3LongWing()
		h.plane4LongWing()

		//飛機大翼 只選同類型同號碼組合裡面一種組合
		//h.plane2LongWingPick()
		//h.plane3LongWingPick()
		//h.plane4LongWingPick()
	}
	//順子
	if len(h.singleCards) > 4 {
		h.straightFinder(&h.straight.straight5, 5)
		h.straightFinder(&h.straight.straight6, 6)
		h.straightFinder(&h.straight.straight7, 7)
		h.straightFinder(&h.straight.straight8, 8)
		h.straightFinder(&h.straight.straight9, 9)
		h.straightFinder(&h.straight.straight10, 10)
		h.straightFinder(&h.straight.straight11, 11)
		h.straightFinder(&h.straight.straight12, 12)
	}
	//四帶二
	h.fourOne()
	h.fourTwo()
	h.totalCardOptions()
	h.existenceCheck()
	return h
}
