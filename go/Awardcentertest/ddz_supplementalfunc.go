package awardcentertest

import (
	"bufio"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"math"
	"os"
	"sort"
	"time"
)

////MongoDB 結構
//type MongoDB struct {
//	Client *mongo.Client
//	ctx    context.Context
//}
//
////Object mongoDB實體
//var Object *MongoDB

func readFileLineByLine() {
	fptr := flag.String("fpath", "test.txt", "file path to read from")
	flag.Parse()

	f, err := os.Open(*fptr)
	if err != nil {
		log.Fatal(err)
	}
	defer func() {
		if err = f.Close(); err != nil {
			log.Fatal(err)
		}
	}()
	s := bufio.NewScanner(f)
	for s.Scan() {
		fmt.Println(s.Text())
	}
	err = s.Err()
	if err != nil {
		log.Fatal(err)
	}
}

func readWholeFile() {
	_, err := ioutil.ReadFile("Tree Expansion.txt")
	if err != nil {
		fmt.Println("File reading error", err)
		return
	}
}

func createFile() {
	t, err := os.Create("Tree Expansion.txt")
	if err != nil {
		fmt.Println(err)
		t.Close()
		return
	}
}

func saveData(thisNodeInfo Nodes) {
	f, err := os.OpenFile("Tree Expansion.txt", os.O_APPEND|os.O_WRONLY, 0644)
	if err != nil {
		fmt.Println(err)
		return
	}
	newLine := thisNodeInfo
	_, err = fmt.Fprintln(f, newLine)
	if err != nil {
		fmt.Println(err)
		f.Close()
		return
	}
	err = f.Close()
	if err != nil {
		fmt.Println(err)
		return
	}
}

func straightMax(s []int) int {
	var reference = 0
	for i := 0; i < len(s); i++ {
		if s[i]%10000 >= reference {
			reference = s[i] % 10000
		}
	}
	return reference
}

//把各別的特定l層給印出來/存起來
//func thisLayerNodesBeta(thisStepNodes []Nodes, l int, distinct map[int]bool) {
//	fmt.Println("第", l, "層的所有節點：")
//	for i := range thisStepNodes {
//		if thisStepNodes[i].Order == l && thisStepNodes[i].Isopoint%1000 != -999 {
//			//saveData(thisStepNodes[i])
//			if thisStepNodes[i].Index%1000-thisStepNodes[i].Isopoint%1000 > 6 {
//				fmt.Println(thisStepNodes[i].Index%1000, thisStepNodes[i].Isopoint%1000, thisStepNodes[i].Index%1000-thisStepNodes[i].Isopoint%1000)
//			}
//			if !distinct[thisStepNodes[i].Index%1000-thisStepNodes[i].Isopoint%1000] {
//				distinct[thisStepNodes[i].Index%1000-thisStepNodes[i].Isopoint%1000] = true
//			}
//			//unique(distinct, thisStepNodes[i].Index%1000-thisStepNodes[i].Isopoint%1000)
//			//difference = append(difference, thisStepNodes[i].Index%1000-thisStepNodes[i].Isopoint%1000)
//		}
//	}
//}

//func WriteInvetoryNodeInMongo(thisKey InvNodeKeyBeta, thisIsoPoint Isopoints, location string) {
//	newIsoNodeKey := InventoryNode{
//		Key:     thisKey,
//		IsoNode: thisIsoPoint,
//	}
//	err := save2mongo.Object.InsertDocument("spookyforce", location, newIsoNodeKey)
//	if err != nil {
//		fmt.Errorf("MongoDB寫入失敗 %d", err)
//	}
//}
//
//func WriteTreeNodeInMongo(theseNodes []Nodes, location string) {
//	newLayer := Layer{
//		Nodes: theseNodes,
//	}
//	err := save2mongo.Object.InsertDocument("spookyforce", location, newLayer)
//	if err != nil {
//		fmt.Errorf("MongoDB寫入失敗 %d", err)
//	}
//}

func thisLayerNodes(thisStepNodes []Nodes, l int, tree []Layer) {
	fmt.Println("第", l, "層的所有節點：")
	//WriteTreeNodeInMongo(thisStepNodes, "mainTree")
	for i := range thisStepNodes {
		if thisStepNodes[i].Order == l {
			//saveData(thisStepNodes[i])
			fmt.Println(thisStepNodes[i])
			//if len(thisStepNodes[i].Outcome) != 0 {
			//	fmt.Println(thisStepNodes[i])
			//}
			//WriteTreeNodeInMongo(thisStepNodes[i], "mainTree")
		}
	}
}

func unique(distinct map[int]bool, e int) {
	if _, v := distinct[e]; v {
		fmt.Println(v)
		//fmt.Println(i, v, e)
		//if _, e := keys[entry]; e {
		//	keys[entry] = true
		//	list = append(list, entry)
		//}
	}
}

func HasIsoPoint(iso int) string {
	if iso == -999 {
		return "無"
	}
	output := "第" + string(iso%1000) + "層" + "; 位置" + string(iso/1000)
	return output
}

//方便讀的把各別的特定l層給印出來
func thisLayerNodesFriendlyList(thisStepNodes []Nodes, l int) {
	fmt.Println("第", l, "層的所有節點：")
	for i := range thisStepNodes {
		if thisStepNodes[i].Order == l {
			if (l-1)%3 == 0 {
				if len(thisStepNodes[i].Outcome) != 0 {
					if thisStepNodes[i].Outcome[0] == 0 && thisStepNodes[i].Outcome[1] == 1 {
						if thisStepNodes[i].PassCount == 0 {
							fmt.Println("地主出牌：", getCardsName(thisStepNodes[i].CardSetInfo.CardSet), "本節點:", thisStepNodes[i].Index, "上層節點:", thisStepNodes[i].ParentInfo, "農夫方獲勝")
						} else {
							fmt.Println("地主PASS：", getCardsName(thisStepNodes[i].CardSetInfo.CardSet), "本節點:", thisStepNodes[i].Index, "上層節點:", thisStepNodes[i].ParentInfo, "農夫方獲勝")
						}

					} else if thisStepNodes[i].Outcome[0] == 1 && thisStepNodes[i].Outcome[1] == 0 {
						if thisStepNodes[i].PassCount == 0 {
							fmt.Println("地主出牌：", getCardsName(thisStepNodes[i].CardSetInfo.CardSet), "本節點:", thisStepNodes[i].Index, "上層節點:", thisStepNodes[i].ParentInfo, "地主方獲勝")
						} else {
							fmt.Println("地主PASS：", getCardsName(thisStepNodes[i].CardSetInfo.CardSet), "本節點:", thisStepNodes[i].Index, "上層節點:", thisStepNodes[i].ParentInfo, "地主方獲勝")
						}
					}
				} else {
					fmt.Println(thisStepNodes[i], "本節點:", thisStepNodes[i].Index, "上層節點:", thisStepNodes[i].ParentInfo)
				}
			} else if (l-1)%3 == 1 {
				if len(thisStepNodes[i].Outcome) != 0 {
					if thisStepNodes[i].Outcome[0] == 0 && thisStepNodes[i].Outcome[1] == 1 {
						if thisStepNodes[i].PassCount == 0 {
							fmt.Println("農夫一出牌：", getCardsName(thisStepNodes[i].CardSetInfo.CardSet), "本節點:", thisStepNodes[i].Index, "上層節點:", thisStepNodes[i].ParentInfo, "農夫方獲勝")
						} else {
							fmt.Println("農夫一PASS：", getCardsName(thisStepNodes[i].CardSetInfo.CardSet), "本節點:", thisStepNodes[i].Index, "上層節點:", thisStepNodes[i].ParentInfo, "農夫方獲勝")
						}
					} else if thisStepNodes[i].Outcome[0] == 1 && thisStepNodes[i].Outcome[1] == 0 {
						if thisStepNodes[i].PassCount == 0 {
							fmt.Println("農夫一出牌：", getCardsName(thisStepNodes[i].CardSetInfo.CardSet), "本節點:", thisStepNodes[i].Index, "上層節點:", thisStepNodes[i].ParentInfo, "地主方獲勝")
						} else {
							fmt.Println("農夫一PASS：", getCardsName(thisStepNodes[i].CardSetInfo.CardSet), "本節點:", thisStepNodes[i].Index, "上層節點:", thisStepNodes[i].ParentInfo, "地主方獲勝")
						}
					}
				} else {
					fmt.Println(thisStepNodes[i], "本節點:", thisStepNodes[i].Index, "上層節點:", thisStepNodes[i].ParentInfo)
				}
			} else if (l-1)%3 == 2 {
				if len(thisStepNodes[i].Outcome) != 0 {
					if thisStepNodes[i].Outcome[0] == 0 && thisStepNodes[i].Outcome[1] == 1 {
						if thisStepNodes[i].PassCount == 0 {
							fmt.Println("農夫二出牌：", getCardsName(thisStepNodes[i].CardSetInfo.CardSet), "本節點:", thisStepNodes[i].Index, "上層節點:", thisStepNodes[i].ParentInfo, "農夫方獲勝")
						} else {
							fmt.Println("農夫二PASS：", getCardsName(thisStepNodes[i].CardSetInfo.CardSet), "本節點:", thisStepNodes[i].Index, "上層節點:", thisStepNodes[i].ParentInfo, "農夫方獲勝")
						}
					} else if thisStepNodes[i].Outcome[0] == 1 && thisStepNodes[i].Outcome[1] == 0 {
						if thisStepNodes[i].PassCount == 0 {
							fmt.Println("農夫二出牌：", getCardsName(thisStepNodes[i].CardSetInfo.CardSet), "本節點:", thisStepNodes[i].Index, "上層節點:", thisStepNodes[i].ParentInfo, "地主方獲勝")
						} else {
							fmt.Println("農夫二PASS：", getCardsName(thisStepNodes[i].CardSetInfo.CardSet), "本節點:", thisStepNodes[i].Index, "上層節點:", thisStepNodes[i].ParentInfo, "地主方獲勝")
						}
					}
				} else {
					fmt.Println(thisStepNodes[i], "本節點:", thisStepNodes[i].Index, "上層節點:", thisStepNodes[i].ParentInfo)
				}
			} else {
				fmt.Println("地主開局：", getCardsName(thisStepNodes[i].RestCards[0]), "農夫一開局：", getCardsName(thisStepNodes[i].RestCards[1]), "農夫二開局：", getCardsName(thisStepNodes[i].RestCards[2]))
			}
		}
	}
}

func getCardsName(thisPoker []int) []string {
	var attach []string
	sort.Slice(thisPoker, func(i, j int) bool { return thisPoker[i]%SuitShift < thisPoker[j]%SuitShift })
	for i := 0; i < len(thisPoker); i++ {
		if thisPoker[i]/10000 == 1 {
			if thisPoker[i]%10000 == 15 {
				output := fmt.Sprintf("%s%s", "黑桃", " 2")
				if len(attach) > 0 {
					attach = append(attach, "|")
				}
				attach = append(attach, output)
			} else if thisPoker[i]%10000 == 14 {
				output := fmt.Sprintf("%s%s", "黑桃", " A")
				if len(attach) > 0 {
					attach = append(attach, "|")
				}
				attach = append(attach, output)
			} else if thisPoker[i]%10000 == 13 {
				output := fmt.Sprintf("%s%s", "黑桃", " K")
				if len(attach) > 0 {
					attach = append(attach, "|")
				}
				attach = append(attach, output)
			} else if thisPoker[i]%10000 == 12 {
				output := fmt.Sprintf("%s%s", "黑桃", " Q")
				if len(attach) > 0 {
					attach = append(attach, "|")
				}
				attach = append(attach, output)
			} else if thisPoker[i]%10000 == 11 {
				output := fmt.Sprintf("%s%s", "黑桃", " J")
				if len(attach) > 0 {
					attach = append(attach, "|")
				}
				attach = append(attach, output)
			} else {
				output := fmt.Sprintf("%s%2d", "黑桃", thisPoker[i]%10000)
				if len(attach) > 0 {
					attach = append(attach, "|")
				}
				attach = append(attach, output)
			}

		} else if thisPoker[i]/10000 == 2 {
			if thisPoker[i]%10000 == 15 {
				output := fmt.Sprintf("%s%s", "紅心", " 2")
				if len(attach) > 0 {
					attach = append(attach, "|")
				}
				attach = append(attach, output)
			} else if thisPoker[i]%10000 == 14 {
				output := fmt.Sprintf("%s%s", "紅心", " A")
				if len(attach) > 0 {
					attach = append(attach, "|")
				}
				attach = append(attach, output)
			} else if thisPoker[i]%10000 == 13 {
				output := fmt.Sprintf("%s%s", "紅心", " K")
				if len(attach) > 0 {
					attach = append(attach, "|")
				}
				attach = append(attach, output)
			} else if thisPoker[i]%10000 == 12 {
				output := fmt.Sprintf("%s%s", "紅心", " Q")
				if len(attach) > 0 {
					attach = append(attach, "|")
				}
				attach = append(attach, output)
			} else if thisPoker[i]%10000 == 11 {
				output := fmt.Sprintf("%s%s", "紅心", " J")
				if len(attach) > 0 {
					attach = append(attach, "|")
				}
				attach = append(attach, output)
			} else {
				output := fmt.Sprintf("%s%2d", "紅心", thisPoker[i]%10000)
				if len(attach) > 0 {
					attach = append(attach, "|")
				}
				attach = append(attach, output)
			}
		} else if thisPoker[i]/10000 == 3 {
			if thisPoker[i]%10000 == 15 {
				output := fmt.Sprintf("%s%s", "梅花", " 2")
				if len(attach) > 0 {
					attach = append(attach, "|")
				}
				attach = append(attach, output)
			} else if thisPoker[i]%10000 == 14 {
				output := fmt.Sprintf("%s%s", "梅花", " A")
				if len(attach) > 0 {
					attach = append(attach, "|")
				}
				attach = append(attach, output)
			} else if thisPoker[i]%10000 == 13 {
				output := fmt.Sprintf("%s%s", "梅花", " K")
				if len(attach) > 0 {
					attach = append(attach, "|")
				}
				attach = append(attach, output)
			} else if thisPoker[i]%10000 == 12 {
				output := fmt.Sprintf("%s%s", "梅花", " Q")
				if len(attach) > 0 {
					attach = append(attach, "|")
				}
				attach = append(attach, output)
			} else if thisPoker[i]%10000 == 11 {
				output := fmt.Sprintf("%s%s", "梅花", " J")
				if len(attach) > 0 {
					attach = append(attach, "|")
				}
				attach = append(attach, output)
			} else {
				output := fmt.Sprintf("%s%2d", "梅花", thisPoker[i]%10000)
				if len(attach) > 0 {
					attach = append(attach, "|")
				}
				attach = append(attach, output)
			}
		} else if thisPoker[i]/10000 == 4 {
			if thisPoker[i]%10000 == 15 {
				output := fmt.Sprintf("%s%s", "方塊", " 2")
				if len(attach) > 0 {
					attach = append(attach, "|")
				}
				attach = append(attach, output)
			} else if thisPoker[i]%10000 == 14 {
				output := fmt.Sprintf("%s%s", "方塊", " A")
				if len(attach) > 0 {
					attach = append(attach, "|")
				}
				attach = append(attach, output)
			} else if thisPoker[i]%10000 == 13 {
				output := fmt.Sprintf("%s%s", "方塊", " K")
				if len(attach) > 0 {
					attach = append(attach, "|")
				}
				attach = append(attach, output)
			} else if thisPoker[i]%10000 == 12 {
				output := fmt.Sprintf("%s%s", "方塊", " Q")
				if len(attach) > 0 {
					attach = append(attach, "|")
				}
				attach = append(attach, output)
			} else if thisPoker[i]%10000 == 11 {
				output := fmt.Sprintf("%s%s", "方塊", " J")
				if len(attach) > 0 {
					attach = append(attach, "|")
				}
				attach = append(attach, output)
			} else {
				output := fmt.Sprintf("%s%2d", "方塊", thisPoker[i]%10000)
				if len(attach) > 0 {
					attach = append(attach, "|")
				}
				attach = append(attach, output)
			}
		} else if thisPoker[i] == 50031 {
			if len(attach) > 0 {
				attach = append(attach, "|")
			}
			attach = append(attach, " 大 鬼")
		} else if thisPoker[i] == 50030 {
			if len(attach) > 0 {
				attach = append(attach, "|")
			}
			attach = append(attach, " 小 鬼")
		}
	}
	return attach
}

func OutcomeNotIn(c []int, s [][]int) bool {
	var checkAll = 1
	if len(c) == 0 {
		return false
	}
	for i := 0; i < len(s); i++ {
		if c[0] == s[i][0] && c[1] == s[i][1] {
			checkAll *= 0
		}
	}
	if checkAll == 1 {
		return true
	}
	return false
}
func duplicatesRemover(Outcome [][]int) [][]int {
	var distinct [][]int
	for e := range Outcome {
		if OutcomeNotIn(Outcome[e], distinct) {
			distinct = append(distinct, Outcome[e])
		}
	}
	return distinct
}

func pairwiseEqual(a []int, b []int) bool {
	if len(a) != len(b) {
		return false
	}
	var check = 1
	for i := 0; i < len(a); i++ {
		if a[i] != b[i] {
			check *= 0
		}
	}
	if check == 0 {
		return false
	}
	return true
}

func maxCards(theseNodes []int) int {
	var maxCard int
	for i, e := range theseNodes {
		if i == 0 || e%SuitShift < maxCard {
			maxCard = e
		}
	}
	return maxCard
}

func TreeInfo(tree []Layer) {
	var NodesCount int
	for i := range tree {
		NodesCount += len(tree[i].Nodes)
	}
	if NodesCount >= int(math.Pow10(6)) && NodesCount < int(math.Pow10(9)) {
		fmt.Println("Total Nodes in the tree:", float64(NodesCount)/math.Pow10(6),
			"M, or ", NodesCount, "Layers:", len(tree))
	} else if NodesCount >= int(math.Pow10(9)) && NodesCount < int(math.Pow10(12)) {
		fmt.Println("Total Nodes in the tree:", float64(NodesCount)/math.Pow10(9),
			"B, or ", NodesCount, "Layers:", len(tree))
	} else {
		fmt.Println("Total Nodes in the tree:", NodesCount, "Layers:", len(tree))
	}
}

func totalLayers(tree []Layer, initialLayer int, destLayer int) {
	defer timeTrack(time.Now(), "主樹節點填入Mongo時間")
	//創建檔案
	//createFile()
	for i := range tree {
		if len(tree[i].Nodes) != 0 && i <= destLayer && i >= initialLayer {
			//thisLayerNodes(tree[i].Nodes, i, tree)
			thisLayerNodesFriendlyList(tree[i].Nodes, i)
			//fmt.Println("▃▃▃▃▃▃▃▃▃▃ Total Node(s) in Layer", i, "th :", len(tree[i].Nodes), "▃▃▃▃▃▃▃▃▃▃▃")
		}
	}
}

func ShrinkingExpansion(inGamePlayer []hand, freewillplayer int) {
	//getGameSet := inGamePlayer
	shorten := []int{len(inGamePlayer[0].Poker), len(inGamePlayer[1].Poker), len(inGamePlayer[2].Poker)}
	for i := 0; i < len(inGamePlayer); i++ {
		inGamePlayer[i].Poker = inGamePlayer[i].Poker[:shorten[i]]
		inGamePlayer[i] = CardTypeConstructor(inGamePlayer[i])
		fmt.Println(getCardsName(inGamePlayer[i].Poker))
	}
	for k := 0; k < 3; k++ {
		printPokerInFormat(inGamePlayer[k].Poker, k)
	}
	ConstructTreeShinked(inGamePlayer, freewillplayer)
}

func typeComparision(handCard []int, parentCard []int, typeSerial int) bool {
	var value bool
	if typeSerial >= 1 && typeSerial <= 3 {
		typeSerial = 1
	} else if typeSerial >= 7 && typeSerial <= 14 {
		typeSerial = 7
	} else if typeSerial >= 15 && typeSerial <= 26 {
		typeSerial = 15
	} else if typeSerial >= 27 && typeSerial <= 34 {
		typeSerial = 27
	} else if typeSerial >= 35 && typeSerial <= 36 {
		typeSerial = 35
	}
	switch typeSerial {
	case 0: //單張
		if (handCard[0]/10000 != 5 && parentCard[0]/10000 != 5 && handCard[0]%10000 > parentCard[0]%10000) || handCard[0] == 50031 || (handCard[0]/10000 == 5 && parentCard[0]/10000 != 5) {
			value = true
		}
	case 1: //對子
		if handCard[0]%10000 > parentCard[0]%10000 {
			value = true
		}
	case 4: //王炸
		value = true
	case 5: //三帶一
		if planeBody(handCard)[0] > planeBody(parentCard)[0] {
			value = true
		}
	case 6: //三帶二
		if planeBody(handCard)[0] > planeBody(parentCard)[0] {
			value = true
		}
	case 7:
		if straightMax(handCard) > straightMax(parentCard) {
			value = true
		}
	case 15:
		if straightMax(planeBody(handCard)) > straightMax(planeBody(parentCard)) {
			value = true
		}
	case 27:
		if straightMax(handCard) > straightMax(parentCard) {
			value = true
		}
	case 35:
		if straightMax(fourBody(handCard)) > straightMax(fourBody(parentCard)) {
			value = true
		}
	default:
		value = false
	}
	return value
}
