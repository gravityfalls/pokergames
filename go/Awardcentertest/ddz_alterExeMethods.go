package awardcentertest

import (
	"fmt"
	"math/rand"
	"sort"
	"time"
)

//TestDifferentMethods
func ConstructTreeShinked(inGamePLayer []hand, freewillplayer int) {
	defer timeTrack(time.Now(), "ConstructTreeShrinked")
	//建造tree
	var tree = make([]Layer, 1)
	//建造初始節點
	tree[0].Nodes = append(tree[0].Nodes, Nodes{0, 0, 2, -999, []int{}, CardType{[]int{}, -1}, map[int][]int{}, []int{}})
	for i := 0; i < 3; i++ {
		tree[0].Nodes[0].RestCards[i] = inGamePLayer[i].Poker
	}
	//var inventory = make(map[InvNodeKeyBeta]Isopoints)
	//recurrently generating child Nodes for each current node
	var l = 1
	for {
		var newLayer Layer
		tree = append(tree, newLayer)
		if len(tree[l-1].Nodes) != 0 {
			tree[l-1].iteratedGeneratingProcessCenter(tree, tree[l-1].Nodes, l, (l-1)%3, freewillplayer) //開始分佈節點
			fmt.Println(l-1, len(tree[l-1].Nodes))
		} else {
			break
		}
		l++
	}
	//計算總節點數
	TreeInfo(tree)
	//反向從底層往上層填節點結局
	backwardsFilling(tree)
	//列出所有節點／儲存所有節點
	totalLayers(tree, 1, 12)
}

func (t *Layer) iteratedGeneratingProcessCenter(tree []Layer, lastStepNodes []Nodes, l int, role int, freewillplayer int) {
	//飛機兩層8張牌，開始分佈節點
	tree[l-1].iteratedGeneratingProcessPlane2LayerWing(tree, lastStepNodes, l, role, freewillplayer) //飛機兩層8張牌，開始分佈節點
	//順子八張牌，開始分佈節點
	//tree[l-1].iteratedGeneratingProcessStraight8Cards(tree, lastStepNodes, l, role, freewillplayer) //順子八張牌，開始分佈節點
	//順子六張牌
	//tree[l-1].iteratedGeneratingProcessStraight6s(tree, lastStepNodes, l, role, freewillplayer) //順子六張牌
}

func (t *Layer) iteratedGeneratingProcessStraight6s(tree []Layer, lastStepNodes []Nodes, l int, role int, freewillplayer int) {
	//timeTrack(time.Now(), "this Layer took")
	var nodeCount = 0
	for thisNode := range lastStepNodes {
		var beforeAddingNodes = len(tree[l].Nodes)
		if len(lastStepNodes[thisNode].Outcome) == 0 {
			var parentNodeIndex = lastStepNodes[thisNode].Index
			var thisHand hand
			thisHand.Poker = lastStepNodes[thisNode].RestCards[role]
			handCards := tree[l-1].newNodesGenWithCon(CardTypeConstructor(thisHand),
				lastStepNodes[thisNode].CardSetInfo.CardSet, lastStepNodes[thisNode].RestCards, l, parentNodeIndex,
				lastStepNodes[thisNode].PassCount, lastStepNodes[thisNode].CardSetInfo.CardTypeSerial, 1) //組成這一手玩家的節點
			var newNodes []Nodes
			for s := 0; s < len(handCards); s++ {
				if lastStepNodes[thisNode].PassCount == 2 {
					if lastStepNodes[thisNode].CardSetInfo.CardTypeSerial == -1 { //開局：前一手是PASS且累積兩次PASS
						//handCards[s].Index = nodeCount*1000 + l
						//nodeCount++
						newNodes = append(newNodes, handCards[s])
						//lastStepNodes[thisNode].ChildInfo = append(lastStepNodes[thisNode].ChildInfo, handCards[s].Index)
						//tree[l].Nodes = append(tree[l].Nodes, handCards[s])
					} else if handCards[s].CardSetInfo.CardTypeSerial != -1 {
						//handCards[s].Index = nodeCount*1000 + l
						//nodeCount++
						newNodes = append(newNodes, handCards[s])
						//lastStepNodes[thisNode].ChildInfo = append(lastStepNodes[thisNode].ChildInfo, handCards[s].Index)
						//tree[l].Nodes = append(tree[l].Nodes, handCards[s])
					}
				} else if handCards[s].CardSetInfo.CardTypeSerial == lastStepNodes[thisNode].CardSetInfo.CardTypeSerial && handCards[s].CardSetInfo.CardTypeSerial != -1 { //同性質的牌組非PASS
					if lastStepNodes[thisNode].PassCount < 2 && typeComparision(handCards[s].CardSetInfo.CardSet, lastStepNodes[thisNode].CardSetInfo.CardSet, handCards[s].CardSetInfo.CardTypeSerial) {
						//前一節點PASS累積數少於2-->比較排型大小
						//handCards[s].Index = nodeCount*1000 + l
						//nodeCount++
						newNodes = append(newNodes, handCards[s])
						//lastStepNodes[thisNode].ChildInfo = append(lastStepNodes[thisNode].ChildInfo, handCards[s].Index)
						//tree[l].Nodes = append(tree[l].Nodes, handCards[s])
					} else if lastStepNodes[thisNode].PassCount == 2 {
						//前一節點PASS累積數＝2-->可直接出下一個
						//handCards[s].Index = nodeCount*1000 + l
						//nodeCount++
						newNodes = append(newNodes, handCards[s])
						//lastStepNodes[thisNode].ChildInfo = append(lastStepNodes[thisNode].ChildInfo, handCards[s].Index)
						//tree[l].Nodes = append(tree[l].Nodes, handCards[s])
					}
				} else if handCards[s].CardSetInfo.CardTypeSerial == 3 && (lastStepNodes[thisNode].CardSetInfo.CardTypeSerial != 3 && lastStepNodes[thisNode].CardSetInfo.CardTypeSerial != 4) {
					//炸彈
					//handCards[s].Index = nodeCount*1000 + l
					//nodeCount++
					newNodes = append(newNodes, handCards[s])
					//lastStepNodes[thisNode].ChildInfo = append(lastStepNodes[thisNode].ChildInfo, handCards[s].Index)
					//tree[l].Nodes = append(tree[l].Nodes, handCards[s])
				} else if handCards[s].CardSetInfo.CardTypeSerial == 4 {
					//王炸
					//handCards[s].Index = nodeCount*1000 + l
					//nodeCount++
					newNodes = append(newNodes, handCards[s])
					//lastStepNodes[thisNode].ChildInfo = append(lastStepNodes[thisNode].ChildInfo, handCards[s].Index)
					//tree[l].Nodes = append(tree[l].Nodes, handCards[s])
				} else if handCards[s].CardSetInfo.CardTypeSerial == -1 && lastStepNodes[thisNode].PassCount < 2 {
					//這一手PASS，總PASS數<=2 (還沒繞一輪) 繼承前一手的牌型
					handCards[s].CardSetInfo.CardSet = lastStepNodes[thisNode].CardSetInfo.CardSet
					handCards[s].CardSetInfo.CardTypeSerial = lastStepNodes[thisNode].CardSetInfo.CardTypeSerial
					//handCards[s].Index = nodeCount*1000 + l
					handCards[s].PassCount = lastStepNodes[thisNode].PassCount + 1
					//nodeCount++
					newNodes = append(newNodes, handCards[s])
					//lastStepNodes[thisNode].ChildInfo = append(lastStepNodes[thisNode].ChildInfo, handCards[s].Index)
					//tree[l].Nodes = append(tree[l].Nodes, handCards[s])
				}
			}
			//newNodes為已經能夠黏上去的節點
			//檢視有哪些牌型
			var typeMap = make(map[int][]Nodes)
			var typeCollector = make([]int, 0)
			for i := range newNodes {
				if newNodes[i].PassCount-lastStepNodes[thisNode].PassCount == 1 {
					typeMap[-1] = append(typeMap[-1], newNodes[i])

				} else {
					typeMap[newNodes[i].CardSetInfo.CardTypeSerial] = append(typeMap[newNodes[i].CardSetInfo.CardTypeSerial], newNodes[i])
				}
				if notIn(newNodes[i].CardSetInfo.CardTypeSerial, typeCollector) {
					typeCollector = append(typeCollector, newNodes[i].CardSetInfo.CardTypeSerial)
				}
			}

			var netAddings int
			var usedNodes []int
			if l <= 50 {
				if !playerwithFreeWill(l, freewillplayer) { //假設農夫 freewillplayer 玩家有自由選擇的意志
					//設局的牌組進來
					//順子六張
					if len(typeMap[8]) > 0 && netAddings < 1 {
						var minStraight6 = []int{100, -1}
						for j := 0; j < len(typeMap[8]); j++ {
							if typeMap[8][j].CardSetInfo.CardSet[0]%SuitShift < minStraight6[0] {
								minStraight6[0] = typeMap[8][j].CardSetInfo.CardSet[0] % SuitShift
								minStraight6[1] = j
							}
						}
						location := minStraight6[1]
						typeMap[8][location].Index = nodeCount*1000 + l
						nodeCount++
						lastStepNodes[thisNode].ChildInfo = append(lastStepNodes[thisNode].ChildInfo, typeMap[8][location].Index)
						tree[l].Nodes = append(tree[l].Nodes, typeMap[8][location])
						netAddings = len(tree[l].Nodes) - beforeAddingNodes
					}
					//特定牌型沒牌，就把PASS加進來
					if len(typeMap[-1]) != 0 && netAddings < 1 {
						typeMap[-1][0].Index = nodeCount*1000 + l
						nodeCount++
						lastStepNodes[thisNode].ChildInfo = append(lastStepNodes[thisNode].ChildInfo, typeMap[-1][0].Index)
						tree[l].Nodes = append(tree[l].Nodes, typeMap[-1][0])
						netAddings = len(tree[l].Nodes) - beforeAddingNodes
					}
					//三層連順
					if len(typeMap[27]) > 0 && netAddings < 1 {
						//機翼號碼不為其他機腹的號碼
						for j := range typeMap[27] {
							typeMap[27][j].Index = nodeCount*1000 + l
							nodeCount++
							lastStepNodes[thisNode].ChildInfo = append(lastStepNodes[thisNode].ChildInfo, typeMap[27][j].Index)
							tree[l].Nodes = append(tree[l].Nodes, typeMap[27][j])
							netAddings = len(tree[l].Nodes) - beforeAddingNodes
						}
					}
					//三帶一
					if len(typeMap[5]) > 0 && netAddings < 1 {
						//機翼號碼不為其他機腹的號碼
						for j := range typeMap[5] {
							typeMap[5][j].Index = nodeCount*1000 + l
							nodeCount++
							lastStepNodes[thisNode].ChildInfo = append(lastStepNodes[thisNode].ChildInfo, typeMap[5][j].Index)
							tree[l].Nodes = append(tree[l].Nodes, typeMap[5][j])
							netAddings = len(tree[l].Nodes) - beforeAddingNodes
						}
					}

					//上一家出什麼
					if len(typeMap[lastStepNodes[thisNode].CardSetInfo.CardTypeSerial]) > 0 && netAddings < 1 {
						//機翼號碼不為其他機腹的號碼
						for j := range typeMap[lastStepNodes[thisNode].CardSetInfo.CardTypeSerial] {
							typeMap[lastStepNodes[thisNode].CardSetInfo.CardTypeSerial][j].Index = nodeCount*1000 + l
							nodeCount++
							lastStepNodes[thisNode].ChildInfo = append(lastStepNodes[thisNode].ChildInfo, typeMap[lastStepNodes[thisNode].CardSetInfo.CardTypeSerial][j].Index)
							tree[l].Nodes = append(tree[l].Nodes, typeMap[lastStepNodes[thisNode].CardSetInfo.CardTypeSerial][j])
							netAddings = len(tree[l].Nodes) - beforeAddingNodes
						}
					}
					//if netAddings < 1 {
					//	for i := range typeCollector {
					//		if notIn(typeCollector[i], []int{8, 5, 27, -1, lastStepNodes[thisNode].CardSetInfo.CardTypeSerial}) {
					//			for j := range typeMap[typeCollector[i]] {
					//				typeMap[typeCollector[i]][j].Index = nodeCount*1000 + l
					//				nodeCount++
					//				lastStepNodes[thisNode].ChildInfo = append(lastStepNodes[thisNode].ChildInfo, typeMap[typeCollector[i]][j].Index)
					//				tree[l].Nodes = append(tree[l].Nodes, typeMap[typeCollector[i]][j])
					//				netAddings = len(tree[l].Nodes) - beforeAddingNodes
					//			}
					//		}
					//	}
					//}
					//如果都沒有代表牌很少
					if netAddings < 1 {
						for i := range newNodes {
							newNodes[i].Index = nodeCount*1000 + l
							nodeCount++
							lastStepNodes[thisNode].ChildInfo = append(lastStepNodes[thisNode].ChildInfo, newNodes[i].Index)
							tree[l].Nodes = append(tree[l].Nodes, newNodes[i])
							netAddings = len(tree[l].Nodes) - beforeAddingNodes
						}
					}
				} else {
					for i := range newNodes {
						newNodes[i].Index = nodeCount*1000 + l
						nodeCount++
						lastStepNodes[thisNode].ChildInfo = append(lastStepNodes[thisNode].ChildInfo, newNodes[i].Index)
						tree[l].Nodes = append(tree[l].Nodes, newNodes[i])
						usedNodes = append(usedNodes, i)
					}
				}
			}
		}
	}
	//passRemover(tree, l)
}

func (t *Layer) iteratedGeneratingProcessStraight8Cards(tree []Layer, lastStepNodes []Nodes, l int, role int, freewillplayer int) {
	//timeTrack(time.Now(), "this Layer took")
	var nodeCount = 0
	for thisNode := range lastStepNodes {
		var beforeAddingNodes = len(tree[l].Nodes)
		//fmt.Println(bool(lastStepNodes[thisNode].Index == tree.Nodes[lastNodesIndices[thisNode]].Index))
		//total := getNodeRestCardsTotal(lastStepNodes[thisNode].RestCards)
		if len(lastStepNodes[thisNode].Outcome) == 0 {
			//fillingToInventory(lastStepNodes[thisNode], inventory)
			var parentNodeIndex = lastStepNodes[thisNode].Index
			var thisHand hand
			thisHand.Poker = lastStepNodes[thisNode].RestCards[role]
			handCards := tree[l-1].newNodesGenWithCon(CardTypeConstructor(thisHand),
				lastStepNodes[thisNode].CardSetInfo.CardSet, lastStepNodes[thisNode].RestCards, l, parentNodeIndex,
				lastStepNodes[thisNode].PassCount, lastStepNodes[thisNode].CardSetInfo.CardTypeSerial, 1) //組成這一手玩家的節點
			var newNodes []Nodes
			for s := 0; s < len(handCards); s++ {
				if lastStepNodes[thisNode].PassCount == 2 {
					if lastStepNodes[thisNode].CardSetInfo.CardTypeSerial == -1 { //開局：前一手是PASS且累積兩次PASS
						//handCards[s].Index = nodeCount*1000 + l
						//nodeCount++
						newNodes = append(newNodes, handCards[s])
						//lastStepNodes[thisNode].ChildInfo = append(lastStepNodes[thisNode].ChildInfo, handCards[s].Index)
						//tree[l].Nodes = append(tree[l].Nodes, handCards[s])
					} else if handCards[s].CardSetInfo.CardTypeSerial != -1 {
						//handCards[s].Index = nodeCount*1000 + l
						//nodeCount++
						newNodes = append(newNodes, handCards[s])
						//lastStepNodes[thisNode].ChildInfo = append(lastStepNodes[thisNode].ChildInfo, handCards[s].Index)
						//tree[l].Nodes = append(tree[l].Nodes, handCards[s])
					}
				} else if handCards[s].CardSetInfo.CardTypeSerial == lastStepNodes[thisNode].CardSetInfo.CardTypeSerial && handCards[s].CardSetInfo.CardTypeSerial != -1 { //同性質的牌組非PASS
					if lastStepNodes[thisNode].PassCount < 2 && typeComparision(handCards[s].CardSetInfo.CardSet, lastStepNodes[thisNode].CardSetInfo.CardSet, handCards[s].CardSetInfo.CardTypeSerial) {
						//前一節點PASS累積數少於2-->比較排型大小
						//handCards[s].Index = nodeCount*1000 + l
						//nodeCount++
						newNodes = append(newNodes, handCards[s])
						//lastStepNodes[thisNode].ChildInfo = append(lastStepNodes[thisNode].ChildInfo, handCards[s].Index)
						//tree[l].Nodes = append(tree[l].Nodes, handCards[s])
					} else if lastStepNodes[thisNode].PassCount == 2 {
						//前一節點PASS累積數＝2-->可直接出下一個
						//handCards[s].Index = nodeCount*1000 + l
						//nodeCount++
						newNodes = append(newNodes, handCards[s])
						//lastStepNodes[thisNode].ChildInfo = append(lastStepNodes[thisNode].ChildInfo, handCards[s].Index)
						//tree[l].Nodes = append(tree[l].Nodes, handCards[s])
					}
				} else if handCards[s].CardSetInfo.CardTypeSerial == 3 && (lastStepNodes[thisNode].CardSetInfo.CardTypeSerial != 3 && lastStepNodes[thisNode].CardSetInfo.CardTypeSerial != 4) {
					//炸彈
					//handCards[s].Index = nodeCount*1000 + l
					//nodeCount++
					newNodes = append(newNodes, handCards[s])
					//lastStepNodes[thisNode].ChildInfo = append(lastStepNodes[thisNode].ChildInfo, handCards[s].Index)
					//tree[l].Nodes = append(tree[l].Nodes, handCards[s])
				} else if handCards[s].CardSetInfo.CardTypeSerial == 4 {
					//王炸
					//handCards[s].Index = nodeCount*1000 + l
					//nodeCount++
					newNodes = append(newNodes, handCards[s])
					//lastStepNodes[thisNode].ChildInfo = append(lastStepNodes[thisNode].ChildInfo, handCards[s].Index)
					//tree[l].Nodes = append(tree[l].Nodes, handCards[s])
				} else if handCards[s].CardSetInfo.CardTypeSerial == -1 && lastStepNodes[thisNode].PassCount < 2 {
					//這一手PASS，總PASS數<=2 (還沒繞一輪) 繼承前一手的牌型
					handCards[s].CardSetInfo.CardSet = lastStepNodes[thisNode].CardSetInfo.CardSet
					handCards[s].CardSetInfo.CardTypeSerial = lastStepNodes[thisNode].CardSetInfo.CardTypeSerial
					//handCards[s].Index = nodeCount*1000 + l
					handCards[s].PassCount = lastStepNodes[thisNode].PassCount + 1
					//nodeCount++
					newNodes = append(newNodes, handCards[s])
					//lastStepNodes[thisNode].ChildInfo = append(lastStepNodes[thisNode].ChildInfo, handCards[s].Index)
					//tree[l].Nodes = append(tree[l].Nodes, handCards[s])
				}
			}
			//檢視有哪些牌型
			var typeMap = make(map[int][]Nodes)
			for i := range newNodes {
				if newNodes[i].PassCount-lastStepNodes[thisNode].PassCount == 1 {
					typeMap[-1] = append(typeMap[-1], newNodes[i])
				} else {
					typeMap[newNodes[i].CardSetInfo.CardTypeSerial] = append(typeMap[newNodes[i].CardSetInfo.CardTypeSerial], newNodes[i])
				}
			}
			var netAddings int
			var usedNodes []int
			if l <= 50 {
				if !playerwithFreeWill(l, freewillplayer) { //假設農夫 freewillplayer 玩家有自由選擇的意志
					//設局的牌組進來
					//第一順位考慮八張牌順子
					if len(typeMap[10]) > 0 && netAddings < 1 {
						for j := range typeMap[10] {
							if notInS2S(typeMap[10][j].CardSetInfo.CardSet, planeBody(lastStepNodes[thisNode].RestCards[role])) {
								typeMap[10][j].Index = nodeCount*1000 + l
								nodeCount++
								lastStepNodes[thisNode].ChildInfo = append(lastStepNodes[thisNode].ChildInfo, typeMap[10][j].Index)
								tree[l].Nodes = append(tree[l].Nodes, typeMap[10][j])
								netAddings = len(tree[l].Nodes) - beforeAddingNodes
							}
						}
					}
					//雙層飛機帶小翼
					if len(typeMap[20]) > 0 && netAddings < 1 {
						for j := range typeMap[20] {
							typeMap[20][j].Index = nodeCount*1000 + l
							nodeCount++
							lastStepNodes[thisNode].ChildInfo = append(lastStepNodes[thisNode].ChildInfo, typeMap[20][j].Index)
							tree[l].Nodes = append(tree[l].Nodes, typeMap[20][j])
							netAddings = len(tree[l].Nodes) - beforeAddingNodes
						}
					}
					//炸彈
					if len(typeMap[3]) > 0 && netAddings < 1 {
						for j := range typeMap[3] {
							typeMap[3][j].Index = nodeCount*1000 + l
							nodeCount++
							lastStepNodes[thisNode].ChildInfo = append(lastStepNodes[thisNode].ChildInfo, typeMap[3][j].Index)
							tree[l].Nodes = append(tree[l].Nodes, typeMap[3][j])
							netAddings = len(tree[l].Nodes) - beforeAddingNodes
						}
					}
					//特定牌型沒牌，就把PASS加進來
					if len(typeMap[-1]) != 0 && netAddings < 1 {
						typeMap[-1][0].Index = nodeCount*1000 + l
						nodeCount++
						lastStepNodes[thisNode].ChildInfo = append(lastStepNodes[thisNode].ChildInfo, typeMap[-1][0].Index)
						tree[l].Nodes = append(tree[l].Nodes, typeMap[-1][0])
						netAddings = len(tree[l].Nodes) - beforeAddingNodes
					}
					//什麼都沒有就單張
					if len(typeMap[0]) > 0 && netAddings < 1 {
						var minValue = []int{100, 100} //(點數，位置)
						for j := range typeMap[0] {
							if typeMap[0][j].CardSetInfo.CardSet[0]%SuitShift < minValue[0] {
								minValue[0] = typeMap[0][j].CardSetInfo.CardSet[0] % SuitShift
								minValue[1] = j
							}
						}
						typeMap[0][minValue[1]].Index = nodeCount*1000 + l
						nodeCount++
						lastStepNodes[thisNode].ChildInfo = append(lastStepNodes[thisNode].ChildInfo, typeMap[0][minValue[1]].Index)
						tree[l].Nodes = append(tree[l].Nodes, typeMap[0][minValue[1]])
						netAddings = len(tree[l].Nodes) - beforeAddingNodes
					}
					//或是上一家的牌型
					//if len(typeMap[lastStepNodes[thisNode].CardSetInfo.CardTypeSerial]) > 0 && netAddings < 1 {
					//	//機翼號碼不為其他機腹的號碼
					//	for j := range typeMap[lastStepNodes[thisNode].CardSetInfo.CardTypeSerial] {
					//		typeMap[lastStepNodes[thisNode].CardSetInfo.CardTypeSerial][j].Index = nodeCount*1000 + l
					//		nodeCount++
					//		lastStepNodes[thisNode].ChildInfo = append(lastStepNodes[thisNode].ChildInfo, typeMap[lastStepNodes[thisNode].CardSetInfo.CardTypeSerial][j].Index)
					//		tree[l].Nodes = append(tree[l].Nodes, typeMap[lastStepNodes[thisNode].CardSetInfo.CardTypeSerial][j])
					//		netAddings = len(tree[l].Nodes) - beforeAddingNodes
					//	}
					//}
					if netAddings < 1 {
						for i := range newNodes {
							newNodes[i].Index = nodeCount*1000 + l
							nodeCount++
							lastStepNodes[thisNode].ChildInfo = append(lastStepNodes[thisNode].ChildInfo, newNodes[i].Index)
							tree[l].Nodes = append(tree[l].Nodes, newNodes[i])
							netAddings = len(tree[l].Nodes) - beforeAddingNodes
						}
					}
				} else {
					for i := range newNodes {
						newNodes[i].Index = nodeCount*1000 + l
						nodeCount++
						lastStepNodes[thisNode].ChildInfo = append(lastStepNodes[thisNode].ChildInfo, newNodes[i].Index)
						tree[l].Nodes = append(tree[l].Nodes, newNodes[i])
						usedNodes = append(usedNodes, i)
					}
				}
			}
		}
	}
	//passRemover(tree, l)
}

func (t *Layer) iteratedGeneratingProcessPlane2LayerWing(tree []Layer, lastStepNodes []Nodes, l int, role int, freewillplayer int) {
	//timeTrack(time.Now(), "this Layer took")
	var nodeCount = 0
	for thisNode := range lastStepNodes {
		var beforeAddingNodes = len(tree[l].Nodes)
		//fmt.Println(bool(lastStepNodes[thisNode].Index == tree.Nodes[lastNodesIndices[thisNode]].Index))
		if len(lastStepNodes[thisNode].Outcome) == 0 {
			//fillingToInventory(lastStepNodes[thisNode], inventory)
			var parentNodeIndex = lastStepNodes[thisNode].Index
			var thisHand hand
			thisHand.Poker = lastStepNodes[thisNode].RestCards[role]
			handCards := tree[l-1].newNodesGenWithCon(CardTypeConstructor(thisHand),
				lastStepNodes[thisNode].CardSetInfo.CardSet, lastStepNodes[thisNode].RestCards, l, parentNodeIndex,
				lastStepNodes[thisNode].PassCount, lastStepNodes[thisNode].CardSetInfo.CardTypeSerial, 1) //組成這一手玩家的節點
			var newNodes []Nodes
			for s := 0; s < len(handCards); s++ {
				if lastStepNodes[thisNode].PassCount == 2 {
					if lastStepNodes[thisNode].CardSetInfo.CardTypeSerial == -1 { //開局：前一手是PASS且累積兩次PASS
						//handCards[s].Index = nodeCount*1000 + l
						//nodeCount++
						newNodes = append(newNodes, handCards[s])
						//lastStepNodes[thisNode].ChildInfo = append(lastStepNodes[thisNode].ChildInfo, handCards[s].Index)
						//tree[l].Nodes = append(tree[l].Nodes, handCards[s])
					} else if handCards[s].CardSetInfo.CardTypeSerial != -1 {
						//handCards[s].Index = nodeCount*1000 + l
						//nodeCount++
						newNodes = append(newNodes, handCards[s])
						//lastStepNodes[thisNode].ChildInfo = append(lastStepNodes[thisNode].ChildInfo, handCards[s].Index)
						//tree[l].Nodes = append(tree[l].Nodes, handCards[s])
					}
				} else if handCards[s].CardSetInfo.CardTypeSerial == lastStepNodes[thisNode].CardSetInfo.CardTypeSerial && handCards[s].CardSetInfo.CardTypeSerial != -1 { //同性質的牌組非PASS
					if lastStepNodes[thisNode].PassCount < 2 && typeComparision(handCards[s].CardSetInfo.CardSet, lastStepNodes[thisNode].CardSetInfo.CardSet, handCards[s].CardSetInfo.CardTypeSerial) {
						//前一節點PASS累積數少於2-->比較排型大小
						//handCards[s].Index = nodeCount*1000 + l
						//nodeCount++
						newNodes = append(newNodes, handCards[s])
						//lastStepNodes[thisNode].ChildInfo = append(lastStepNodes[thisNode].ChildInfo, handCards[s].Index)
						//tree[l].Nodes = append(tree[l].Nodes, handCards[s])
					} else if lastStepNodes[thisNode].PassCount == 2 {
						//前一節點PASS累積數＝2-->可直接出下一個
						//handCards[s].Index = nodeCount*1000 + l
						//nodeCount++
						newNodes = append(newNodes, handCards[s])
						//lastStepNodes[thisNode].ChildInfo = append(lastStepNodes[thisNode].ChildInfo, handCards[s].Index)
						//tree[l].Nodes = append(tree[l].Nodes, handCards[s])
					}
				} else if handCards[s].CardSetInfo.CardTypeSerial == 3 && (lastStepNodes[thisNode].CardSetInfo.CardTypeSerial != 3 && lastStepNodes[thisNode].CardSetInfo.CardTypeSerial != 4) {
					//炸彈
					//handCards[s].Index = nodeCount*1000 + l
					//nodeCount++
					newNodes = append(newNodes, handCards[s])
					//lastStepNodes[thisNode].ChildInfo = append(lastStepNodes[thisNode].ChildInfo, handCards[s].Index)
					//tree[l].Nodes = append(tree[l].Nodes, handCards[s])
				} else if handCards[s].CardSetInfo.CardTypeSerial == 4 {
					//王炸
					//handCards[s].Index = nodeCount*1000 + l
					//nodeCount++
					newNodes = append(newNodes, handCards[s])
					//lastStepNodes[thisNode].ChildInfo = append(lastStepNodes[thisNode].ChildInfo, handCards[s].Index)
					//tree[l].Nodes = append(tree[l].Nodes, handCards[s])
				} else if handCards[s].CardSetInfo.CardTypeSerial == -1 && lastStepNodes[thisNode].PassCount < 2 {
					//這一手PASS，總PASS數<=2 (還沒繞一輪) 繼承前一手的牌型
					handCards[s].CardSetInfo.CardSet = lastStepNodes[thisNode].CardSetInfo.CardSet
					handCards[s].CardSetInfo.CardTypeSerial = lastStepNodes[thisNode].CardSetInfo.CardTypeSerial
					//handCards[s].Index = nodeCount*1000 + l
					handCards[s].PassCount = lastStepNodes[thisNode].PassCount + 1
					//nodeCount++
					newNodes = append(newNodes, handCards[s])
					//lastStepNodes[thisNode].ChildInfo = append(lastStepNodes[thisNode].ChildInfo, handCards[s].Index)
					//tree[l].Nodes = append(tree[l].Nodes, handCards[s])
				}
			}
			//檢視有哪些牌型
			var typeMap = make(map[int][]Nodes)
			for i := range newNodes {
				if newNodes[i].PassCount-lastStepNodes[thisNode].PassCount == 1 {
					typeMap[-1] = append(typeMap[-1], newNodes[i])
				} else {
					typeMap[newNodes[i].CardSetInfo.CardTypeSerial] = append(typeMap[newNodes[i].CardSetInfo.CardTypeSerial], newNodes[i])
				}
			}

			//非玩家前N手的特定牌型拉入
			var netAddings int
			var usedNodes []int
			if l <= 50 {
				if !playerwithFreeWill(l, freewillplayer) { //假設農夫 freewillplayer 玩家有自由選擇的意志
					//設局的牌組進來
					//第一順位考慮四層飛機
					if len(typeMap[22]) > 0 && netAddings < 1 {
						for j := range typeMap[22] {
							a := notPlaneBodyCards(typeMap[22][j].CardSetInfo.CardSet)
							b := notPlaneBodyCards(lastStepNodes[thisNode].RestCards[role])
							sort.Slice(a, func(i, j int) bool { return a[i]%SuitShift < a[j]%SuitShift })
							sort.Slice(b, func(i, j int) bool { return b[i]%SuitShift < b[j]%SuitShift })
							if isInS2S(a, b[:4]) {
								typeMap[22][j].Index = nodeCount*1000 + l
								nodeCount++
								lastStepNodes[thisNode].ChildInfo = append(lastStepNodes[thisNode].ChildInfo, typeMap[22][j].Index)
								tree[l].Nodes = append(tree[l].Nodes, typeMap[22][j])
								netAddings = len(tree[l].Nodes) - beforeAddingNodes
							}
						}
					}
					//雙層飛機帶小翼
					if len(typeMap[20]) > 0 && netAddings < 1 {
						for j := range typeMap[20] {
							a := notPlaneBodyCards(typeMap[20][j].CardSetInfo.CardSet)
							b := notPlaneBodyCards(lastStepNodes[thisNode].RestCards[role])
							sort.Slice(a, func(i, j int) bool { return a[i]%SuitShift < a[j]%SuitShift })
							sort.Slice(b, func(i, j int) bool { return b[i]%SuitShift < b[j]%SuitShift })
							if isInS2S(a, b[:2]) {
								typeMap[20][j].Index = nodeCount*1000 + l
								nodeCount++
								lastStepNodes[thisNode].ChildInfo = append(lastStepNodes[thisNode].ChildInfo, typeMap[20][j].Index)
								tree[l].Nodes = append(tree[l].Nodes, typeMap[20][j])
								netAddings = len(tree[l].Nodes) - beforeAddingNodes
							}
						}
					}
					//炸彈
					if len(typeMap[3]) > 0 && netAddings < 1 {
						//機翼號碼不為其他機腹的號碼
						for j := range typeMap[3] {
							typeMap[3][j].Index = nodeCount*1000 + l
							nodeCount++
							lastStepNodes[thisNode].ChildInfo = append(lastStepNodes[thisNode].ChildInfo, typeMap[3][j].Index)
							tree[l].Nodes = append(tree[l].Nodes, typeMap[3][j])
							netAddings = len(tree[l].Nodes) - beforeAddingNodes
						}
					}
					//特定牌型沒牌，就把PASS加進來
					if len(typeMap[-1]) != 0 && netAddings < 1 {
						typeMap[-1][0].Index = nodeCount*1000 + l
						nodeCount++
						lastStepNodes[thisNode].ChildInfo = append(lastStepNodes[thisNode].ChildInfo, typeMap[-1][0].Index)
						tree[l].Nodes = append(tree[l].Nodes, typeMap[-1][0])
						netAddings = len(tree[l].Nodes) - beforeAddingNodes
					}

					if len(typeMap[1]) > 0 && netAddings < 1 {
						var minValue = []int{100, 100} //(點數，位置)
						for j := range typeMap[1] {
							if typeMap[1][j].CardSetInfo.CardSet[0]%SuitShift < minValue[0] {
								minValue[0] = typeMap[1][j].CardSetInfo.CardSet[0] % SuitShift
								minValue[1] = j
							}
						}
						typeMap[1][minValue[1]].Index = nodeCount*1000 + l
						nodeCount++
						lastStepNodes[thisNode].ChildInfo = append(lastStepNodes[thisNode].ChildInfo, typeMap[1][minValue[1]].Index)
						tree[l].Nodes = append(tree[l].Nodes, typeMap[1][minValue[1]])
						netAddings = len(tree[l].Nodes) - beforeAddingNodes
					}

					//什麼都沒有就單張
					if len(typeMap[0]) > 0 && netAddings < 1 {
						var minValue = []int{100, 100} //(點數，位置)
						for j := range typeMap[0] {
							if typeMap[0][j].CardSetInfo.CardSet[0]%SuitShift < minValue[0] {
								minValue[0] = typeMap[0][j].CardSetInfo.CardSet[0] % SuitShift
								minValue[1] = j
							}
						}
						typeMap[0][minValue[1]].Index = nodeCount*1000 + l
						nodeCount++
						lastStepNodes[thisNode].ChildInfo = append(lastStepNodes[thisNode].ChildInfo, typeMap[0][minValue[1]].Index)
						tree[l].Nodes = append(tree[l].Nodes, typeMap[0][minValue[1]])
						netAddings = len(tree[l].Nodes) - beforeAddingNodes
					}

					if netAddings < 1 {
						for i := range newNodes {
							newNodes[i].Index = nodeCount*1000 + l
							nodeCount++
							lastStepNodes[thisNode].ChildInfo = append(lastStepNodes[thisNode].ChildInfo, newNodes[i].Index)
							tree[l].Nodes = append(tree[l].Nodes, newNodes[i])
							netAddings = len(tree[l].Nodes) - beforeAddingNodes
						}
					}
				} else {
					for i := range newNodes {
						newNodes[i].Index = nodeCount*1000 + l
						nodeCount++
						lastStepNodes[thisNode].ChildInfo = append(lastStepNodes[thisNode].ChildInfo, newNodes[i].Index)
						tree[l].Nodes = append(tree[l].Nodes, newNodes[i])
						usedNodes = append(usedNodes, i)
					}
				}
			}

		}
	}
	//passRemover(tree, l)
}

//不可能預先知道這個pass的價值
func passRemover(tree []Layer, l int) {
	for e := range tree[l-1].Nodes {
		if len(tree[l-1].Nodes[e].ChildInfo) > 1 {

		}
	}
}

func CuttingNodes(newLayer []Nodes, preserve int) []Nodes {
	if len(newLayer) > preserve+1 {
		var src cryptoSource
		rnd := rand.New(src)
		rand.Seed(rnd.Int63n(time.Now().UnixNano()))
		rand.Shuffle(len(newLayer), func(i, j int) { newLayer[i], newLayer[j] = newLayer[j], newLayer[i] })
		newLayer = newLayer[:preserve]
	}
	return newLayer
}

func playerwithFreeWill(l int, role int) bool {
	if notIn(role, []int{0, 1, 2}) {
		return false
	}
	if (l-1)%3 == role {
		return true
	}
	return false
}
func (t *Layer) newNodesGenWithCon(h hand, received []int, inheritedRestCards map[int][]int, l int, PN int, PassCount int, parentType int, main int) []Nodes {
	var newLayer []Nodes
	var count = 0
	if (PassCount < 2 && parentType == 10) || PassCount == 2 {
		//Nodes for straight 8
		for i := 0; i < len(h.straight.straight8); i++ {
			newLayer = append(newLayer, nodeFillIn(h.Poker, h.straight.straight8, i, l, count, 10, PN))
			count++
		}
	}
	//if len(newLayer) == 0 { //農夫一為玩家 (l-1)%3 == 1 || playerwithFreeWill(l,1)
	if (PassCount < 2 && parentType == 20) || PassCount == 2 {
		//Nodes for plane Layer 2 short wings
		for i := 0; i < len(h.plane.planeL2SWing); i++ {
			newLayer = append(newLayer, nodeFillIn(h.Poker, h.plane.planeL2SWing, i, l, count, 20, PN))
			count++
		}
	}
	if (PassCount < 2 && parentType == 9) || (PassCount == 2 && len(newLayer) == 0) {
		//Nodes for straight 7
		for i := 0; i < len(h.straight.straight7); i++ {
			newLayer = append(newLayer, nodeFillIn(h.Poker, h.straight.straight7, i, l, count, 9, PN))
			count++
		}
	}
	if (PassCount < 2 && parentType == 21) || PassCount == 2 {
		//Nodes for plane Layer 3 short wings
		for i := 0; i < len(h.plane.planeL3SWing); i++ {
			newLayer = append(newLayer, nodeFillIn(h.Poker, h.plane.planeL3SWing, i, l, count, 21, PN))
			count++
		}
	}

	if (PassCount < 2 && parentType == 6) || (PassCount == 2 && len(newLayer) < 2) {
		//Nodes for threeTwos
		for i := 0; i < len(h.threeTwos); i++ {
			newLayer = append(newLayer, nodeFillIn(h.Poker, h.threeTwos, i, l, count, 6, PN))
			count++
		}
	}
	if (PassCount < 2 && parentType == 15) || PassCount == 2 {
		//Nodes for plane Layer 2 no wings
		for i := 0; i < len(h.plane.planeL2NoWing); i++ {
			newLayer = append(newLayer, nodeFillIn(h.Poker, h.plane.planeL2NoWing, i, l, count, 15, PN))
			count++
		}
	}
	if (PassCount < 2 && parentType == 16) || PassCount == 2 {
		//Nodes for plane Layer 3 no wings
		for i := 0; i < len(h.plane.planeL3NoWing); i++ {
			newLayer = append(newLayer, nodeFillIn(h.Poker, h.plane.planeL3NoWing, i, l, count, 16, PN))
			count++
		}
	}
	if (PassCount < 2 && parentType == 17) || PassCount == 2 {
		//Nodes for plane Layer 4 no wings
		for i := 0; i < len(h.plane.planeL4NoWing); i++ {
			newLayer = append(newLayer, nodeFillIn(h.Poker, h.plane.planeL4NoWing, i, l, count, 17, PN))
			count++
		}
	}
	if (PassCount < 2 && parentType == 18) || PassCount == 2 {
		//Nodes for plane Layer 5 no wings
		for i := 0; i < len(h.plane.planeL5NoWing); i++ {
			newLayer = append(newLayer, nodeFillIn(h.Poker, h.plane.planeL5NoWing, i, l, count, 18, PN))
			count++
		}
	}
	if (PassCount < 2 && parentType == 19) || PassCount == 2 {
		//Nodes for plane Layer 6 no wings
		for i := 0; i < len(h.plane.planeL6NoWing); i++ {
			newLayer = append(newLayer, nodeFillIn(h.Poker, h.plane.planeL6NoWing, i, l, count, 19, PN))
			count++
		}
	}

	if (PassCount < 2 && parentType == 22) || PassCount == 2 {
		//Nodes for plane Layer 4 short wings
		for i := 0; i < len(h.plane.planeL4SWing); i++ {
			newLayer = append(newLayer, nodeFillIn(h.Poker, h.plane.planeL4SWing, i, l, count, 22, PN))
			count++
		}
	}
	if (PassCount < 2 && parentType == 23) || PassCount == 2 {
		//Nodes for plane Layer 5 short wings
		for i := 0; i < len(h.plane.planeL5SWing); i++ {
			newLayer = append(newLayer, nodeFillIn(h.Poker, h.plane.planeL5SWing, i, l, count, 23, PN))
			count++
		}
	}
	if (PassCount < 2 && parentType == 24) || PassCount == 2 {
		//Nodes for plane Layer 2 long wings
		for i := 0; i < len(h.plane.planeL2LWing); i++ {
			newLayer = append(newLayer, nodeFillIn(h.Poker, h.plane.planeL2LWing, i, l, count, 24, PN))
			count++
		}
	}
	if (PassCount < 2 && parentType == 25) || PassCount == 2 {
		//Nodes for plane Layer 3 long wings
		for i := 0; i < len(h.plane.planeL3LWing); i++ {
			newLayer = append(newLayer, nodeFillIn(h.Poker, h.plane.planeL3LWing, i, l, count, 25, PN))
			count++
		}
	}
	if (PassCount < 2 && parentType == 26) || PassCount == 2 {
		//Nodes for plane Layer 4 long wings
		for i := 0; i < len(h.plane.planeL4LWing); i++ {
			newLayer = append(newLayer, nodeFillIn(h.Poker, h.plane.planeL4LWing, i, l, count, 26, PN))
			count++
		}
	}

	//if len(newLayer) < 2 {
	if (PassCount < 2 && parentType == 7) || PassCount == 2 {
		//Nodes for straight 5
		for i := 0; i < len(h.straight.straight5); i++ {
			newLayer = append(newLayer, nodeFillIn(h.Poker, h.straight.straight5, i, l, count, 7, PN))
			count++
		}
	}
	if (PassCount < 2 && parentType == 8) || PassCount == 2 {
		//Nodes for straight 6
		for i := 0; i < len(h.straight.straight6); i++ {
			newLayer = append(newLayer, nodeFillIn(h.Poker, h.straight.straight6, i, l, count, 8, PN))
			count++
		}
	}

	if (PassCount < 2 && parentType == 11) || PassCount == 2 {
		//Nodes for straight 9
		for i := 0; i < len(h.straight.straight9); i++ {
			newLayer = append(newLayer, nodeFillIn(h.Poker, h.straight.straight9, i, l, count, 11, PN))
			count++
		}
	}
	if (PassCount < 2 && parentType == 12) || PassCount == 2 {
		//Nodes for straight 10
		for i := 0; i < len(h.straight.straight10); i++ {
			newLayer = append(newLayer, nodeFillIn(h.Poker, h.straight.straight10, i, l, count, 12, PN))
			count++
		}
	}
	if (PassCount < 2 && parentType == 13) || PassCount == 2 {
		//Nodes for straight 11
		for i := 0; i < len(h.straight.straight11); i++ {
			newLayer = append(newLayer, nodeFillIn(h.Poker, h.straight.straight11, i, l, count, 13, PN))
			count++
		}
	}
	if (PassCount < 2 && parentType == 14) || PassCount == 2 {
		//Nodes for straight 12
		for i := 0; i < len(h.straight.straight12); i++ {
			newLayer = append(newLayer, nodeFillIn(h.Poker, h.straight.straight12, i, l, count, 14, PN))
			count++
		}
	}
	if (PassCount < 2 && parentType == 27) || PassCount == 2 {
		//Nodes for pairStraight 3 Layers
		for i := 0; i < len(h.pairStraight.pairStraight3); i++ {
			newLayer = append(newLayer, nodeFillIn(h.Poker, h.pairStraight.pairStraight3, i, l, count, 27, PN))
			count++
		}
	}
	if (PassCount < 2 && parentType == 28) || PassCount == 2 {
		//Nodes for pairStraight 4 Layers
		for i := 0; i < len(h.pairStraight.pairStraight4); i++ {
			newLayer = append(newLayer, nodeFillIn(h.Poker, h.pairStraight.pairStraight4, i, l, count, 28, PN))
			count++
		}
	}
	if (PassCount < 2 && parentType == 29) || PassCount == 2 {
		//Nodes for pairStraight 5 Layers
		for i := 0; i < len(h.pairStraight.pairStraight5); i++ {
			newLayer = append(newLayer, nodeFillIn(h.Poker, h.pairStraight.pairStraight5, i, l, count, 29, PN))
			count++
		}
	}
	if (PassCount < 2 && parentType == 30) || PassCount == 2 {
		//Nodes for pairStraight 6 Layers
		for i := 0; i < len(h.pairStraight.pairStraight6); i++ {
			newLayer = append(newLayer, nodeFillIn(h.Poker, h.pairStraight.pairStraight6, i, l, count, 30, PN))
			count++
		}
	}
	if (PassCount < 2 && parentType == 31) || PassCount == 2 {
		//Nodes for pairStraight 7 Layers
		for i := 0; i < len(h.pairStraight.pairStraight7); i++ {
			newLayer = append(newLayer, nodeFillIn(h.Poker, h.pairStraight.pairStraight7, i, l, count, 31, PN))
			count++
		}
	}
	if (PassCount < 2 && parentType == 32) || PassCount == 2 {
		//Nodes for pairStraight 8 Layers
		for i := 0; i < len(h.pairStraight.pairStraight8); i++ {
			newLayer = append(newLayer, nodeFillIn(h.Poker, h.pairStraight.pairStraight8, i, l, count, 32, PN))
			count++
		}
	}
	if (PassCount < 2 && parentType == 33) || PassCount == 2 {
		//Nodes for pairStraight 9 Layers
		for i := 0; i < len(h.pairStraight.pairStraight9); i++ {
			newLayer = append(newLayer, nodeFillIn(h.Poker, h.pairStraight.pairStraight9, i, l, count, 33, PN))
			count++
		}
	}
	if (PassCount < 2 && parentType == 34) || PassCount == 2 {
		//Nodes for pairStraight 10 Layers
		for i := 0; i < len(h.pairStraight.pairStraight10); i++ {
			newLayer = append(newLayer, nodeFillIn(h.Poker, h.pairStraight.pairStraight10, i, l, count, 34, PN))
			count++
		}
	}

	if (PassCount < 2 && parentType == 35) || PassCount == 2 {
		//Nodes for fourOnes
		for i := 0; i < len(h.fourOnes); i++ {
			newLayer = append(newLayer, nodeFillIn(h.Poker, h.fourOnes, i, l, count, 35, PN))
			count++
		}
	}
	if (PassCount < 2 && parentType == 36) || PassCount == 2 {
		//Nodes for fourTwos
		for i := 0; i < len(h.fourTwos); i++ {
			newLayer = append(newLayer, nodeFillIn(h.Poker, h.fourTwos, i, l, count, 36, PN))
			count++
		}
	}

	if (PassCount < 2 && parentType == 2) || PassCount == 2 {
		//Nodes for threes
		for i := 0; i < len(h.threes); i++ {
			newLayer = append(newLayer, nodeFillIn(h.Poker, h.threes, i, l, count, 2, PN))
			count++
		}
	}
	if (PassCount < 2 && parentType == 5) || PassCount == 2 {
		//Nodes for threeOnes
		for i := 0; i < len(h.threeOnes); i++ {
			newLayer = append(newLayer, nodeFillIn(h.Poker, h.threeOnes, i, l, count, 5, PN))
			count++
		}
	}
	if (PassCount < 2 && parentType == 1) || PassCount == 2 {
		//Nodes for pairs
		for i := 0; i < len(h.pairs); i++ {
			newLayer = append(newLayer, nodeFillIn(h.Poker, h.pairs, i, l, count, 1, PN))
			count++
		}
	}
	//newLayer = CuttingNodes(newLayer, 3)
	//Let farmer 2 have free will
	//Nodes for singles
	for i := 0; i < len(h.singleCards); i++ {
		newLayer = append(newLayer, nodeFillIn(h.Poker, h.singleCards, i, l, count, 0, PN))
		count++
	}
	//保證能pass
	//node for pass
	if (l-1) != 0 || len(received) != 0 {
		newLayer = append(newLayer, nodeFillIn(h.Poker, h.void, 0, l, count, -1, PN))
		count++
	}

	//Nodes for bombs
	for i := 0; i < len(h.bombs); i++ {
		newLayer = append(newLayer, nodeFillIn(h.Poker, h.bombs, i, l, count, 3, PN))
		count++
	}
	//保證有王炸
	//node for kBomb
	if len(h.kBomb) != 0 {
		newLayer = append(newLayer, nodeFillIn(h.Poker, h.kBomb, 0, l, count, 4, PN))
		count++
	}

	//檢視節點剩下牌組 誰已經沒有牌了，地主沒有牌給[1,0]，農夫任一沒有牌給[0,1]
	for i := 0; i < len(newLayer); i++ {
		if len(newLayer[i].RestCards[(l-1)%3]) == 0 && (l-1)%3 != 0 {
			newLayer[i].Outcome = []int{0, 1}
		} else if len(newLayer[i].RestCards[(l-1)%3]) == 0 && (l-1)%3 == 0 {
			newLayer[i].Outcome = []int{1, 0}
		}
		//t.Nodes = append(t.Nodes, newLayer[i])
	}

	if len(newLayer) != 0 {
		for r := 0; r < len(newLayer); r++ {
			for i := 0; i < 3; i++ {
				if (l-1)%3 != i {
					newLayer[r].RestCards[i] = inheritedRestCards[i]
				}
			}
			//newLayer[r].Isopoint = -999
			newLayer[r].ParentInfo = PN
		}
	}
	//fmt.Println("Candidate Nodes", len(newLayer))
	return newLayer
}
