package awardcentertest

import (
	"fmt"
	"time"
)

func SegmentationFinder() {
	defer timeTrack(time.Now(), "Segmentation Time")
	playerCards := StraightPlaneBombOpening()
	playerCards[0].Poker = playerCards[0].Poker[:3]
	var collector cardsSetsOp
	collector.openingCooking(playerCards[0])
	var tree = make([]SegLayer, 1)
	tree[0].SegLayer = append(tree[0].SegLayer, SegNode{[]int{}, playerCards[0].Poker, 0})

	var l = 1
	for {
		var newSegLayer SegLayer
		tree = append(tree, newSegLayer)
		if len(tree[l-1].SegLayer) != 0 {
			tree[l-1].iteratedGeneratingProcess(tree, l) //開始分佈節點，因為l從1開始，所以這邊role=(l-1)%3
		} else {
			break
		}
		l++
	}
	totalSegLayers(tree, 0, l)
}
func (t *SegLayer) iteratedGeneratingProcess(tree []SegLayer, l int) {
	for thisNode := range tree[l-1].SegLayer {
		var count = tree[l-1].SegLayer[thisNode].Seg
		var collector cardsSetsOp
		var newHand hand
		for j := range tree[l-1].SegLayer[thisNode].CardSet {
			if notIn(tree[l].SegLayer[thisNode].CardSet[j], tree[l-1].SegLayer[thisNode].RestCards) {
				newHand.Poker = append(newHand.Poker, tree[l-1].SegLayer[thisNode].CardSet[j])
			}
		}
		newHand = CardTypeConstructor(newHand)
		collector.openingCooking(newHand)
		for i := range collector.pile {
			count++
			newNodes := SegNode{[]int{}, collector.pile[i], count}
			tree[l].SegLayer = append(tree[l].SegLayer, newNodes)
		}
	}
}

func totalSegLayers(tree []SegLayer, initialLayer int, destLayer int) {
	defer timeTrack(time.Now(), "主樹節點填入Mongo時間")
	//創建檔案
	//createFile()
	for i := range tree {
		if len(tree[i].SegLayer) != 0 && i <= destLayer && i >= initialLayer {
			thisSegLayerNodes(tree[i].SegLayer, i)
			//fmt.Println("▃▃▃▃▃▃▃▃▃▃ Total Node(s) in Layer", i, "th :", len(tree[i].Nodes), "▃▃▃▃▃▃▃▃▃▃▃")
		}
	}
}

func thisSegLayerNodes(thisStepNodes []SegNode, l int) {
	for i := range thisStepNodes {
		fmt.Println(thisStepNodes[i])
	}
}
